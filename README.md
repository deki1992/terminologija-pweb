# README - Rečnik računarskih termina #

This README would normally document whatever steps are necessary to get your application up and running.

### Opis aplikacije ###

* Naslov sve govori. Aplikacija služi beleženju termina iz raznih kategorija računarstva, predlaganju njihovih prevoda te ocenjivanju i komentarisanju prevoda.
* Verzija 0.1 a.k.a prva moguća samo da radi za potrebe odbrane rada

### Instalacija - priprema za programere ###

* Angular.js verzija **1.x** - instalirati pomoću **npm install**
* Slim framework verzija **2** - koristiti **composer** alat (priložen je composer.json)
* Preporučeno radno okruženje - IntelliJ IDEA Ultimate / PhpStorm
* Konfiguracija je u **api/include/constants.php**. **Poželjno je ne menjati ga ovde.** Na produkcionim serverima bi bila drugačija konfiguracija; ovo skladište koda služi za razvojni tim.
* Baza podataka - pristupni podaci su **termuser** : **termuser** za API. Podesiti server baze po potrebi. Tabele se prave pomoću modela u priloženom terminologija.mwb / terminologija.sql
* Testovi nisu razrađeni, inače se (podrazumevano uz novi Angular projekat) koristi Protractor za end-to-end testove i Karma za JS testiranje
* Podrazumevana lokacija aplikacije - trebalo bi da je svejedno, http://localhost/bilosta. Npr localhost:8080/webp/ (odnosi se na koreni folder projekta)
* **Korisnička aplikacija (Angular)**: app/, **API**: api/. **Ne menjati ove linkove i sve bi trebalo da radi** (dakle puna lokacija bi bila http://localhost:8080/webp/app)

### ČITATI WIKI ###

Pošto Wiki nije najažurniji, **obavezno PRATITI TODO I FIXME u kodu**