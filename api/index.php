<?php
require_once '../vendor/autoload.php';
require_once 'include/constants.php';
// Change values in constants.php on production server.

require_once 'include/Exception.php';
require_once 'include/logger.php';
require_once 'include/DB1.php';
require_once 'include/access_token.php';
require_once 'include/Session.php';

if (defined("RTAPP_LANG"))
	@include 'include/i18n.'.RTAPP_LANG.'.php' || require_once 'include/i18n.php';
else
	require_once 'include/i18n.php';

use RTApp\AccessToken as AccessToken;
use RTApp\APIError as APIError;
use Slim\Log;
use RTApp\Session;

$app = new \Slim\Slim(array(
	'mode' => RTAPP_DEVMODE,

	// 'cookies.encrypt' => true,
	// 'cookies.lifetime' => '20 minutes',

	// 'cookies.path' => '/',
	// 'cookies.secure' => false,
	// 'cookies.httponly' => true,
	// 'cookies.secret_key' => 'CHANGE_ME',
));
//$app->setName('rt_app');

$app->configureMode('production', function () use ($app) {
	require_once 'include/logger.php';
	$app->config(array(
		'log.writer' => new \RTApp\Logger(),
		'log.enabled' => true,
		'debug' => false
	));
});

$app->configureMode('development', function () use ($app) {
	$app->config(array(
		'log.enabled' => false,
        'debug' => false,//true,
	));
});

$app->notFound(function() use ($app) {
	$app->status(404);
	echo json_encode(array(
		"error" => array(
			"type"    => "NotFound",
			"code"    => 404,
			"message" => "Unknown API"
		)
	));
});

$app->error(function(Exception $ex) use ($app) {
	$class = get_class($ex);
	if ($class == "RTApp\\Bug")
		// FIXME Should bug message be returned to frontend in production? Or just code with generic message?
		$app->status(500);
	else if ($class == "RTApp\\APIError") {
		$app->status($ex->getHTTPStatus());
		if ($ex->getHTTPStatus() == 401 && $ex->getCode() != APIError::BAD_CREDENTIALS) {
			// F F F F FIXME. Why am I taking this OAuth/REST thing TOO seriously? (and this will never REALLY be OAuth)
			$authHeader = "Bearer realm=\"REST API\"";
			if ($ex->getCode() != APIError::TOKEN_MISSING)
				$authHeader .= ", error=\"invalid_token\"";
			$app->response->header("WWW-Authenticate", $authHeader);
		}
	} else if ($class == "RTApp\\AppError")
		$app->status(500);
	else {
		// In development mode, give entire stack trace for all errors not thrown in our code
		if ($app->getMode() == "development")
			$moreDetails = PHP_EOL.$ex->getFile().":".$ex->getLine().PHP_EOL.$ex->getTraceAsString();
		$app->status(500);
	}

	// Log all application errors and bugs in production mode (not needed for API errors)
	// CHECK IF SLIM DOES THIS AUTOMATICALLY. Log is enabled in production - is it used if our own
	// error handler handles the exception? Also check if it's used always or only on ErrorException.
	if ($app->getMode() == "production" && $app->response->getStatus() >= 500)
		$app->getLog()->error($ex); // error(message, array("exception"=>$ex))

	echo json_encode(array(
		"error" => array(
			"type"    => get_class($ex),
			"code"    => $ex->getCode(),
			"message" => _T($ex->getMessage()) . (empty($moreDetails) ? '' : $moreDetails)
		)
	));
});


function tokenAuth(\Slim\Route $route) {
	// HTTP header Authorization: Bearer ACCESS_TOKEN
	$headers = getallheaders();
	$auth = empty($headers['Authorization']) ? "" : $headers['Authorization'];
	//$auth = \Slim\Slim::getInstance()->request->headers->get('Authorization');
	if (empty($auth)) $auth = "";
	if (substr($auth, 0, 7) != 'Bearer ')
		throw new APIError("This operation requires a valid authentication token", APIError::TOKEN_MISSING);

	$tok = AccessToken::parse(substr($auth, 7));
	// (WE WILL NOT REQUIRE IP ADDRESS MATCH)
	//if ($_SERVER['REMOTE_ADDR'] != $tok['ip']) error...
	// For now only app_id 0 (browser). If there were others, they would probably send additional header...
	if ($_SERVER['HTTP_USER_AGENT'] != $tok['ua'] || $tok['app_id'] != 0)
		throw new APIError("Provided access token does not belong to this app", APIError::TOKEN_DENIED);

	$user_id = $tok['user'];
	$session_id = $tok['session_id'];
	$valid_s = empty($tok['validity']) ? RTAPP_TOKEN_DURATION : intval($tok['validity']);
	$valid_m = (int)floor($valid_s / 60);
	$valid_s = $valid_s % 60;
	$valid_h = (int)floor($valid_m / 60);
	$valid_m = $valid_m % 60;
	$valid_time = "$valid_h:$valid_m:$valid_s";
	$updated = //\RTApp\DB::instance()->sql_update(<<<QUERY
		\RTApp\Database::instance()->execPreparedQuery(<<<QUERY
UPDATE sessions SET last_active = NOW(3) WHERE ID = ? AND User_ID = ? AND TIMEDIFF(NOW(3), last_active) < ?
QUERY
		, /*'iis',*/ array($session_id, $user_id, $valid_time), FALSE);
	if ($updated < 1)
		// User session is either invalid, logged out (no longer in database) or expired due to inactivity
		throw new APIError("Invalid access token supplied", APIError::TOKEN_INVALID);

	$user = \RTApp\User::getFromSession($tok);
	$isActive = $user->get('active'); // FIXME Every model class should have own getters
	if (!$isActive)
		// User is deactivated / not allowed to login. Do not accept any of its sessions
		throw new APIError("Invalid access token supplied", APIError::TOKEN_INVALID);

	// Save the user somewhere perhaps? Since all actions user does would probably use at least its ID
	AccessToken::$currentToken = $tok;
}

function logAction(\Slim\Route $route,\Slim\Http\Request $request = null)
{
	$logEntry = new \RTApp\Logger();
	$logEntry->write(implode(getallheaders()),Log::DEBUG);
}

function parseRequestBody(\Slim\Http\Request $request) {
	//if (!$request->isAjax() && !$request->isXhr())
	//	throw new APIError("Bad Request", APIError::BAD_CONTENT);
	// Angular does not seem to send AJAX requests !!! (I DON'T KNOW HOW IS THAT POSSIBLE)
	$body = $request->getBody();
	$json = @json_decode($body, true);
	if ($json === FALSE)
		throw new APIError("Bad Request", APIError::BAD_CONTENT);
	return $json;
}


// TEST. Dumps information. The only endpoint (including errors) that displays HTML
$app->get('/','logAction', function () use ($app) {
	var_dump($app->environment);
	var_dump($app->request->get());
	var_dump($app->request->getBody());
	echo $app->request->getUserAgent()." ".$app->request->getIp()." ".$app->request->getPath()." ".$app->request->getPathInfo();
	var_dump($app->request->headers->all());
	// WARNING : $app->request->headers DOES NOT CONTAIN Authorization HEADER!
	// Headers seem to be fetched from $_ENV/$_SERVER variables and Apache DOES NOT include Authorization!
	// For basic authorization there are PHP_AUTH_*, but for others only getallheaders() work!
});

require_once "include/api_sessions.php";
require_once "include/api_categories.php";
require_once "include/api_terms.php";
require_once "include/api_translations.php";
require_once "include/api_comments_votes.php";
require_once "include/api_users.php";
require_once "include/api_example.php";
require_once "include/api_language.php";
require_once "include/api_log.php";

#**********************************************************
// ONLY FOR DEVELOPMENT, used for manual user insertion into database. Generates password hashes
$app->get('/password/:pw', function ($pw) use($app) {
	$user = new \RTApp\User();
	$hash = "";
	$salt = "";
	$user->setPassword($pw);
	$user->getPassword($hash, $salt);

	$app->response()->header("Content-Type", "text/plain; charset=utf-8"); // application/json. Just not HTML!
	echo json_encode(array(
		"password" => $hash,
		"salt"     => $salt
	), JSON_PRETTY_PRINT);
});

$app->run();
