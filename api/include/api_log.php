<?php
/**
 * Created by IntelliJ IDEA.
 * User: Nemanja
 * Date: 26.6.2016
 * Time: 11:45
 */

require_once "User.php";
require_once "ActionLog.php";
require_once "Exception.php";

$app->get("/logs",'tokenAuth',function() use($app){
    
    $user = \RTApp\User::getFromSession(\RTApp\AccessToken::$currentToken);
    if($user->isAdmin()) {
        $pagesize = isset($_GET['pagesize']) ? $_GET['pagesize'] : null;
        $page = isset($_GET['page']) ? $_GET['page'] : null;
        $actions = isset($_GET['actions']) ?  $_GET['actions']:null;
        $types =isset( $_GET['types'])? $_GET['types']: null;
        $app->response->headers->set('Content-Type', 'application/json');
        if (null != $pagesize && null != $page) {
            $app->response->setBody(json_encode(\RTApp\ActionLog::getPaged($pagesize, $page,$actions,$types)));
        } else
            $app->response->setBody(json_encode(\RTApp\ActionLog::GetAll()));
    }
    else {
        //$app->response->setStatus(403);
        //$app->response->setBody("You need to be an administrator to view activity log");
        throw new \RTApp\APIError("This action requires administrator access", \RTApp\APIError::ACCESS_DENIED);
    }
});

$app->get("/logs/:id",'tokenAuth',function($id) use($app){
    $user = \RTApp\User::getFromSession(\RTApp\AccessToken::$currentToken);
    if($user->isAdmin()) {
        $app->response->setBody(json_encode(\RTApp\ActionLog::getLog($id)));
    }
    else
    {
        //$app->response->setStatus(403);
        //$app->response->setBody("You need to be an administrator to view activity log");
        throw new \RTApp\APIError("This action requires administrator access", \RTApp\APIError::ACCESS_DENIED);
    }
});

