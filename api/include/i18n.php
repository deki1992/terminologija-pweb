<?php

function _T($str) {
	global $_T;
	return array_key_exists($str, $_T) ? $_T[$str] : $str;
}

$_T = array(
	'Unknown API' => 'Nepoznata akcija',
	'Specified entity was not found' => 'Traženi resurs nije pronađen',
	'This action requires administrator access' => 'Potreban je administratorski nivo pristupa',

	// Application Errors
	// TODO Is "try again later", etc necessary? Could that be added automatically to AppError messages?
	'Failed to connect to database' => 'Neuspelo povezivanje sa bazom. Molimo pokušajte kasnije ili se obratite administratorima',
	'Failed to initialize database' => 'Greška u konekciji sa bazom. Molimo pokušajte kasnije ili se obratite administratorima',
	'Database query failed' => 'Greška pri upitu bazi podataka. Molimo pokušajte kasnije ili se obratite administratorima',

	// Unauthorized
	'Invalid access token supplied' => 'Neispravna korisnička sesija',
	'This operation requires a valid authentication token' => 'Potrebna je aktivna korisnička sesija za ovu operaciju',
	'Provided access token does not belong to this app' => 'Data korisnička sesija ne pripada ovoj aplikaciji',
	'Bad username or password' => 'Neispravno korisničko ime ili šifra',
	'Incorrect password' => 'Pogrešna šifra', // For changing account e-mail or password

	// Bad Request
	'Bad Request' => 'Neispravan zahtev',
	'Missing parameters' => 'Nedostaju neophodne vrednosti',
	//'Unsupported operation'=>'',

	// User
	'Specified user was not found' => 'Traženi korisnik ne postoji',
	'The given username or e-mail address is already registered' => 'Dato korisničko ime ili e-mail adresa su već registrovani',
	'E-mail address is already registered on another account' => 'Data e-mail adresa već pripada drugom korisniku',
);
