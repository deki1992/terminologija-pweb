<?php 

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';
require_once 'Example.php';
require_once 'Translation1.php';

class OriginalTerm extends Entity implements \JsonSerializable{

	protected static $_table = "OriginalTerm";

	private $ID;
	private $term;
	private $language;
	private $language_code;
	private $creator;
	private $creation_date;
	private $description;
	private $conclusion;
	private $category_id;
	private $category_title;
	private $translations;
	private $examples;

	#****************************************************
	
	public function __construct() {}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

	#****************************************************

	private function setExamples($_examples){
		$this->examples = $_examples;
	}
	#****************************************************

	private function setTranslations($_translations){
		$this->translations = $_translations;
	}
	#****************************************************

	private function setCategory($_category){
		$this->category_id = $_category;
	}
	
	#****************************************************

	public static function insertTerm($_term, $_language, $_creator, $_description, $_conclusion, $categories){
		
		$db = Database::instance();

		$db->beginTransaction();
		$query = "INSERT INTO OriginalTerm ( term, language, creator, creation_date, description, conclusion) VALUES ( ?, ?, ?,  NOW(), ?, ?);";
		$args = array( $_term, $_language, intval($_creator), $_description, $_conclusion);
		
		$db->execPreparedQuery($query, $args, False);
		
		$index = $db->lastID();

		$query = "INSERT INTO CATEGORY_TERM VALUES (?, ?);";
		foreach($categories as $cid) {
			$args = array(intval($cid), $index);
			$db->execPreparedQuery($query, $args, False);		}

		$newTerm = OriginalTerm::getTermData($db, $index, $categories[0]); //FIXME
		$logdata = array("term"=>$newTerm,"categories"=>Category::getNames($db,$categories));
		ActionLog::addLogEntry($db, intval($_creator),ActionLog::INSERT ,'TERM' ,$index ,json_encode($logdata));

        $db->commit();

		return json_encode($newTerm);
		
	}
	
	#****************************************************
	
	// FIXME 1/2: Multi-getters should be static.
	public static function getTermsInCategory($category){
		
		$db = Database::instance();
		$listOfObjects = Array();
		
		$query = "SELECT OT.ID, OT.TERM, L.LANGUAGE, L.CODE, U.USERNAME, OT.CREATION_DATE, OT.DESCRIPTION, OT.CONCLUSION, C.ID, C.TITLE ".
			"FROM (((OriginalTerm AS OT join CATEGORY_TERM AS CT ON OT.ID = CT.TERM_ID) ".
			"JOIN USER AS U ON U.ID = OT.CREATOR) JOIN CATEGORIES AS C ON CT.CATEGORY_ID = C.ID) ".
			"JOIN LANGUAGES AS L ON L.CODE = OT.LANGUAGE WHERE CT.CATEGORY_ID = (?);";
		$args = array($category);
		
		$result = $db->execPreparedQuery($query, $args, True);
		
		foreach ($result as $value){
			$foo = new OriginalTerm();
			$foo->loadValues($value);
			$foo->category_id = $category;
			array_push($listOfObjects, $foo);
						
		}
		
		return json_encode($listOfObjects);
		
	}
	#****************************************************
	
	public static function deleteTerm($userId,$term_id, $category_id){
		

		$db = Database::instance();
		$db->beginTransaction();

		//$oldTerm = OriginalTerm::getTermData($db, $_id, ??category??);

		$countQuery = "SELECT COUNT(*) from CATEGORY_TERM WHERE TERM_ID = (?);";
		$countArg = array($term_id);
		$result = $db->execPreparedQuery($countQuery, $countArg, true);
		$count = $result[0][0];

		if (intval($count) == 1){

			$query = "DELETE FROM OriginalTerm WHERE ID = (?);";
			$args = array($term_id);

			$db->execPreparedQuery($query, $args, False);
			ActionLog::addLogEntry($db, intval($userId),ActionLog::DELETE ,'TERM' ,$term_id ,json_encode($args));

		}
		else if(intval($count) > 1){
			//Ukoliko se termin nalazi u vise kategorija, brise se samo u toj kategoriji
			$query = "DELETE FROM CATEGORY_TERM WHERE TERM_ID = (?) AND CATEGORY_ID = (?);";
			$args = array($term_id, $category_id);

			$db->execPreparedQuery($query, $args, False);
			ActionLog::addLogEntry($db, intval($userId),ActionLog::DELETE ,'CATEGORY_TERM' ,$term_id ,json_encode($args));
		}



		$db->commit();
		
	}
	
	#****************************************************
	// FIXME 2/2: For getting single entity, constructor with $id argument is recommended
	// (This is kind of the same as Entity::load())
	public static function getTermDetailed($_term_id, $_category_id){
		
		$db = Database::instance();
		$orgTermData = OriginalTerm::getTermData($db, $_term_id, $_category_id);
		

		$orgTermData->setExamples(\RTApp\Example::getExamplesForTerm($_term_id, $_category_id));
		$orgTermData->setTranslations(\RTApp\Translation::getTranslationsForTerm($_term_id, $_category_id));

		
		return json_encode($orgTermData);
		
	}

	#**************************************************************
    //TODO Ovo je nepotrebno komplikovan upit ali "radi posao" za sada
	public static function searchTerms($term,$language,$category)
	{
		$db = Database::instance();
		$logger = new Logger();
		$query = "SELECT OT.*,CT.Category_ID,C.Title,U.Username FROM
		ORIGINALTERM AS OT JOIN CATEGORY_TERM AS CT ON OT.ID = CT.TERM_ID
		JOIN CATEGORIES C ON CT.CATEGORY_ID = C.ID  
        JOIN USER U on OT.creator = u.id
		WHERE :language in (OT.LANGUAGE,'*')
		AND OT.TERM LIKE CONCAT('%',:term,'%')
		AND :categoryid in (CT.CATEGORY_ID,'*')";
		$args = array(":term"=>$term,":language"=>$language,":categoryid"=>$category);
		$result = $db->execPreparedQuery($query, $args, true,Database::FETCH_ASSOC,$logger);
		return json_encode($result);
	}
	#**************************************************************
	
	
	#****************************************************
	public static function updateTerm($userId,$id,$term,$language,$description,$conclusion)
	{
		$db = Database::instance();
		$logger = new Logger();
		$query = "UPDATE TERMINOLOGY.ORIGINALTERM SET ".
			"term = ifnull(:term,term)".
			",language = ifnull(:language,language)".
			",description = ifnull(:description,description)".
			",conclusion = ifnull(:conclusion,conclusion)".
			" where ID = :id";
		$args = array(":id"=>$id,":term"=>$term,":language"=>$language,":description"=>$description,":conclusion"=>$conclusion);
		$db->beginTransaction();

		$rowsChanged = $db->execPreparedQuery($query, $args,false,null,$logger);
		if($rowsChanged > 0) //only log if there were actual changes
		{
			$termDetails = self::getTermDetails($db, $id);
			ActionLog::addLogEntry($db, $userId, ActionLog::UPDATE, 'TERM', $id,json_encode($termDetails));
		}
		$db->commit();
	}
	
	#****************************************************
	
	private function loadValues($result){
		
		$this->ID = $result[0];
		$this->term = $result[1];
		$this->language = $result[2];
		$this->language_code = $result[3];
		$this->creator = $result[4];
		$this->creation_date = $result[5];
		$this->description = $result[6];
		$this->conclusion = $result[7];
		$this->category_id = $result[8];
		$this->category_title = $result[9];

	}
	#****************************************************
	public function getID()
	{
		return $this->ID;
	}

	#****************************************************

	private static function getTermData($db, $id, $category){

		$query = "SELECT OT.ID, OT.TERM, L.LANGUAGE, L.CODE, U.USERNAME, OT.CREATION_DATE, OT.DESCRIPTION, OT.CONCLUSION, C.ID, C.TITLE ".
					"FROM (((OriginalTerm AS OT join CATEGORY_TERM AS CT ON OT.ID = CT.TERM_ID) ".
 					"JOIN USER AS U ON U.ID = OT.CREATOR) JOIN CATEGORIES AS C ON CT.CATEGORY_ID = C.ID) ".
 					"JOIN LANGUAGES AS L ON L.CODE = OT.LANGUAGE ".
 					"WHERE CT.CATEGORY_ID = (?) AND CT.term_id = (?);";

		$args = array(intval($category), intval($id));
		$result = $db->execPreparedQuery($query, $args, true);

		$newTerm = new OriginalTerm();
		$newTerm->loadValues($result[0]);

		return $newTerm;

	}

	#************************************************************************

	private static function getTermDetails($db,$id) //get *just* original term details
	{
		$query = "SELECT * from OriginalTerm where ID = :id";

		$args = array(intval($id));
		$result = $db->execPreparedQuery($query, $args, true,Database::FETCH_ASSOC);
		return $result[0];
	}
	#************************************************************************

	public static function addTermToCategory($uid, $tid, $cid){

		$db = Database::instance();
		$query = "INSERT INTO CATEGORY_TERM VALUES (?, ?);";
		$args = array($cid, $tid);

		$db->beginTransaction();
		$db->execPreparedQuery($query, $args, false);

		$logdata = array("termID"=>$tid,"categoryID"=>$cid);
		ActionLog::addLogEntry($db, intval($uid),ActionLog::INSERT ,'CATEGORY_TERM' ,0 ,json_encode($logdata));
		$db->commit();

	}
	
}

  ?>