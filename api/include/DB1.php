<?php
namespace {
	require_once 'constants.php';
}

namespace RTApp{

use PDO;
	use Slim\Log;

class Database
{
    private $host      = RTDB_HOST;
    private $user      = RTDB_USER;
    private $pass      = RTDB_PASSWORD;
    private $dbname    = RTDB_DBNAME;
 
    private $dbh;
    private $error;
	private static $_this;

	// We don't want all entity classes to depend directly on PDO
	const FETCH_ASSOC = PDO::FETCH_ASSOC;
	const FETCH_NUM   = PDO::FETCH_NUM;
	const FETCH_OBJ   = PDO::FETCH_OBJ;
	const FETCH_CLASS = PDO::FETCH_CLASS;
	const FETCH_COLUMN = PDO::FETCH_COLUMN;

	private function __construct(){
        // Set DSN
	    // TODO RTDB_DRIVER.':host='.... also PDO::ATTR_PERSISTENT => RTDB_PERSISTENT
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';charset=utf8';
        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION,
//	        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			PDO::ATTR_EMULATE_PREPARES => false 
        );
        // Create a new PDO instance
        try{
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        // Catch any errors
        catch(\PDOException $e){
            $this->error = $e->getMessage();
	        // TODO: uncomment this....
	        //throw new AppError("Failed to connect to database", $e->getCode(), $e);
	        // (or perhaps our app-specific code that represents database error. It would be logged anyway)
        }
    }
	
	#******************************************
	
	public static function instance() {
		if (empty(self::$_this))
			self::$_this = new self();
		return self::$_this;
	}
	
	#******************************************

	#Trebaju nam transakcije
	public function beginTransaction() {
		return $this->dbh->beginTransaction();
	}
	public function commit() {
		return $this->dbh->commit();
	}
	public function rollback() {
		return $this->dbh->rollBack();
	}
	// FIXME Check for errors in all these functions (can return FALSE) and throw AppException

	#******************************************

	public function exactQuery($query){

		$result = $this->dbh->query($query);
		return $result->fetchAll();
		}
	
	#******************************************
	
	public function countResults($query){

		return $this->dbh->exec($query);
		
	}
	
	#******************************************
	
	
	public function execPreparedQuery($query, $args, $returnResult, $resultType = null,$logger=null){

		try {
			if (null!=$logger) $logger->write('execPreparedQuery:' . $query . ' - with arguments:(' . implode(', ', $args) . ')', Log::DEBUG);
			$stmt = $this->dbh->prepare($query);
			$result = $stmt->execute($args);
		}
		catch(\PDOException $e){
			throw new APIError("Failed to execute query:".$e->getMessage(), -1, $e);
		}

		if ($returnResult)
			return $stmt->fetchAll($resultType);
		else
			return $stmt->rowCount(); // For UPDATE type queries. Return number of rows affected

	}
	#******************************************

	#Vraca ID zadnje unetog elementa
	public function lastID(){

		return $this->dbh->lastInsertId();
	}
	
}
}
?>