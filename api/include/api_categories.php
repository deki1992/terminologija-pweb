<?php
require_once 'Category.php';
require_once 'OriginalTerm.php';

use \RTApp\Category as Category;
use \RTApp\User;
use \RTApp\AccessToken;

#**********************************************************
$app->get('/categories','logAction', function () use ($app) {

	$ret = Category::getAll();

	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->headers->set('Access-Control-Allow-Origin', '*');
	// FIXME Allow Origin could be put in application middleware ($app->add(...)) so it would apply to all requests
	$app->response->setBody($ret);
	
});
#**********************************************************
//Vraca sve kategorije u kojima se nalazi termin sa kljucem :id
$app->get('/categories/term/:id', function ($id) use ($app) {

	$ret = \RTApp\Category::getCategoriesForTerm($id);

	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);

});

#**********************************************************
//Vraca podatke o kategoriji sa kljudem :id
$app->get('/categories/:id', function ($id) use ($app) {


	$ret = \RTApp\Category::getByID($id);
	
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
	
});
#**********************************************************
//Vraca sve termine u datoj kategoriji
$app->get('/categories/:id/terms', function ($id) use ($app) {

	$ret = \RTApp\OriginalTerm::getTermsInCategory($id);

	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);

});
#**********************************************************
//add *existing* term to category
$app->post('/categories/:id/terms/:tid','tokenAuth',function ($id,$tid) use ($app) {

	
	$app->response->setStatus(201);

});

#**********************************************************
$app->post('/categories', 'tokenAuth', function () use ($app) {
	$data = parseRequestBody($app->request());

	if (!isset($data['title']) || !isset($data['description'])) {
		throw new \RTApp\APIError('Missing parameters',\RTApp\APIError::BAD_CONTENT);
	} else {
		$creator = User::getFromSession(AccessToken::$currentToken);
		$title = $data['title'];
		$description = $data['description'];
		$conclusion = isset($data['conclusion']) ? $data['conclusion'] : null;
		$ret = \RTApp\Category::insert($creator->id(), $title, $description, $conclusion);

		$app->response->headers->set('Content-Type', 'application/json');
		$app->response->setBody($ret);
		$app->response->setStatus(201);
	}

});
#**********************************************************
$app->put('/categories/:id','tokenAuth',function($id) use ($app)
{
	$user = User::getFromSession(AccessToken::$currentToken);
	$data = parseRequestBody($app->request());

	$title = isset($data['title'])?$data['title']:null;
	$description = isset($data['description'])?$data['description']:null;
	$conclusion = isset($data['conclusion'])?$data['conclusion']:null;

	if($id == null || $data['id']!=$id) 
		throw new \RTApp\APIError("Attempting to update wrong category?",\RTApp\APIError::INVALID_PARAMS);
	else
	{
		\RTApp\Category::update($user->id(),$id,$title,$description,$conclusion);
	}
});
