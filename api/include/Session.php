<?php

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';
require_once 'logger.php';

use Slim\Log;

class Session extends Entity
{
    private $id;
    private $user_id;
    private $start;
    private $last_active;

    public static function Create($user_id)
    {        
        $db = Database::instance();
        $db->beginTransaction();
        $query = "INSERT INTO SESSIONS (`User_ID`) VALUES ( :user_id )"; //
        $db->execPreparedQuery($query,array(':user_id'=>$user_id),false);
        $sId = $db->lastID();
        $db->commit();
        if(null==$sId) throw new Bug("Couldn't read last created session_id, session not created?");
        return new Session($sId);
    }

    #****************************************************

    private function loadValues($value){
        $this->id = $value[0];
        $this->user_id = $value[1];
        $this->start = $value[2];
        $this->last_active = $value[3];
    }

    public function __construct($sessionId)
{
    $db = Database::instance();
    $query = 'SELECT * FROM SESSIONS WHERE ID=:sessionId';
    $args = array(":sessionId"=>$sessionId);
    $result = $db -> execPreparedQuery($query, $args, true);
    $value = $result[0];

    if($value)
        $this->loadValues($value);
    else
        throw new Bug("Session id not found!");
}

    public function getId()
    {
        return $this->id;
    }

}