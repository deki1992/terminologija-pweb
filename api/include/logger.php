<?php
namespace RTApp;

use Slim\Log;

class Logger
{
	private $fp;

	function __construct() {
		$this->fp = fopen(__DIR__ . '/app.log', 'a');
		//flock($this->fp, LOCK_EX); // LOCK_SH ????
	}

	function __destruct() {
		//flock($this->fp, LOCK_UN);
		fclose($this->fp);
	}

	public function write($message, $level) {
		$fp = $this->fp; // HACK

		switch ($level) {
		case Log::DEBUG:
			fwrite($fp, '[D]'); break;
		case Log::INFO:
			fwrite($fp, '[I]'); break;
		case Log::NOTICE:
			fwrite($fp, '[N]'); break;
		case Log::WARN:
			fwrite($fp, '[W]'); break;
		case Log::ERROR:
			fwrite($fp, '[E]'); break;
		case Log::CRITICAL:
			fwrite($fp, '[C]'); break;
		case Log::ALERT:
			fwrite($fp, '[F]'); break;
		case Log::EMERGENCY:
			fwrite($fp, '[X]'); break;
		default:
			fwrite($fp, '[?]'); break;
		}
		fwrite($fp, ' '.date('c').' ');

		if (is_string($message))
			fwrite($fp, $message . PHP_EOL);
		else
			fwrite($fp, var_export($message, true) . PHP_EOL);
	}
}
