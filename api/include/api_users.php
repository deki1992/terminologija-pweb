<?php
use RTApp\AccessToken;

require_once 'logger.php';
require_once 'User.php';
require_once 'access_token.php';

#*********profil korisnika
$app->get('/user/:id', function ($id) use ($app) {
	$user = new \RTApp\User($id, true);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($user->toJSON());
});

#*****Registracija korisnika*****************************************************
// POST /users i POST /sessions su jedini zahtevi POST-tipa koji ne traze token authentication
// (registracija i prijava korisnika). "POST tip" zahteva = POST, PUT, DELETE. Prakticno sve sto nije GET.
// FIXME ... ne izmisljajmo toplu vodu (a.k.a ponavljamo kod)
$app->post('/user', function () use ($app) {
	$data = parseRequestBody($app->request());
	$user = new \RTApp\User();
	$user->setPassword($data['password']);
	$user->set('username', $data['username']);
	$user->set('email', $data['email']);
	$user->set('firstName', $data['firstname']);
	$user->set('lastName', $data['lastname']);
	$user->set('company', $data['company']);
	if (!empty($data['notes']))
		$user->set('notes', $data['notes']);
	$user->insert();
	$app->status(201); // Created
	return $user->toJSON();
});

$app->put('/user','tokenAuth', function () use ($app)
{
	$data = parseRequestBody($app->request());
	$user = $user = \RTApp\User::getFromSession(AccessToken::$currentToken);
	$user->update($data);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($user->toJSON());
});
