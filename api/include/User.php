<?php
namespace RTApp;

require_once 'Exception.php';
require_once 'access_token.php';
require_once 'DB1.php';
require_once 'Entity.php';

/**
 * @class User
 *
 * Model class for a User.
 */
class User extends Entity
{
	protected static $_table = "user";

	const ROLE_USER = 0;
	const ROLE_ADMIN = 1;
	
	private $id = NULL;
	private $username;
	private $firstName, $lastName;
	private $email;
	private $company;
	private $notes;
	private $passwordHash, $passwordSalt;
	private $userRole = self::ROLE_USER;
	private $active = TRUE;
	private $regDate;

	/**
	 * @method __construct
	 *  Creates an instance of User, either new or existing.
	 * @param int $id
	 *  Identifier of an existing user. If omitted, assumes creating a new user.
	 * @param boolean $fetch
	 *  If set to FALSE, do not automatically set the information from the database.
	 *  Only valid for existing users. Should probably be FALSE if updating or deleting.
	 * @throws APIError if $fetch is TRUE but the user specified by $id does not exist.
	 */
	public function __construct($id = 0, $fetch = TRUE) {
		// This might be overkill, but could be pretty good way to make things "universal"
		// This way all models could use the same mechanism for generating output or accessing database
		// Note that $_fields cannot have references in the default value, that's why we assign it here
		$passwordJoin = "JOIN Password on Password.UserID = ".self::$_table.".ID";
		$this->_fields = array(
			"id"        => array("ID",        &$this->id),
			"username"  => array("username",  &$this->username),
			"firstName" => array("firstname", &$this->firstName),
			"lastName"  => array("lastname",  &$this->lastName),
			"email"     => array("email",     &$this->email),
			"company"   => array("company",   &$this->company),
			"notes"     => array("notes",     &$this->notes),
			"userRole"  => array("role",      &$this->userRole),
			"active"    => array("active",    &$this->active),
			"regDate"   => array("registration_date", &$this->regDate),
			"password"  => array("password",  &$this->passwordHash, $passwordJoin),
			"salt"      => array("salt",      &$this->passwordSalt),
		);

		// Now comes the "regular" instantiation part
		if (!empty($id)) {
			$this->id = $id;
			
			if ($fetch) {
				$this->load(array("username", "firstName", "lastName", "email", "company", "notes",
					"userRole", "active", "regDate"));
/*
				$t = self::$_table;
				$data = DB::instance()->sql_fetch_all(<<<QUERY
SELECT username, firstname, lastname, email, company, notes, role, active, registration_date
FROM $t WHERE id = ?
QUERY
				, 'i', array(&$id)); // , __CLASS__

				if (empty($data))
					throw new APIError("Specified user was not found", APIError::NOT_FOUND);
				$this->fill($data[0]);
*/
			}
		}
	}

	public static function getWithPassword($username) {
		//
	}
	
	// TODO use getWithPassword instead of this! (We would need user data anyway after successful login)
	public static function getPasswordByUsername($username, &$hash, &$salt) {
		$t = self::$_table;
		$data = //DB::instance()->sql_fetch_all(<<<QUERY
			Database::instance()->execPreparedQuery(<<<QUERY
SELECT ID, username, email, role, active, salt, password 
FROM $t JOIN Password ON $t.ID = Password.userID WHERE username = ?
QUERY
		//, 's', array(&$username)
		, array(&$username), TRUE, Database::FETCH_ASSOC);

		if (empty($data)) {
			$hash = "";
			$salt = "";
			return;
		}
		$user = $data[0];
		$hash = $user['password'];
		$salt = $user['salt'];
		return $user;
	}

	public static function getFromSession($access_token) {
		if (is_array($access_token)) // Parsed access token (like AccessToken::$currentToken)
			$tok = $access_token;
		else // Raw encrypted access token, given by browser
			$tok = AccessToken::parse($access_token);
		try {
			$user = new self(intval($tok['user']), FALSE); // 'user' in token is the ID
			$user->load(array("username", "firstName", "lastName", "email", "company", "active", "userRole"));
			return $user;
		} catch (\Exception $e) {
			// No such user, invalidate all its access tokens
			throw new APIError("Invalid access token supplied", $e->getCode());
		}
	}


	// Getters & Setters
	public function id() {
		return $this->id;
	}
	public function registrationDate() {
		return $this->regDate;
	}

	// THIS WOULD NOT BE USED unless we would change getPasswordByUsername.
	// After making User::getByUsername return the user WITH password (like new User() does without
	// password and by ID) this exact function would then be used to verify the user's password
	public function getPassword(&$hash, &$salt) {
		$hash = $this->passwordHash;
		$salt = $this->passwordSalt;
	}

	// TODO And this is another reason why would we really NEED to change getPasswordByUsername
	// 1. If getPassword is not useful, then this is unusable (it needs access to salt and hash)
	// 2. If we would NOT use this method, then we should not have setPassword with plaintext
	//    password either. Functions for hashing and verification should be in same class!
	// 3. Why return the whole user when "just" checking the password? Because we need to authenticate
	//    the user then. This includes INSERT into sessions with user ID AND returning user data
	//    to frontend. Why make the frontend issue one more request to fetch user data after login?!
	// 4. In short, we should probably still return everything. Not "just password" but ALSO the
	//    password. Just not by ID but by username.
	public function verifyPassword($password) {
		// return verifyPasswordHash($password, $this->passwordSalt, $this->passwordHash);
	}
	public static function verifyPasswordHash($password, $salt, $hash) {
		return password_hash($password, PASSWORD_BCRYPT, array(
			"salt" => $salt
		)) == $hash;
	}

	public function setPassword($password) {
		// random_bytes(16)
		$this->passwordSalt = bin2hex(mcrypt_create_iv(16)); // 128-bit salt
		$this->passwordHash = password_hash($password, PASSWORD_BCRYPT, array(
			"salt" => $this->passwordSalt
		));
	}

	public function toJSON($fields = array()) {
		// We'll skip password related fields... toJSON is supposed to return data to frontend.
		if (empty($fields))
			$fields = array("id", "username", "firstName", "lastName", "email", "company", "notes", "userRole",
				"regDate");
		// If we want to return ALL fields, like Entity does by default, we need to set $fields to getFields()
		return parent::toJSON($fields);
	}


	/**
	 * @method insert
	 *  Adds the user to the database.
	 * @return int ID of the newly created user.
	 * @throws Bug if the user ID is not 0, which means user should already exist in the system.
	 */
	public function insert() {
		if (!empty($this->id))
			throw new Bug('Attempting to insert an existing user', __METHOD__);

		$args = array($this->username, $this->email, $this->firstName, $this->lastName,
			$this->company, $this->active, $this->passwordSalt);
		$n1 = $n2 = "";
		if (!empty($this->notes)) {
			$n1 = ", notes";
			$n2 = ",?";
			$args[] = $this->notes;
		}
		$query = "INSERT INTO `user` (username, email, firstname, lastname, company, active, salt $n1)"
			." VALUES (?,?,?,?,?,?,? $n2)";

		$db = Database::instance();
		try {
			$db->beginTransaction();

			$ret = $db->execPreparedQuery($query, $args, false);
			$this->id = $db->lastID();
			if ($ret < 1) {
				$db->rollback();
				// u sustini znaci da korisnik vec postoji. ali to je obradjeno ispod...
				throw new AppError("Database query failed");
			}

			$query = "INSERT INTO Password (UserID, Password) VALUES (?, ?)";
			$args = array($this->id, $this->passwordHash);
			$db->execPreparedQuery($query, $args, false);

			$db->commit();
		} catch (\PDOException $ex) {
			$db->rollback();
			if ($ex->errorInfo[1] == 1062) // MySQL code 1062 = duplicate index.
				throw new APIError("The given username or e-mail address is already registered", APIError::ENTITY_EXISTS);
			else
				throw new AppError("Database query failed");
		}
	}

	/**
	 * @method update
	 *  Saves the changes to the user.
	 * @param array $fields List of fields to update. Defaults to all.
	 *  FIXME tracks which ones were actually changed and defaults to THAT!
	 * @throws Bug if the user ID is 0, which means we have not selected the user to update (but a new user).
	 */
	public function update($fields = array()) {
		if (empty($this->id))
			throw new Bug('No user specified for update', __METHOD__);

		//jedina polja koja za sada mogu da se izmene ovako su
		if(!empty($fields["firstName"])) $this->firstName = $fields['firstName'];
		if(!empty($fields["lastName"]))  $this->lastName  = $fields['lastName'];
		if(!empty($fields["email"]))     $this->email     = $fields['email'];
		if(!empty($fields["company"]))   $this->company   = $fields['company'];
		if(!empty($fields["password"]))  $this->setPassword($fields["password"]);

		// FIXME though it works, all this code should probably not be here in the model, but in api_*.php
		// (main application logic / "controllers"). Model classes should pretty much just execute DB queries.
		if(!empty($fields["email"]) || !empty($fields["password"])) {
			// Changing e-mail address or password needs current password, for security (session token is not enough)
			$t = self::$_table;
			$data = Database::instance()->execPreparedQuery(
				"SELECT salt, password  FROM $t JOIN Password ON $t.ID = Password.userID WHERE ID = ?",
				array(&$this->id), TRUE, Database::FETCH_ASSOC);
			if (empty($data))
				throw new Bug('Update user with password: already authenticated user suddenly not found!', __METHOD__);
			$salt = $data[0]['salt'];
			$hash = $data[0]['password'];
			if (empty($fields["oldpassword"]) || !self::verifyPasswordHash($fields["oldpassword"], $salt, $hash))
				throw new APIError("Incorrect password", APIError::BAD_CREDENTIALS);
		}

		$args = array();
		$updates = array();
		$allowedUpdates = array('firstName', 'lastName', 'email', 'company');
		foreach($fields as $field => $value) {
			//if (!array_key_exists($field, $this->_fields))
			//	continue; // THROW ...
			if (!in_array($field, $allowedUpdates))
				continue;
			$updates[] = $this->_fields[$field][0].' = ?';
			$args[] = $value;
		}
		if (!empty($fields["password"])) {
			$updates[] = "salt = ?";
			$args[] = $this->passwordSalt;
		}

		$args[] = $this->id;
		$logger = new Logger();
		try {
			Database::instance()->beginTransaction();
			$ret = Database::instance()->execPreparedQuery("UPDATE `user` SET ".implode(',', $updates)
				." WHERE ID = ?", $args, false,null,$logger);
			if ($ret < 1) {
				Database::instance()->rollback(); // FIXME Should this be in catch? (Does the db rollback automatically on error?)
				throw new APIError("Specified user was not found", APIError::NOT_FOUND);
			}

			if (!empty($fields["password"])) // Update password. Salt is in the user table, password in a separate table
				Database::instance()->execPreparedQuery("UPDATE Password SET Password = ? WHERE UserID = ?",
					array($this->passwordHash, $this->id), FALSE, NULL, $logger);
			Database::instance()->commit();
		} catch (\PDOException $ex) {
			if ($ex->errorInfo[1] == 1062)
				throw new APIError("E-mail address is already registered on another account", APIError::ENTITY_EXISTS);
			else
				throw new AppError("Database query failed");
		}
	}

	/**
	 * @method delete
	 *  Deletes the user from the database. You should stop using this instance of User after that.
	 * @throws Bug if the user ID is 0, which means we have not selected the user to delete (but a new user).
	 */
	public function delete() {
		if (empty($this->id))
			throw new Bug('No user specified to delete', __METHOD__);

		$args = array($this->id);
		$ret = Database::instance()->execPreparedQuery("DELETE FROM `user` WHERE ID = ?", $args, FALSE);
		if ($ret < 1)
			throw new APIError("Specified user was not found", APIError::NOT_FOUND);
	}

	public function isAdmin(){

		return $this->userRole;
	}




}