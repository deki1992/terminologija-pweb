<?php
/**
 * Created by PhpStorm.
 * User: Luka
 * Date: 23/6/2016
 * Time: 11:25 AM
 */

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';

class Language extends Entity implements \JsonSerializable
{
    private $code;
    private $language;

    #****************************************************
    public function __construct() {}

    #****************************************************
    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    #****************************************************

    public static function getLanguages(){

        $db = Database::instance();
        $query = "SELECT * FROM LANGUAGES; ";

        $result = $db->exactQuery($query);

        $listOfObjects = Array();

        foreach ($result as $value){
            $LanguageObject = new Language();
            $LanguageObject->loadValues($value);
            array_push($listOfObjects, $LanguageObject);


        }

        return json_encode($listOfObjects);

    }
    #****************************************************

    private function loadValues($values){

        $this->code = $values[0];
        $this->language = $values[1];

    }

}