<?php

namespace RTApp;

// Different exception classes are useful for the app's general exception handler. It needs to know
// exception message and error code, but also to know what HTTP status and data (JSON) to return

// Bug is for errors that should not happen at that point (look at that like assertion failure).
class Bug extends \Exception
{
	public function __construct($message, $where = "", $code = 0, \Exception $previous = NULL)
	{
		parent::__construct($message, $code, $previous);

		$this->message = "$where: $message"; // $function::$class
		\Slim\Slim::getInstance()->getLog()->log(\Slim\Log::ERROR, $this);
	}
}

// AppError is for DB and other errors - return HTTP 500, log source errors and error codes
// TRY to never throw \Exception, but AppError instead (make a new \Exception to set as $previous)
// FIXME IS IT GOOD TO DO LOGGING IN EXCEPTION CONSTRUCTOR? Could it be done in the global exception handler?
class AppError extends \Exception
{
	public function __construct($message, $where = "", $code = 0, \Exception $previous = NULL)
	{
		parent::__construct($message, $code, $previous);

		// Use this format for logging exceptions - log this as well as previous exception!
		$this->message = "$where: $message"; // The given code will be part of exception too
		if (!empty($previous)) // empty() is THE universal thing
			$this->message .= " (code ".$previous->getCode()."): ".$previous->getMessage();
		\Slim\Slim::getInstance()->getLog()->log(\Slim\Log::ERROR, $this);

		// Now revert to just the original message (and use its code)
		$this->message = $message;
	}
}

// APIError is for invalid request format, missing arguments, etc etc... return specified HTTP status
// - Actually, APIError could be pretty much any error from bad request to unauthorized or access denied
class APIError extends \Exception
{
	const TOKEN_MISSING   = 2000; // Need to be logged in for this
	const TOKEN_INVALID   = 2001; // Hash validation or similar things failed
	const TOKEN_DENIED    = 2002; // Token for wrong app (if we are going to check user agent, etc)
	const TOKEN_EXPIRED   = 2003; // Session expired (inactive for a long time) or already logged out
	const BAD_CREDENTIALS = 2004; // Used for login / obtaining access token

	const ACCESS_DENIED = 1403; // General permission denied error (HTTP 403)

	const BAD_CONTENT = 0; // Wrong content type (usually expects Form Data or JSON for request body)
	const MISSING_PARAMS = 1;
	const INVALID_PARAMS = 2; // This could be returned for pretty much any form validation failure
	const BAD_ROUTE = 3; // Requested method for requested resource is not supported

	const ENTITY_EXISTS = 409; // HTTP 409 Conflict
	const NOT_FOUND = 404; // Unknown API endpoint

	// Let the error codes here specify the "type" of the error, which means the HTTP status to return:
	// $code from 0 to 999 - Bad request (invalid format, missing required parameters, invalid action)
	// $code from 1000 to 1999 - Access Denied (no permission)
	// $code from 2000 to 2099 - Unauthorized (wrong credentials, missing access token. invalid token?)
	// $code 404 - Not Found :D
	// HTTP status to return - Bad request = 400, Access denied = 403, Unauthorized = 401
	// That's it for now. Later we could divide the codes further and use more HTTP status codes
	public function __construct($message, $code = 0, \Exception $previous = null) // $code = 400
	{
		parent::__construct($message, $code, $previous);
	}

	public function getHTTPStatus() {
		// FIXME this is temporary hack!
		if ( 400 <= $this->code && $this->code < 500)
			return $this->code;
		if (   0 <= $this->code && $this->code < 1000)
			return 400;
		if (1000 <= $this->code && $this->code < 1999)
			return 403;
		if (2000 <= $this->code && $this->code < 2099)
			return 401;
		return 400; // FIXME ?
	}
}
