<?php
// Slim application mode
define('RTAPP_DEVMODE', 'development');

// We will use AES in CBC mode for encryption
define('RTAPP_TOKEN_CIPHER', MCRYPT_RIJNDAEL_128);
define('RTAPP_TOKEN_CIPHER_MODE', MCRYPT_MODE_CBC);
// Key for Access Token encryption. 256-bit.
// IV for AES is 128-bit (so is MD5) and does not need to be top secret.
define('RTAPP_TOKEN_IV', md5('Custom Init Vector', TRUE));
define('RTAPP_TOKEN_KEY', hex2bin('7e01d0734947495875e8ca35ecc4820ff7359761d38694e91f4e758f36cc2bdd'));

define('RTAPP_TOKEN_DURATION', 1800); // 30 minutes. Default duration of short token.

define('RTDB_HOST', 'localhost');
define('RTDB_PORT', 3306);
define('RTDB_USER', 'termuser');
define('RTDB_PASSWORD', 'termuser');
define('RTDB_DBNAME', 'terminology');
define('RTDB_DRIVER', 'mysqli'); // 'mysql'? (what does PDO expect this to be, if it is going to be used?)
define('RTDB_PERSISTENT', TRUE);
