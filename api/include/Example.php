<?php 

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';

class Example extends Entity implements \JsonSerializable{

	protected static $_table = "examples";

	private $id;
	private $example;
	private $category_ID; // trebaju nam ova polja za front end, resurs je nekompletan bez njih ?
	private $term_ID;
	
	#****************************************************
	public function __construct() {
				
	}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }
	
	#****************************************************
	
	public static function addExample( $user_id, $_example, $_category, $_term ){
		
		$db = Database::instance();
		

		$helperQuery = "SELECT COUNT(*) FROM EXAMPLES AS E WHERE E.CATEGORY_ID = (?) AND E.TERM_ID=(?);";
		$helperArgs = array($_category, $_term);

		$query = "INSERT INTO Examples ( Example, Category_ID, term_ID) VALUES ( ?, ?, ?);";
		$args = array( $_example, intval($_category), intval($_term));

		$db->beginTransaction();
		//provera broja primera

		$result = $db->execPreparedQuery($helperQuery, $helperArgs, True);
		$count = $result[0][0];

		if (intval($count) < 5){
			$db->execPreparedQuery($query, $args, False);

		}
		else {
			$db->rollback();
			$ret = array("Message" => "Too many examples already!");
			return json_encode($ret);

		}
		$id = $db->lastID();
		$newExample = Example::getExampleData($db, $id);

		ActionLog::addLogEntry($db, $user_id,ActionLog::INSERT ,'EXAMPLE' ,$newExample->getId(),json_encode($newExample));
		$db->commit();



		return json_encode($newExample);
	}
	
	#****************************************************
	
	private function loadValues($result){
		
		$this->id = $result[0];
		$this->example = $result[1];
		$this->term_ID = $result[2];
		$this->category_ID = $result[3];
		
	}	
	
	#****************************************************
	
	

	#public function getExample($id){}  Mozda nije potrebno
	
	
	#****************************************************
	
	public static function getExamplesForTerm($term_id, $category_id){
		
		$db = Database::instance();
		
		$query = "SELECT E.ID 'Id', E.example 'Example', E.term_ID 'Term' , E.category_ID 'Category' FROM Examples AS E WHERE E.term_ID = (?) AND E.category_ID = (?); ";
		$args = array(intval($term_id), intval($category_id) );
		
		$result = $db->execPreparedQuery($query, $args, True);
		
		$listOfObjects = Array();
		
		foreach ($result as $value){
			$foo = new Example();
			$foo->loadValues($value);
			array_push($listOfObjects, $foo);
		}		
		return $listOfObjects;
		
	}
	
	#****************************************************


	public static function deleteExample($user_id,$id){

		$db = Database::instance();
		$db->beginTransaction();

		$oldExample = Example::getExampleData($db, $id);

		$query = "DELETE FROM EXAMPLES WHERE ID = (?);";
		$args = array($id);

		$db->execPreparedQuery($query, $args, false);
		ActionLog::addLogEntry($db, $user_id,ActionLog::DELETE ,'EXAMPLE' ,$oldExample->getId() ,json_encode($oldExample));
		$db->commit();
	}

	#****************************************************

	public static function updateExample($user_id, $id, $text){

		$db = Database::instance();
		$db->beginTransaction();

		$oldExample = Example::getExampleData($db, $id);

		$query = "UPDATE EXAMPLES SET EXAMPLE = (?) WHERE ID = (?);";
		$args = array($text, $id);

		$db->execPreparedQuery($query, $args, false);
		ActionLog::addLogEntry($db, $user_id,ActionLog::UPDATE ,'EXAMPLE' ,$oldExample->getId() ,json_encode($oldExample));

		$db->commit();
	}
#****************************************************
	public function getId()
	{
		return $this->id;
	}
#****************************************************
	public function getCategoryId()
	{
		return $this->category_ID;
	}
#****************************************************
	public function getTermId()
	{
		return $this->term_ID;
	}
#****************************************************
	public function getExample()
	{
		return $this->example;
	}

	#****************************************************

	private static function getExampleData($db, $id){

		$query = "SELECT E.ID 'Id', E.example 'Example', E.term_ID 'Term' , E.category_ID 'Category' FROM Examples AS E WHERE E.ID = (?); ";
		$args = array($id);
		$result = $db->execPreparedQuery($query, $args, true);

		$exampleData = new Example();
		$exampleData->loadValues($result[0]);

		return $exampleData;

}





	


}
 ?>   