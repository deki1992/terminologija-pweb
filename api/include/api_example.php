<?php
require_once 'Example.php';

use \RTApp\User;
use \RTApp\AccessToken;

#**********************************************************
$app->post('/example', 'tokenAuth', function () use ($app) {

    $user = User::getFromSession(AccessToken::$currentToken);
    $data = parseRequestBody($app->request());

    $ret = \RTApp\Example::addExample($user->id(), $data['Example'], $data['Category'], $data['Term']);

    $app->response->headers->set('Content-Type', 'application/json');
    $app->response->setBody($ret);
});

#**********************************************************

$app->delete('/example/:id', 'tokenAuth', function ($id) use ($app) {

    $user = User::getFromSession(AccessToken::$currentToken);


    if($user->isAdmin()) {

        \RTApp\Example::deleteExample($user->id(),$id);
        $app->response->setStatus(200);
    }
    else
        $app->response->setStatus(403);
});

#**********************************************************


$app->put('/example/:id', 'tokenAuth', function ($id) use ($app) {

    $user = User::getFromSession(AccessToken::$currentToken);

    if($user->isAdmin()) {
         $data = $app->request->put();
         \RTApp\Example::updateExample($user->id(), $id, $data['Example']);
         $app->response->setStatus(200);
    }
    else
        $app->response->setStatus(403);

});

