<?php  

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';

class Comment extends Entity implements \JsonSerializable{

	protected static $_table = "comments";

	private $id;
	private $user_id;
	private $fullname;
	private $translation_id;
	private $created;
	private $content;
	private $parent;
	private $modified;

	#****************************************************
	public function __construct($id=null) {
		if(null!=$id)
		{
			$db = Database::instance();
			$query = 'SELECT C.ID, U.ID, U.username, C.creation_date, C.text, C.reply,C.Translated_term_ID from User AS U JOIN Comments AS C on U.ID = C.Creator where C.ID=?';
			$args = array($id);
			$ret = $db->execPreparedQuery($query, $args,true );
			$this->loadValues($ret[0]);
		}
	}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }
	
	#****************************************************
	
	public static function addComment( $_user_id, $_translation_id, $_text, $_reply = null  ){
	
		$db = Database::instance();
		
		#Ubaciti proveru da li je korisnik ulogovan
		
		#Ako je $_reply == null, onda ce ga intval pretvoriti u 0, pa ga vracamo na null
		$rep = intval($_reply);
		if ($rep == 0)
			$rep = null;

		
		$query = "INSERT INTO Comments ( Creator, Translated_term_ID, Creation_date, Text, Reply) VALUES ( ?, ?, NOW(), ?, ?);";
		$args = array( intval($_user_id), intval($_translation_id), $_text, $rep);

		$db->beginTransaction();
		$db->execPreparedQuery($query, $args, False);

		$id = $db->lastID();

		$newComment = Comment::getComment($db, $id);



		ActionLog::addLogEntry($db, $_user_id,ActionLog::INSERT ,'COMMENT' ,$id ,json_encode($newComment));
		$db->commit();


		return json_encode($newComment);
	}
	
	#****************************************************
	
	public static function getCommentsForTranslation($translation_id){

		$db = Database::instance();

		$query = "SELECT C.ID, U.ID, U.username, C.creation_date, C.text, C.reply, C.LAST_MODIFIED from User AS U JOIN Comments AS C on U.ID = C.Creator where C.Translated_term_ID = (?);";
		$args = array(intval($translation_id) );

		$result = $db->execPreparedQuery($query, $args, True);

		$listOfObjects = Array();

		foreach ($result as $value){
			$foo = new Comment();
			$foo->loadValues($value);
			array_push($listOfObjects, $foo);


		}

		return json_encode($listOfObjects);
		
	}
	#****************************************************

	public static function deleteComment($_user_id, $cid){

		$db = Database::instance();
		$db ->beginTransaction();

		//Nalazenje objekta koji ce se obrisati. Potrebno je zbog log tabele
		$oldComment = Comment::getComment($db, $cid);

		$query = "DELETE FROM COMMENTS WHERE ID = (?);";
		$args = array($cid);
		$db->execPreparedQuery($query, $args, false);

		//pri brisanju resursa, logujemo ID parent resursa ?
		ActionLog::addLogEntry($db, $_user_id,ActionLog::DELETE ,'COMMENT' ,$oldComment->getTranslationID() ,json_encode($oldComment));
		$db->commit();


	}

	#****************************************************

	public static function changeComment($_user_id, $cid, $_text){

		$db = Database::instance();
		$db ->beginTransaction();

		//Nalazenje objekta koji ce se menjati. Potrebno je zbog log tabele
		$oldComment = Comment::getComment($db, $cid);

		$query = "UPDATE COMMENTS SET TEXT = (?), LAST_MODIFIED = NOW() WHERE ID = (?);";
		$args = array($_text, $cid);
		$db->execPreparedQuery($query, $args, false);

		//pri brisanju resursa, logujemo ID parent resursa ?
		ActionLog::addLogEntry($db, $_user_id,ActionLog::UPDATE ,'COMMENT' ,$cid ,json_encode($oldComment));
		$db->commit();


	}

	
	#****************************************************

	private static function getComment($db, $cID){

		$query = "SELECT C.ID, U.ID, U.username, C.creation_date, C.text, C.reply, C.LAST_MODIFIED, C.Translated_term_id from User AS U JOIN Comments AS C on U.ID = C.Creator where C.ID = (?);";
		$args = array($cID);
		$result = $db->execPreparedQuery($query, $args, true);
		$commentData = new Comment();
		$commentData->loadValues($result[0]);
		$commentData->setTranslationID($result[0][7]);
		return $commentData;

	}

	#****************************************************
	private function loadValues($values){

		$this->id = $values[0];
		$this->user_id = $values[1];
		$this->fullname = $values[2];
		$this->created = $values[3];
		$this->content = $values[4];
		$this->parent = $values[5];
		$this->modified = $values[6];
	}	
	
	#****************************************************

	private function setTranslationID($tID){
		$this->translation_id = $tID;
	}
	#****************************************************

	private function getTranslationID(){
		return $this->translation_id;
	}



}

 ?>