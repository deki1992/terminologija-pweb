<?php


namespace RTApp;

use Slim\Log;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';


class ActionLog extends Entity implements \JsonSerializable
{
    private $id = NULL;
    private $user_id;
    private $date;
    private $actiontype;
    private $resourceType;
    private $resourceId;
    private $argument;
    private $username;

    #****************************************************
    public function __construct($id=null)
    {
        if(null!=$id) {
            $db = Database::instance();
            $query = "select L.*, U.username from log L join user U on L.user_id = U.id where L.id = :id";
            $args = array(":id" => intval($id));
            $data = $db->execPreparedQuery($query, $args, true);
            $this->load_values($data[0]);
        }
    }

    const INSERT = 0;
    const UPDATE = 1;
    const DELETE = 2;

    #****************************************************
    public function jsonSerialize()
    {
        return (object) get_object_vars($this);
    }

    #****************************************************

public static function getAll()
{
    $db = Database::instance();
    $logEntries = array();
    $query = "select L.*, U.username from log L join user U on L.user_id = U.id order by L.DATE DESC;";
    $results = $db->ExactQuery($query);
    foreach($results as $result)
    {
        $logEntry = new ActionLog();
        $logEntry->load_values($result);
        array_push($logEntries, $logEntry);
    }

    return $logEntries;
}

    public static function getLog($id){
        $log = new ActionLog($id);
        return $log;
    }

    public static function getPaged($_count, $_page,$actions,$types)
    {
        $db = Database::instance();
        $pagesize = intval($_count);
        $page = intval($_page);
        $logEntries = array("total"=>0,"rows"=>array());
        $logger = new Logger();
        if($pagesize <= 0 || $page <= 0) throw new APIError("Invalid paging parameters",APIError::INVALID_PARAMS);
        $offset = $pagesize * ($page-1);

        $db->beginTransaction();

        $query = 'select SQL_CALC_FOUND_ROWS L.*, U.username from log L join user U on L.user_id = U.id
 where FIND_IN_SET(cast(L.actiontype as char),:actions) and  FIND_IN_SET(L.resourceType,:types) order by L.DATE DESC limit :pagesize offset :offset;';
        $args = array('pagesize'=>intval($pagesize),':offset'=>intval($offset),':actions'=>$actions,':types'=>$types);
        $results = $db->execPreparedQuery($query, $args, true,null,$logger);
        $logEntries["total"] = $db->execPreparedQuery("select FOUND_ROWS()",null,true,Database::FETCH_COLUMN);
        foreach($results as $result)
        {
            $logEntry = new ActionLog();
            $logEntry->load_values($result);
            array_push($logEntries["rows"], $logEntry);
        }

        return $logEntries;
    }

    public static function addLogEntry($db, $_user_id, $_action_type, $_resource_type,$_resource_id, $_arguments) {
        $query = "INSERT INTO `TERMINOLOGY`.`LOG` (user_id,actiontype,resourceType,resourceId,arguments) VALUES (:user_id,:actionType,:resourceType,:resourceId,:arguments);";
        $logger = new Logger();
        $args = array(
            ":user_id"=>$_user_id,
            ":actionType"=>$_action_type,
            ":resourceType"=>$_resource_type,
            ":resourceId"=>$_resource_id,
            ":arguments"=>$_arguments);
        $db->execPreparedQuery($query,$args,false,null,$logger);
    }


    private function load_values($values){
        $this->id = $values[0];
        $this->user_id = $values[1];
        $this->date = $values[2];
        $this->actiontype = $values[3];
        $this->resourceType = $values[4];
        $this->resourceId = $values[5];
        $this->argument = $values[6];
        $this->username = $values[7];
    }

    

}