<?php  
namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';

class Vote extends Entity implements \JsonSerializable{

	protected static $_table = "Votes";

	private $one = 0;
	private $two = 0;
	private $three = 0;
	private $four = 0;
	private $five = 0;

	#****************************************************
	public function __construct() {}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }
	
	#****************************************************
	
	public static function addVote( $userID, $translationID, $vote){
		
		
		#Ubaciti proveru da li ke korisnik ulogovan
		
		#Provera da li je glas u dozvoljenom opsegu
		$voteNum = intval($vote);
		if ($voteNum < 0  ||  $voteNum > 5)
			throw new APIError("Invalid parameter, 'Vote' must be in [1,5]", APIError::INVALID_PARAMS);


		$db = Database::instance();
		$db->beginTransaction();		
		$query = "INSERT INTO Votes ( User_ID, Translated_term_ID, Vote, CREATION_DATE) VALUES ( ?, ?, ?, NOW());";
		$args = array(intval($userID), intval($translationID), $voteNum );
		$db->execPreparedQuery($query, $args, False);

		$newVote = ['user_id' => $userID, 'vote' => $vote, 'translation' => $translationID];

	
		//Vote nema svoj ID, logovacemo translation ID
		ActionLog::addLogEntry($db, $userID,ActionLog::INSERT ,'VOTE' ,$translationID ,json_encode($newVote));
		$db->commit();

		//username nije podesen
		return json_encode($newVote);
	}
	
	#****************************************************
	
	public static function getAverageVote($translationID){
		//weighted rating (WR) = (v � (v+1)) � R + (1 � (v+1)) � C
		// R - prosecna ocena termina
		// v - broj ocena termina
		// C - prosecna vrednost svih ocena u bazi

		$db = Database::instance();
		
		$query = "SELECT AVG(Vote), COUNT(Vote) FROM Votes WHERE Translated_term_ID = (?);";
		$args = array(intval($translationID) );
		
		$result = $db->execPreparedQuery($query, $args, True);
		
		$R =  intval($result[0][0]);
		$v = intval($result[0][1]);

		$query = "SELECT AVG(VOTE) FROM VOTES;";
		$res = $db->exactQuery($query);

		$C = intval($res[0][0]);

		return (($v / ($v + 1)) * $R) + ((1 / ($v + 1)) * $C);

		
	}
	
	#****************************************************
	
	public static function getVotesForTranslation($translation_id){
		
		$db = Database::instance();
		
		$query = "SELECT VOTE AS OCENA, COUNT(VOTE) AS BROJ_OCENA FROM VOTES WHERE TRANSLATED_TERM_ID = (?) GROUP BY VOTE;";
		$args = array(intval($translation_id) );
		
		$result = $db->execPreparedQuery($query, $args, True);
		

		$VoteObject = new Vote();
		foreach ($result as $value){
			$VoteObject->loadValues($value);
		}
		
		return json_encode($VoteObject);
		
	}
	#****************************************************

	public static function getVote($uid, $tid){

		$db = Database::instance();

		$query = "SELECT VOTE FROM VOTES AS V WHERE V.USER_ID = (?) AND V.TRANSLATED_TERM_ID = (?);";
		$args = array($uid, $tid);

		$res = $db->execPreparedQuery($query, $args, True);
		if (empty($res))
			$ret = array('-1');
		else
			$ret = array($res[0][0]);

		return json_encode($ret);
	}


	#****************************************************
		
	private function loadValues($values){
		
		switch ($values[0]){
			case "1":
				$this->one = intval($values[1]);
				break;
			case "2":
				$this->two = intval($values[1]);
				break;
			case "3":
				$this->three = intval($values[1]);
				break;
			case "4":
				$this->four = intval($values[1]);
				break;
			case "5":
				$this->five = intval($values[1]);
				break;

		}
	}	
	
	#****************************************************
	
}
  ?>