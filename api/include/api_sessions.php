<?php
require_once "User.php";

use RTApp\AccessToken as AccessToken;
use RTApp\APIError as APIError;
use RTApp\User as User;

//FIXME this is a hack, Dejane ako mozes implementiraj pravi refresh token (sacuvan sa sesijom)
$app->get('/sessions/user','tokenAuth', function () {
	 $ct = AccessToken::$currentToken;
	$user = User::getFromSession($ct);

	$new_token = AccessToken::create(array(
		"user"       => $user->id(),
		"session_id" => $ct["session_id"],
		"ua"         => $ct["ua"]
		//
	));

	$output = array(
		"user" => $user->toJSON(
			array("id", "username", "firstName", "lastName", "email", "company", "userRole")),//$user,
		"access_token" => $new_token,
		"expires_in"   => RTAPP_TOKEN_DURATION,
	);
	echo json_encode($output);
});

$app->get('/sessions(/:id)', function ($id = 0) {
	throw new APIError("Unsupported operation", APIError::BAD_ROUTE);
});

// PUT /sessions/user
// "user" here means current user session. We will not be using session ID here nor will the frontend know it.
// For now, PUT will only be used to generate new short access token, being given a long one.
$app->put('/sessions(/:id)', 'tokenAuth', function ($id) {
	if ($id != "user")
		throw new APIError("Unsupported operation", APIError::BAD_ROUTE);
	
	// Check if the token is valid AND long-lived (validity longer than default). tokenAuth should check session...
	$longToken = AccessToken::$currentToken;
	if (empty($longToken['validity']))
		throw new APIError("A long-lived access token is required to obtain a new, short one.", APIError::TOKEN_INVALID);

	// Make and return new access token (probably same as supplied, but with empty / default validity)
	$shortToken = $longToken;
	unset($shortToken['validity']);
	$outToken = AccessToken::create($shortToken);

	echo json_encode(array(
		"access_token" => $outToken,
		"expires_in" => RTAPP_TOKEN_DURATION
	));
});

// DELETE /sessions/user
// Again as in PUT. "user" replaces session ID since only operations on current user session are valid.
$app->delete('/sessions(/:id)', 'tokenAuth', function ($id) {
	if ($id != "user")
		throw new APIError("Unsupported operation", APIError::BAD_ROUTE);

	//$user = User::getFromSession(AccessToken::$currentToken);
	$args = array(AccessToken::$currentToken['user'], AccessToken::$currentToken['session_id']);
	\RTApp\Database::instance()->execPreparedQuery(
		"DELETE FROM sessions WHERE ID = ? AND User_ID = ?", $args, false);

	echo json_encode(array(
		"status" => "ok"
	));
});

// POST /sessions
// User login. SHOULD WE USE FORM DATA OR JSON??? (Form data might be easier for Angular)
$app->post('/sessions', function () use ($app) {
	//$data = $app->request->post();
	$data = parseRequestBody($app->request());
	if (empty($data) || empty($data['username']) || empty($data['password']))
		throw new APIError("Missing parameters", APIError::MISSING_PARAMS);

	$token_duration = empty($data['duration']) ? 0 : intval($data['duration']);

	$hash = "";
	$salt = "";
	$userData = User::getPasswordByUsername($data['username'], $hash, $salt);
	if (empty($hash))
		throw new APIError("Bad username or password", APIError::BAD_CREDENTIALS);

	if (!User::verifyPasswordHash($data['password'], $salt, $hash)) // FIXME ?
		throw new APIError("Bad username or password", APIError::BAD_CREDENTIALS);

	$userId = $userData['ID']; // HACK. MAKE A PROPER User::getByUsername ALREADY!
	$session = RTApp\Session::Create($userId);
	$user = new User($userId, FALSE); // User::getFromSession($tokenData)?
	$user->load(array("username", "firstName", "lastName", "email", "company", "active", "userRole"));
	$tokenData = array(
		"user"       => $userId,
		"session_id" => $session->getId(),
		"ua"         => $_SERVER['HTTP_USER_AGENT'],
		//"validity"   => DEFAULT_SESSION_DURATION,
	);
	$encToken = AccessToken::create($tokenData);

	$output = array(
		"user" => $user->toJSON(
			array("id", "username", "firstName", "lastName", "email", "company", "userRole")),//$user,
		"access_token" => $encToken,
		"expires_in"   => RTAPP_TOKEN_DURATION,
	);
	if ($token_duration > 0) {
		$tokenData["validity"] = $token_duration;
		$encToken = AccessToken::create($tokenData);
		$output["refresh_token"] = $encToken;
	}
	echo json_encode($output);
});
