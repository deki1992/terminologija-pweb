<?php 

namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';
require_once 'Comment.php';
require_once 'Vote.php';

class Translation extends Entity implements \JsonSerializable{

	protected static $_table = "Translation";

	private $id;
	private $translation;
	private $language;
	private $org_term;
	private $category;
	private $user_id;
	private $username;
	private $creation_date;
	private $argument;
	private $rating;

	private $category_id;
	private $lang;
	private $term;
	
	
	
	#****************************************************
	public function __construct() {}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }
	
	#****************************************************
	
	public static function addTranslation( $_translation, $_language, $_org_term, $_category_id, $_creator, $_argument ){
		
		$db = Database::instance();
		$query = "INSERT INTO Translation ( Translation, Language, Org_term, Category_id,  creator, argument) VALUES ( ?, ?, ?, ?, ?, ?);";
		$args = array( $_translation, $_language, intval($_org_term), intval($_category_id), intval($_creator), $_argument);
		$db->beginTransaction();
			$db->execPreparedQuery($query, $args, False);
			$id=$db->lastID();

		$newTranslation = Translation::getTranslationData($db, $id);

		ActionLog::addLogEntry($db, $_creator,ActionLog::INSERT ,'TRANSLATION' ,$newTranslation->getID() ,json_encode($newTranslation));
		$db->commit();


		return json_encode($newTranslation);

	}
	#****************************************************
	
	#Moglo bi se pomeriti u Entity, posto je slicna za svaku klasu
	public static function deleteTranslation($_user_id, $_id){
		
		$db = Database::instance();
		$db->beginTransaction();

		$oldTranslation = Translation::getTranslationData($db, $_id);

		$query = "DELETE FROM Translation WHERE ID = (?);";
		$args = array($_id);

		$result = $db->execPreparedQuery($query, $args, False);

		ActionLog::addLogEntry($db, intval($_user_id),ActionLog::DELETE ,'TRANSLATION' ,$_id ,json_encode($oldTranslation));

		$db->commit();
		
	}
	
	#****************************************************
	
	public static function getTranslationDetailed($_id){

		$translationData = new Translation();
		$translationData = Translation::getAllData($_id);
		return json_encode($translationData);
		
		
	}
	
	#****************************************************
	
	private static function getAllData($_id){
		$db = Database::instance();
		
		$translation = Translation::getTranslationData($db, $_id);
		$translation->setRating(\RTApp\Vote::getAverageVote($_id));
		return $translation;
	}
	
	#****************************************************
	public static function getTranslationsForTerm($_term_id, $_category_id){
		
		$db = Database::instance();
		
		$query = "SELECT T.ID FROM Translation AS T WHERE T.org_term = (?) AND T.Category_id = (?);";
		$args = array(intval($_term_id), intval($_category_id) );
		
		$result = $db->execPreparedQuery($query, $args, True);
		
		$listOfObjects = Array();
		
		foreach ($result as $value){

			$foo = Translation::getAllData($value[0]);
			array_push($listOfObjects, $foo);
			
			
		}
		
		return $listOfObjects;
		
	}
	
	#****************************************************
	
	private function load_values($values){
		
		$this->translation = $values[0];
		$this->language = $values[1];
		$this->org_term = $values[2];
		$this->category = $values[3];
		$this->user_id = $values[4];
		$this->username = $values[5];
		$this->creation_date = $values[6];
		$this->argument = $values[7];

		$this->category_id = $values[8];
		$this->lang = $values[9];
		$this->term = $values[10];
	}
	
	#****************************************************
	

	#public function changeTranslation($_id, ...){}

	#****************************************************
	private function setID($_id){
		$this->id = $_id;
		}

	#****************************************************
	private function getID(){
		return $this->id;
	}
	#****************************************************
	private function setRating($_rating){
		$this->rating = $_rating;
	}
	#****************************************************

	private static function getTranslationData($db, $id){

		$query = <<<QUERY
SELECT T.Translation, L.Language, T.Org_term, C.TITLE,  T.CREATOR, U.USERNAME, T.CREATION_DATE, T.ARGUMENT ,
	C.ID AS Category_ID, L.Code AS Lang, OT.Term
FROM ((Translation AS T JOIN USER AS U ON U.ID = T.CREATOR) 
JOIN CATEGORIES   AS C ON C.ID = T.category_id) 
JOIN LANGUAGES    AS L ON T.LANGUAGE = L.CODE 
JOIN ORIGINALTERM AS OT ON OT.ID = T.Org_term
WHERE T.ID = (?);
QUERY;
		$args = array(intval($id));

		$result = $db->execPreparedQuery($query, $args, True);
		$value = $result[0];

		$translationData = new Translation();
		$translationData->setID($id);
		$translationData->load_values($value);
		return $translationData;


	}
	
	
	
	
}


   ?>