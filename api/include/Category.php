<?php   
namespace RTApp;

require_once 'Exception.php';
require_once 'DB1.php';
require_once 'Entity.php';


class Category extends Entity implements \JsonSerializable{
	
	protected static $_table = "categories";
	
	private $id = NULL;
	private $title;
	private $creation_date;
	private $creator;
	private $description;
	private $conclusion;
	#****************************************************
	public function __construct() {}

	#****************************************************
	 public function jsonSerialize() {
        return (object) get_object_vars($this);
    }
	
	#****************************************************
	
	public static function getAll(){
		$db = Database::instance();
		$listOfObjects = Array();
		
		$query = "SELECT * FROM CATEGORIES;";
		
		$result = $db->exactQuery($query);
		foreach ($result as $value){
			$foo = new Category();
			$foo->loadValues($value);
			array_push($listOfObjects, $foo);
		}
		
		return json_encode($listOfObjects);
	}

	#****************************************************
	#Mislim da ovo nije potrebno
	
	public static function getByID($_id){
		
		$db = Database::instance();

		$categoryData = Category::getCategoryData($db, $_id);

		return json_encode($categoryData);
	}
	
	#****************************************************
	
	public static function insert( $_creator,$_title, $_description, $_conclusion){
		
		#Ubaciti proveru korisnika. Bilo bi logicno da samo admin moze dodavati kategorije...
		
		$db = Database::instance();
		
		$query = "INSERT INTO CATEGORIES (title, creation_date, creator, description, conclusion) VALUES (?, NOW(), ?, ?, ?);";
		$args = array($_title, intval($_creator), $_description, $_conclusion);

		$db->beginTransaction();
		$db->execPreparedQuery($query, $args, False);
		$id = $db->lastID();
		$newCategory = Category::getCategoryData($db, $id);
		ActionLog::addLogEntry($db, intval($_creator),ActionLog::INSERT ,'CATEGORY' ,$id,json_encode($newCategory));
		$db->commit();

		return json_encode($newCategory);
		
	}
	
	#****************************************************
	
	#public function deleteByID($_id){}

	public static function update($user_id,$id, $title=null,$description=null,$conclusion=null)
	{
		$db = Database::instance();
		$query = 'UPDATE TERMINOLOGY.CATEGORIES SET
					Title = IFNULL(:title,Title),
					Description = IFNULL(:description,Description),
					Conclusion = IFNULL(:conclusion,Conclusion)
					WHERE id=:id';
		$args = array(':title'=>$title,':description'=>$description,':conclusion'=>$conclusion,':id'=>$id);
		$db->beginTransaction();
		$ret = Database::instance()->execPreparedQuery($query,$args,false);
		//if ($ret < 1) Ovo moze da znaci i 0 rows affected sto je efektivno ako postujes isti sadrzaj
		//	throw new APIError("Specified category was not found", APIError::NOT_FOUND);
		$cat = Category::getCategoryData($db, $id);
		ActionLog::addLogEntry($db, $user_id,ActionLog::UPDATE ,'CATEGORY' ,$id ,json_encode($cat));

		$db->commit();
	}

	
	#****************************************************
	
	private function loadValues($result){
		
		$this->id = $result[0];
		$this->title = $result[1];
		$this->creation_date = $result[2];
		$this->creator = $result[3];
		$this->description = $result[4];
		$this->conclusion = $result[5];
		
	}

	#****************************************************

	private static function getCategoryData($db, $id){

		$query = "SELECT C.ID, C.TITLE, C.CREATION_DATE, U.USERNAME, C.DESCRIPTION, C.CONCLUSION ".
		" FROM CATEGORIES AS C JOIN USER AS U ON C.CREATOR = U.ID WHERE C.ID = (?);";
		$args = array($id);

		$result = $db->execPreparedQuery($query, $args, True);

		$value = $result[0];
		$categoryData = new Category();
		$categoryData->loadValues($value);
		return $categoryData;

	}
#****************************************************

	public static function getNames(Database $db, $categories)
	{
		$args = array(":categories"=>implode(",",$categories ));
		return $db->execPreparedQuery("SELECT Title from Categories where Id in (:categories)",$args ,true,Database::FETCH_COLUMN);
	}

#****************************************************

	public static function getCategoriesForTerm($tid){

		$db = Database::instance();
		$query = "SELECT C.ID, C.TITLE FROM CATEGORIES AS C JOIN CATEGORY_TERM AS CT ON CT.CATEGORY_ID = C.ID WHERE CT.TERM_ID = (?);";
		$args = array($tid);

		$result = $db->execPreparedQuery($query, $args, true);

		return  json_encode($result);

	}

}

?>