<?php
require_once 'Translation1.php';
require_once 'Example.php';


use \RTApp\User;
use \RTApp\AccessToken;

#**********************************************************

$app->post('/translation', 'tokenAuth', function () use ($app) {

	$data = parseRequestBody($app->request());
	$creator = User::getFromSession(AccessToken::$currentToken);
	$ret = \RTApp\Translation::addTranslation($data['translation'], $data['language'], $data['term_id'], $data['category_id'], $creator->id(), $data['argument']);

	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
	
});

#**********************************************************

$app->get('/translation/:id', function ($id) use ($app) {
	

	$ret = \RTApp\Translation::getTranslationDetailed($id);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);

	
});


#**********************************************************

$app->delete('/translation/:id', 'tokenAuth', function ($id) use ($app) {

	$user = User::getFromSession(AccessToken::$currentToken);


	if ($user->isAdmin() || \RTApp\Translation::isOwner($user->id(), $id)) {
		\RTApp\Translation::deleteTranslation($user->id(),$id);
		$app->response->setStatus(200);
	}
	else
		$app->response->setStatus(403);

});



#**********************************************************
// FIXME maybe this should have been /translation/:id/examples.
//FIXME kakve veze ima example sa translation ? gde treba da ide ovaj metod ?
// Or just post all examples together with translation in one POST /translation request
/*
$app->post('/examples', 'tokenAuth', function () use ($app) {
	
	//$data = $app->request->post();
	$data = parseRequestBody($app->request());
	$EX = new \RTApp\Example();
	$EX->addExample($data['example'], $data['category_id'], $data['term_id']);
	$app->response->headers->set('Content-Type', 'application/json');
	$id = $EX->getId();
	$app->response->setBody(json_encode((object)array('id'=>$id)));
	
});
*/