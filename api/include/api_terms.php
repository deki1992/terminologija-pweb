<?php
require_once 'OriginalTerm.php';

use \RTApp\User;
use \RTApp\AccessToken;

#**********************************************************
$app->get('/term/:category/:term', function ($category, $term ) use ($app) {
	$ret = \RTApp\OriginalTerm::getTermDetailed($term, $category);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
	
});

#**********************************************************
$app->post('/term','tokenAuth', function () use ($app) {

	$data = json_decode($app->request->getBody(), true);
	$term = $data['Term'] ?: '';//$app->request->post('Title');
	$language = $data['Language'] ?: '';//$app->request->post('Language');
	$description = $data['Description'] ?: '';// $app->request->post('Description');
	$conclusion = '';//$data['Conclusion'] ?: '';//$app->request->post('Conclusion'); zar ovo nije za update ?
	$creator = User::getFromSession(AccessToken::$currentToken);
	$categories = $data['Categories'] ?: '';//$app->request->post('category');

	// FIXME super je i ovo ali f-ja insert() je sluzila da postavite polja klase ($OT->attribute='value')
	// zatim bi instancna f-ja na osnovu vrednosti polja uradila INSERT i dalje koristila objekat

	$ret = \RTApp\OriginalTerm::insertTerm($term, $language, $creator->id(), $description, null/*$conclusion*/, $categories);
	$app->response->setBody($ret);
	
});
$app->put('/term/:id','tokenAuth',function($id) use ($app){
	$user = User::getFromSession(AccessToken::$currentToken);
	$data = parseRequestBody($app->request());
	$term = $data['term'] ?: null;
	$language = $data['language_code'] ?: null;
	$description = $data['description'] ?: null;
	$conclusion = $data['conclusion'] ?: null;
	//$categories = $data['Categories'] ?: null; TODO ovo mora drugacije
	RTApp\OriginalTerm::updateTerm($user->id(),$id,$term,$language,$description,$conclusion);
});

#**********************************************************

$app->delete('/term/:category/:term', 'tokenAuth', function ($category, $term) use ($app) {
	//FIXME kako RESTful obrisati termin iz kategorije ? ovo sada briše termin i *sve* u vezi njega :S
	$user = User::getFromSession(AccessToken::$currentToken);
	if ($user->isAdmin() || \RTApp\OriginalTerm::isOwner($user->id(), $term)) {
		\RTApp\OriginalTerm::deleteTerm($user->id(),$term, $category);
		$app->response->setStatus(200);
	}
	else
		$app->response->setStatus(403);
	
});

$app->put('term/:id/categories','tokenAuth',function($id) use ($app) {
	$user = User::getFromSession(AccessToken::$currentToken);
	$data = parseRequestBody($app->request());

	$categories = \RTApp\OriginalTerm::getTermCategories();

	foreach($data as $categoryId) {
	}


});

#**********************************************************
/*Ovo je search funkcija po term, category i language
*/
//FIXME ovo ne mora da bude rest, 
$app->get('/terms/:term(/:language(/:category))',function ($term,$language='EN',$category='*') use ($app) {

	$ret = \RTApp\OriginalTerm::searchTerms($term,$language,$category);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
});

#**********************************************************

$app->post('/term/:category/:term','tokenAuth',  function ($category, $term ) use ($app) {

	$user = User::getFromSession(AccessToken::$currentToken);

	\RTApp\OriginalTerm::addTermToCategory($user->id(), $term, $category);
	$app->response->setStatus(201);
	//$app->response->headers->set('Content-Type', 'application/json');
	//$app->response->setBody($ret);

});


