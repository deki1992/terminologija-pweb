<?php
namespace {
	require_once 'constants.php';
}

namespace RTApp {

/**
 * @class DB
 *
 * Database access class. Used by all models for CRUD operations.
 */
class DB
{
	// TODO THIS IS TOTAL NONSENSE. I just wrote convenience functions for prepare-bind-execute(-GET_RESULT-fetch).
	// This could be useful if database driver ever needs to be changed (e.g. switch from MySQL).
	// That way our model classes are independent of how the data is stored; we update that here.
	// But we have NO query building functions, like those that would convert class fields to column names.
	// Each model class would need to do the same thing while building query... Not cool. Don't have other solution ATM.
	private $mysqli;
	private static $_this;

	private function __construct()
	{
		// MySQLi specific
		$host = RTDB_PERSISTENT ? 'p:'.RTDB_HOST : RTDB_HOST;
		$this->mysqli = new \mysqli($host, RTDB_USER, RTDB_PASSWORD, RTDB_DBNAME);

		if (!empty($this->mysqli->connect_errno))
			throw new AppError("Failed to connect to database", $this->mysqli->connect_errno,
				new \Exception($this->mysqli->connect_error, $this->mysqli->connect_errno));
		if (!$this->mysqli->set_charset("utf8"))
			throw new AppError("Failed to initialize database", $this->mysqli->errno,
				new \Exception($this->mysqli->error, $this->mysqli->errno));
	}

	/**
	 * @method instance
	 *  Gets the instance of the database connection, used for further database access.
	 *  Do not instantiate this class directly. Instead, only use this method to access the database.
	 * @return DB The instance of the DB class.
	 * @throws \Exception if the connection to the database fails.
	 */
	public static function instance() {
		if (empty(self::$_this))
			self::$_this = new self();
		return self::$_this;
	}

//	public function insert($table, $data) {
//
//	}

	/**
	 * @method raw_update
	 *  Prepares and executes raw modification query (INSERT, UPDATE or DELETE).
	 * @param string $query the SQL query to execute, with ? marks in place of data.
	 * @param string $data_types a single character for every ? mark in the query, representing the data type:
	 *  'i' for integer types, 'd' for floating-point, 's' for string and 'b' for blobs.
	 * @param array $data zero-indexed array of data to replace ? marks in the query.
	 * @throws \Exception if the query fails for any reason.
	 * @return int Inserted record ID for INSERT queries; number of affected rows for UPDATE or DELETE queries.
	 */
	public function sql_update($query, $data_types, $data) {
		$stmt = $this->mysqli->prepare($query);
		$first = array();
		$first[] = $data_types;
		$params = array_merge($first, $data);
		// The call below equals to $stmt->bind_param($data_types, $data[0], $data[1], ...)
		call_user_func_array(array($stmt, "bind_param"), $params);

		if (!$stmt->execute())
			throw new AppError("Database query failed", $stmt->errno,
				new \Exception($stmt->error . "\nSQL state: ".$stmt->sqlstate, $stmt->errno));
		return $stmt->insert_id || $stmt->affected_rows; // HACK
	}

	/**
	 * @method sql_fetch_start
	 *  Prepares and executes raw SELECT query and returns the cursor to fetch the rows from.
	 * @param string $query the SQL query to execute, with ? marks in place of arguments (conditions, etc).
	 * @param string $arg_types a single character for every ? mark in the query, representing the argument type:
	 *  'i' for integer types, 'd' for floating-point, 's' for string and 'b' for blobs.
	 * @param array $args zero-indexed array of arguments to replace ? marks in the query.
	 * @throws \Exception if the query execution fails for any reason.
	 * @return \mysqli_result Cursor used for binding results and fetching resulting rows one by one.
	 */
	public function sql_fetch_start($query, $arg_types, $args) { //, $result_refs
		$stmt = $this->mysqli->prepare($query);
		$first = array();
		$first[] = $arg_types;
		$params = array_merge($first, $args);
		// The call below equals to $stmt->bind_param($arg_types, $args[0], $args[1], ...)
		call_user_func_array(array($stmt, "bind_param"), $params);

		if (!$stmt->execute())
			throw new AppError("Database query failed", $stmt->errno,
				new \Exception($stmt->error . "\nSQL state: ".$stmt->sqlstate, $stmt->errno));
		//call_user_func_array(array($stmt, "bind_result"), $result_refs);
		return $stmt->get_result();
	}

	/**
	 * @method sql_fetch_all
	 *  Prepares and executes raw SELECT query and returns all results.
	 * @param string $query the SQL query to execute, with ? marks in place of arguments (conditions, etc).
	 * @param string $arg_types a single character for every ? mark in the query, representing the argument type:
	 *  'i' for integer types, 'd' for floating-point, 's' for string and 'b' for blobs.
	 * @param array $args zero-indexed array of arguments to replace ? marks in the query.
	 * @param string $class <b>DO NOT USE.</b> Makes the function return an instance of given class for every row.
	 * @throws \Exception if the query execution or fetching of data fails for any reason.
	 * @return array Zero-indexed array of rows, each an associative array with keys named by returned columns.
	 */
	public function sql_fetch_all($query, $arg_types, $args, $class = "") { //, ...
		//$col_count = strlen($result_types);
//		$row = array($col_count); // Create a dummy array with expected number of result variables
//		$refs = array(); // Now this array will hold references to those variables
//		for ($i=0; $i<$col_count; $i++)
//			$refs[] = &$row[$i];
		$result = $this->sql_fetch_start($query, $arg_types, $args); //, $refs

		if ($result->num_rows <= 0) {
			$result->free();
			return array();
		}

		$output = array();
		// fetch_row will update $row values
		//while ((empty($class) ? $result->fetch_assoc() : $result->fetch_object($class))) {
		for ($i=0; $i<$result->num_rows; $i++) {
			$outRow = empty($class) ? $result->fetch_assoc() : $result->fetch_object($class);
//			$outRow = array();
//			foreach ($row as $value)
//				$outRow[] = $value;
			$output[] = $outRow;
		}
		return $output;
	}
}
}