<?php
namespace RTApp;

require_once "Exception.php";
require_once "DB1.php";

abstract class Entity
{
	protected static $_table = ""; // static
	protected $_fields = array();
	protected $_modified = array();

	protected function getFields() {
		return array_keys($this->_fields);
	}

	protected function getDBFields($which = array()) {
		$out = array();
		if (empty($which)) {
			foreach ($this->_fields as $field)
				$out[] = $field[0];
		}
		else {
			foreach($which as $key)
				if (array_key_exists($key, $this->_fields))
					$out[] = $this->_fields[$key][0];
		}
		return $out;
	}

	protected function getFieldRefs($which = array()) {
		$out = array();
		if (empty($which)) {
			foreach ($this->_fields as &$field)
				$out[] = &$field[1];
		}
		else {
			foreach($which as $key)
				if (array_key_exists($key, $this->_fields))
					$out[] = &$this->_fields[$key][1];
		}
		return $out;
	}

	public function get($field) {
		if (!array_key_exists($field, $this->_fields))
			throw new Bug(__CLASS__." has no such field '$field'.", __METHOD__);
		return $this->_fields[$field][1];
	}

	public function set($field, $value) {
		if (!array_key_exists($field, $this->_fields))
			throw new Bug(__CLASS__." has no such field '$field'.", __METHOD__);
		$this->_fields[$field][1] = $value;
		$this->_modified[$field] = TRUE; // value does not matter...
	}

	// We need this function because json_encode only returns PUBLIC fields
	// We no longer use PUBLIC fields, but getters and setters for every model class.
	public function toJSON($fields = array()) {
		if (empty($fields))
			$fields = array_keys($this->_fields);

		$output = array();
		foreach($fields as $key)
			if (array_key_exists($key, $this->_fields))
				$output[$key] = $this->_fields[$key][1];

		return $output;
	}


	// These functions have ONE JOB: to make us independent of the system used for storing the entities.
	// This means Entity and DB classes should be the ONLY ones using SQL queries. What if the database
	// system needs to be changed? In that case we change just this class and everything else still works.

	// This fills the fields from the database row. ONE ROW expected - changes only this instance!
	public function load($fields = array(), $idField = "id", $idType = 'i') {
		if (empty($fields))
			$fields = array_keys($this->_fields);

		// Using SQL query to load entity
		$q = "SELECT ".implode(',', $this->getDBFields($fields))." FROM ". static::$_table
			." WHERE ".$this->_fields[$idField][0]." = ?";

		$result = //DB::instance()->sql_fetch_start($q, $idType, array($this->_fields[$idField][1]));
			Database::instance()->execPreparedQuery($q, array($this->_fields[$idField][1]), TRUE,
				Database::FETCH_ASSOC);
		
		if (/*$result->num_rows <= 0*/ count($result) == 0) {
			//$result->free();
			throw new APIError("Specified entity was not found", APIError::NOT_FOUND);
		}
		$row = $result[0];//$result->fetch_assoc();
		$this->fill($row);
	}

	// Fills the fields with data from database row
	protected function fill($row) {
		foreach ($this->_fields as &$field)
			if (array_key_exists($field[0], $row))
				$field[1] = $row[$field[0]];
	}


	public static function isOwner($user_id, $resource_id){

		$query = "SELECT CREATOR FROM ". static::$_table . " WHERE ID = (?);";
		$args = array($resource_id);

		$res = Database::instance()->execPreparedQuery($query, $args, True);

		if ($res[0][0] == $user_id)
			return True;
		else
			return False;

	}

	// getMulti (static) will be the challenge:
	// $arg_types = "", $args = array(), $filter = "", $sort = "", $start = 0, $limit = 0, $blabla...

	// Screw it. At ONE point we will HAVE to write SQL queries outside Entity and DB classes. (for JOIN, etc)
	// The ONLY thing that I know of, that solves this problem, is Visual Studio's LINQ + Entity Framework.
	// And that thing is NOT TRIVIAL to write, I guess.
}