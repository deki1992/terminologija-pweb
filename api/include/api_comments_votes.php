<?php
require_once 'Comment.php';
require_once 'Vote.php';

use \RTApp\User;
use \RTApp\AccessToken;

#**********************************************************
// FIXME Maybe we could give translation ID right in the URL - like /translation/:id/comments ?
// After all, comments and votes directly depend on translation. This is one case where such URLs are used in REST.
$app->post('/comment', 'tokenAuth', function () use ($app) {

	$data = parseRequestBody($app->request());
	$user = User::getFromSession(AccessToken::$currentToken);
	

	
	$ret = \RTApp\Comment::addComment($user->id(), $data['translation_id'], $data['content'], isset($data['parent'])?$data['parent']:null);

	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
});

#**********************************************************
// FIXME ...see note for comment (POST /translation/:id/votes). Naravno radi i ovako...
$app->post('/vote', 'tokenAuth', function () use ($app) {
	
	$data =parseRequestBody($app->request());
	$user = User::getFromSession(AccessToken::$currentToken);

	$ret = \RTApp\Vote::addVote($user->id(), $data['Translation'], $data['Vote']);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);
	
});
#**********************************************************

$app->delete('/comment/:id', 'tokenAuth', function ($id) use ($app) {

	$data =parseRequestBody($app->request());
	$user = User::getFromSession(AccessToken::$currentToken);

	if ($user->isAdmin() || \RTApp\Comment::isOwner($user->id(), $id)){

		\RTApp\Comment::deleteComment($user->id(),$id);

		$app->response->setStatus(200);
	}
	else
		$app->response->setStatus(403);

});

#**********************************************************

$app->put('/comment/:id', 'tokenAuth', function ($id) use ($app) {

	$data =parseRequestBody($app->request());
	$user = User::getFromSession(AccessToken::$currentToken);

	if ($user->isAdmin() || \RTApp\Comment::isOwner($user->id(), $id)){

		\RTApp\Comment::changeComment($user->id(),$id, $data['content']);

		$app->response->setStatus(200);
	}
	else
		$app->response->setStatus(403);

});


#**********************************************************
// FIXME This is probably SO WRONG. ID after resource type means resource ID
//FIXME convert to ?translation=tid parameter for filtering a resource collection
// We are not getting all votes or a single vote - use /translation/:id/votes
$app->get('/vote/:translation', 'tokenAuth' ,function ($translation) use ($app) {
	

	$user = User::getFromSession(AccessToken::$currentToken);


	$ret = \RTApp\Vote::getVote($user->id(), $translation);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($ret);

});




$app->get('/translations/:tlid/votes',function($tlid) use ($app)
{

	$result = \RTApp\Vote::getVotesForTranslation($tlid);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($result);
});


$app->get('/translations/:tlid/comments',function($tlid) use ($app)
{

	$result = \RTApp\Comment::getCommentsForTranslation($tlid);
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->setBody($result);
});

