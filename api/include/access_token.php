<?php
namespace {
	require_once 'constants.php';
}

namespace RTApp {

require_once 'Exception.php';

/**
 * @class AccessToken
 *
 * Manages access tokens used for authentication of logged in users.
 */
class AccessToken
{
	public static $currentToken;

	public static function create(
		//$user, $session_id, $ip = '', $ua = '', $validity = 0, $app_id = 0
		$array)
	{
		// Empty will designate the access token as "short", having default duration
		$validity = empty($array['validity']) ? "" : $array['validity'];
		$ip = empty($array['ip']) ? $_SERVER['REMOTE_ADDR'] : $array['ip'];
		$ua = empty($array['ua']) ? "" : $array['ua']; //$_SERVER['HTTP_USER_AGENT'];
		$app_id = empty($array['app_id']) ? '0' : $array['app_id'];

		if (empty($array['user']) || empty($array['session_id']))
			throw new Bug('Missing parameters', __METHOD__); // __CLASS__, __METHOD__

		$user = $array['user'];
		$session_id = $array['session_id'];

		$token_str = "$user|$session_id|$app_id|$validity|$ip|$ua|||";
		$hash = base64_encode(md5($token_str, TRUE));
		return self::encrypt("$hash|$token_str");
	}

	public static function parse($access_token)
	{
		$parts1 = explode('|', self::decrypt($access_token), 2);
		$hash = $parts1[0];
		$token = rtrim($parts1[1], "\0"); // needed if we use mcrypt cbc mode (zero padding)
		if (base64_encode(md5($token, TRUE)) != $hash)
			//return FALSE; // Token hash failure - invalid token!
			throw new APIError("Invalid access token supplied", APIError::TOKEN_INVALID);

		$parts = explode('|', $token);
		return array(
			"user" => $parts[0],
			"session_id" => $parts[1],
			"app_id" => $parts[2],
			"validity" => $parts[3],
			"ip" => $parts[4],
			"ua" => $parts[5],
		);
	}

	static function encrypt($access_token)
	{
		//$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
		$enc = mcrypt_encrypt(RTAPP_TOKEN_CIPHER, RTAPP_TOKEN_KEY, $access_token, RTAPP_TOKEN_CIPHER_MODE, RTAPP_TOKEN_IV);
		return base64_encode($enc);
	}

	static function decrypt($access_token)
	{
		$enc = base64_decode($access_token);
		$dec = mcrypt_decrypt(RTAPP_TOKEN_CIPHER, RTAPP_TOKEN_KEY, $enc, RTAPP_TOKEN_CIPHER_MODE, RTAPP_TOKEN_IV);
		return $dec;
	}

// $e = openssl_encrypt($data, 'aes-256-ctr', $key, 0, $iv);
// $d = openssl_decrypt(base64_decode($e), 'aes-256-ctr', $key, OPENSSL_RAW_DATA, $iv);
}

}
