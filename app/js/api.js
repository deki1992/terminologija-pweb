/**
 * Created by Dejan on 18/05/2016.
 */
'use strict';

angular.module('myApp.sessionService', [])
	.factory('SessionService', ['$http', '$q', '$location', 'apiRoot','oldSession', function ($http, $q, $location, $apiRoot,oldSession) {
        var userSession = {logged_in:false};
		if(oldSession) setUserSession({data:oldSession});
        if(!userSession.logged_in && localStorage.getItem("access_token"))
        {
            //try to refresh from short token
            $http.get($apiRoot+'sessions/user',{headers: {'Authorization': 'Bearer ' + localStorage.getItem("access_token")}}).then(setUserSession);
        }

		var lastError = null;
		var apiRoot = $apiRoot;
		var getSession = function () {
			return userSession;
		};

		var logOut = function () {
			//userSession.logged_in = false;
			//userSession.session_id = null;
			// other variables...
			userSession = {
				logged_in: false,
				session_id: null
			};

            localStorage.removeItem("access_token");
            localStorage.removeItem("access_token_expires");
			return true; //userSession;
		};

		var logIn = function (username, password) {
			lastError = null;
			var d = $q.defer();
			//return
			$http.post(apiRoot + 'sessions', {
				'username': username,
				'password': password
			})
				.then(setUserSession, setErrors(d, "Prijava neuspešna"))
				.then(function () {
					d.resolve({success: true});
				}, function (response) {
					d.reject(response);
				});
			return d.promise;
        };

		function setUserSession(response) {
			if (response.data.user) {
				userSession.logged_in = true;
				userSession.user = response.data.user;
				userSession.email = response.data.user.email;
				userSession.username = response.data.user.username;
				userSession.personName = response.data.user.firstName + ' ' + response.data.user.lastName;
				userSession.institute = response.data.user.company;
                userSession.userRole = response.data.user.userRole;
			}
			if (response.data.access_token) {
				localStorage.setItem("access_token", response.data.access_token);
				localStorage.setItem("access_token_expires",
					Math.floor(Date.now() / 1000) + response.data.expires_in);
			}
			return checkForError(response);
		}

		var register = function (data) {
			lastError = null;
			var d = $q.defer();
			$http.post(apiRoot + 'user', data)
			// .then(setUserSession) // REGISTER API must insert session and return access token for this to work!
				.then(checkForError, setErrors(d, "Registracija neuspešna"))
				.then(function () {
					d.resolve({success: true});
				}, function (response) {
					d.reject(response);
				});
			return d.promise;
		};

		var update = function(userData) {
			var headers = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("access_token")}};
			lastError = null;
			var d = $q.defer();
			$http.put(apiRoot + 'user', userData,headers)
				.then(updateUser, setErrors(d, "Izmena neuspešna")) // notifyFailure
				.then(function () {
					d.resolve({success: true});
				}, function (response) {
					d.reject(response);
				});
			return d.promise;
		};
		// FIXME we could probably do all this API and error handling work in ApiService (created after this one)

		function updateUser(response) {
			if(response.success && response.data && response.data.user)
				userSession.user = response.data.user;
			return checkForError(response);
		}
		
		function checkForError(response) {
			if (response.data.error) {
				return $q.reject({error: data.error});
			}
			else
				return {success: true};
		}

		function setErrors(d, msg) {
			return function (response) {
				// Cak i HTTP statusi greske (4xx, 5xx) se smatraju neuspehom za $http.post
				lastError = msg + ": " + response.statusText;
				if (response.data) {
					// Server je vratio odgovor (koji bi trebalo da sadrzi gresku jer HTTP status nije 2xx)
					d.reject(response.data);
				} else
					d.reject({error: {message: lastError}});
			}
		}

		function notifyFailure(response) { //TODO
		}

		return {
			getUserSession: getSession,
			logoutUser: logOut,
			loginUser: logIn,
			registerUser: register,
			updateUser : update
		};
	}]);

angular.module('myApp.apiService', [])
    .factory('ApiService', ['$http', 'SessionService', '$q', '$location', 'apiRoot', function ($http, SessionService, $q, $location, $apiRoot) {
        var user_session = SessionService.getUserSession();

        var apiRoot = $apiRoot;

        function createAuthHeaders() {
            return {headers: {'Authorization': 'Bearer ' + localStorage.getItem("access_token")}};
        }

        var apiServiceError = function (errorCode,errorMessage)
        {
            //return {errorCode:errorCode,errorMessage:errorMessage};
            return {error: {code: errorCode, message: errorMessage}};
        };
        var notLoggedIn = function()
        {
            alert('Potrebna je prijava na sistem');
            return new apiServiceError('not_logged_in', 'Potrebna je prethodna prijava na sistem');
        };

        var getWithAuth = function (uri) {
            if (!user_session.logged_in) return $q.reject(notLoggedIn());
            else {
                return $http.get(apiRoot + uri, createAuthHeaders());
            }
        };

        var postWithAuth = function (uri, data) {
            if (!user_session.logged_in) return $q.reject(notLoggedIn());
            else {
                return $http.post(apiRoot + uri, data, createAuthHeaders());
            }
        };

        var putWithAuth = function (uri, data) {
            if (!user_session.logged_in) return $q.reject(notLoggedIn());
            else {
                return $http.put(apiRoot + uri, data, createAuthHeaders());
            }
        };

        var deleteWithAuth = function (uri) {
            if (!user_session.logged_in) return $q.reject(notLoggedIn());
            else {
                return $http.delete(apiRoot + uri, createAuthHeaders());
            }
        };

        var get = function(uri)
        {
            return $http.get(apiRoot + uri);
        };

        return {
            getWithAuth: getWithAuth,
            get: get,
            put: putWithAuth,
            post: postWithAuth,
            delete: deleteWithAuth
        }
    }]);


angular.module('myApp.categoryService', [])
    .factory('CategoryService', ['$q','ApiService', '$location', 'apiRoot', function ( $q, ApiService, $location, $apiRoot) {
        var lastError = null;

        var getDetails = function (id) {
            return ApiService.get('categories/' + id);
        };

        var add = function (data) {
            return ApiService.post('categories', data);
        };

        var getAll = function () {
            return ApiService.get( 'categories');
        };

        var remove = function (id) {
            return ApiService.delete('categories/' + id);
        };

        var update = function (id, data) {
            return ApiService.put('categories/' + id, data)
        };

        var getTerms = function (id) {
            return ApiService.get('categories/' + id+'/terms');
        };

        return {
            getCategories: getAll,
            getCategoryDetails: getDetails,
            getCategoryTerms: getTerms,
            addCategory: add,
            deleteCategory: remove,
            updateCategory: update
        };
    }]);

angular.module('myApp.termService', [])
    .factory('TermService', [ '$q', 'ApiService', '$location', function ( $q, ApiService, $location) {
        var lastError = null;

        var getDetails = function (termid,categoryid) {
            return ApiService.get('term/'+categoryid+'/' + termid);
        };

        var add = function (data) {
            return ApiService.post('term', data);
        };

        var getAll = function () {
            return ApiService.get('term');
        };

        var remove = function (term, category) {
            return ApiService.delete('term/'+category+'/'+term);
        };

        var update = function (id, data) {
            return ApiService.put('term/'+id,data)
        };

        var search = function(term,language,category)
        {
            return ApiService.get('terms/'+term+'/'+language+'/'+category);
        };

        var addToCategory = function(term, category){
            return ApiService.post('term/'+category+'/'+term);
        };

        var getCategories = function(tid){
            return ApiService.get('categories/term/'+tid);
        };


        return {
            getTermDetails: getDetails,
            addTerm: add,
            getCategoriesForTerm : getCategories,
            addTermToCategory: addToCategory,
            deleteTerm: remove,
            updateTerm: update,
            search: search
        };
    }]);

angular.module('myApp.translationService', [])
    .factory('TranslationService', ['ApiService', '$q', '$location', function (ApiService, $q, $location) {

        var get = function (tsId) {
            return ApiService.get('translation/'+tsId);
        };

        var del = function(tsId) {
            return ApiService.delete('translation/'+tsId)
        };

        var update = function(data)
        {
            return ApiService.put('translation',data);
        };

        var add = function(data)
        {
            return ApiService.post('translation',data);
        };


        return {
            getTranslation : get,
            deleteTranslation : del,
            updateTranslation : update,
            addTranslation : add
        }
    }]);

angular.module('myApp.commentService', [])
    .factory('CommentService', ['ApiService', '$q', '$location', 'apiRoot', function (ApiService, $q, $location, $apiRoot) {
        var lastError = null;

        var getDetails = function (cId) {
            return ApiService.get('comments/' + cId);
        };

        var add = function (data) {
            return ApiService.post('comment', data);
        };

        var getAll = function (tId) {
            return ApiService.get('translations/' + tId + '/comments');
        };

        var remove = function (cId) {
            return ApiService.delete('comment/' + cId);
        };

        var update = function (cId, data) {
            return ApiService.put('comment/'+cId, data);
        };


        return {
            getComment: getDetails,
            addComment: add,
            getAllComments: getAll,
            deleteComment: remove,
            updateComment: update
        };
    }]);

angular.module('myApp.exampleService', [])
    .factory('ExampleService', ['ApiService', '$q', '$location', 'apiRoot', function (ApiService, $q, $location, $apiRoot) {
        var lastError = null;


        var add = function (data) {
            //window.alert(data);
            return ApiService.post('/example', data);
        };

        var remove = function (eId) {
            return ApiService.delete('/example/' + eId);
        };

        var update = function (eId, data) {
            return ApiService.put('/example/' + eId, data);
        };


        return {

            addExample: add,
            deleteExample: remove,
            updateExample: update
        };
    }]);

angular.module('myApp.voteService', [])
    .factory('VoteService', ['ApiService', '$q', '$location', 'apiRoot', function (ApiService, $q, $location, $apiRoot) {
        var lastError = null;


        var add = function (data) {
            //window.alert(data);
            return ApiService.post('vote', data);
        };

        var getV = function(translationID){
            return ApiService.getWithAuth('vote/'+translationID);
        }

        var getAll = function(translationID){
            return ApiService.get('translations/'+translationID+"/votes");
        }

        var update = function (data) {
            return ApiService.put('vote',  data);
        };


        return {

            addVote: add,
            getVote: getV,
            getVotes: getAll,
            updateVote: update
        };
    }]);

angular.module('myApp.utilityService',[])
    .factory('UtilityService',['ApiService','$q',function(ApiService,$q) {
        var languages = null;
        var getLanguages = function()
        {
            var d = $q.defer();

            if(languages) { d.resolve(languages); return d.promise;}
            else return ApiService.get('languages').then(function(response){
                languages = response.data; return languages});
        }

        return {
            getLanguages: getLanguages
        };

    }]);

angular.module('myApp.activityLogService',[])
    .factory("ActivityLogService",['ApiService','$q',function(ApiService,$q)
{

    var getPagedActivities = function(pagesize,page,types,actions)
    {
        return ApiService.getWithAuth('logs?pagesize='+pagesize+'&page='+page+'&actions='+actions.join(',')+'&types='+types.join(','));
    }

    var getLatestActivities = function ()
    {
        return getPagedActivities(10,1);
    }

    var allActivities = function()
    {
        return ApiService.getWithAuth('logs');
    }

    var getLogEntry = function(id)
    {
        return ApiService.getWithAuth('logs/'+id);
    }

    return    {
        getActivities : getPagedActivities,
        getLatest : getLatestActivities,
        getAll: allActivities,
        getActivity:getLogEntry
    };


}]);