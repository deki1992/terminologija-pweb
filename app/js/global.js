/**
 * Created by Nemanja on 18.6.2016.
 */

//Construct new search parameters with optional default values
var SearchParams = function(term,language,category)
{
    return {term:term,language:language || 'EN',category: category || '*'}
};

var Example = function(id, categoryId, termId, example)
{
    return {
        id:id,
        category_id:categoryId,
        term_id:termId,
        example:example
    }
}
