/**
 * Created by Dejan on 18/05/2016.
 */
'use strict';

angular.module('myApp.menuView', [])

.controller('MenuCtrl', ['$scope', 'SessionService', function($scope, SessionService) {
	// Ova linija mozda nije potrebna zbog $watch ispod...
	$scope.user_session = SessionService.getUserSession();
	// ...watch sluzi tome da bilo koji kontroler / pogled moze da menja svoj $scope u odnosu na podatke iz servisa.
	// Ovde je to znacajno jer Login Controller zapravo poziva f-je servisa koje menjaju sesiju, a ovaj je samo cita.
	$scope.$watch(function(){
		return SessionService.getUserSession();
	}, function(session){
		$scope.user_session = session;
	}, true);
	// TODO DRUGI NACIN za ovo je dodeliti SessionService nekoj promenljivoj u $scope. Pa u izrazima to koristiti!
	// Cek, pa onda u slucaju ovog menija mogu i samo f-ju da dodelim... pa bi izraz u html bio user_session().nesto...

	$scope.logoutUser = SessionService.logoutUser;
}]);
