/**
 * Created by Nemanja on 15.6.2016.
 */


angular.module('myApp.translation', ['ngRoute', 'jkAngularRatingStars', 'chart.js'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/translation/:id',
                {
                    templateUrl: 'translation/translation.html',
                    controller: 'TranslationCtrl'
                })
    }])
    .controller('TranslationCtrl', ['$scope', 'TranslationService', 'CommentService' , 'VoteService', 'SessionService', '$routeParams', '$location',
        function ($scope, TranslationService, CommentService,VoteService, SessionService,  $routeParams, $location) {

            //--------------------------------------------------
           var UpdateErrors = function (response)
            {
                if(response)
                    $scope.errors.push(response!=null?response.data!=null?response.data.message!=null?response.data.message:response.data:response:'Unspecified error');
            };
            //--------------------------------------------------
            var TranslationComment = function()
            {
                return {};
            };
            //--------------------------------------------------
            var UpdateTranslationDetails = function (response) {
                $scope.translation = response.data;
            };
            //--------------------------------------------------
            $scope.errors = [];
            $scope.translation = {id: $routeParams.id}
            $scope.newComment = null;
           // $scope.mode = 'view';

            if($scope.translation.id != null)
                TranslationService.getTranslation($scope.translation.id).then(UpdateTranslationDetails, UpdateErrors).catch(UpdateErrors);

            //--------------------------------------------------
            function ShowConfirmation(response)
            {
                alert('Changes saved');
            }
            //--------------------------------------------------
            function UpdateComments(response)
            {
               // $scope.commentsData.push(angular.toJson(response.data));
                //FIXME Ovo je samo trenutno resenje. Potrebno je da se komentari azuriraju bez ponovnog ucitavanja stranice
                window.location.reload();
            }
            //--------------------------------------------------
            $scope.commentControls =
            {

                saveComment: function(commentData)
                {

                        CommentService.addComment(commentData).then(UpdateComments,UpdateErrors).catch(UpdateErrors);
                },

                removeComment: function(commentID){

                    CommentService.deleteComment(commentID).then(UpdateComments, UpdateErrors).catch(UpdateErrors);
                },

                updateComment : function(commentData){
                        var obj = angular.fromJson(commentData);
                        var id = obj['id'];
                        var content = {content : obj['content']};

                        CommentService.updateComment(angular.toJson(id), angular.toJson(content)).then(UpdateComments, UpdateErrors).catch(UpdateErrors);
                }
            };

            //--------------------------------------------------
            var session = SessionService.getUserSession();

            //Ucitavanje komentara
                CommentService.getAllComments($scope.translation.id).then(function (response) {

                    $scope.commentsData = response.data;

                    //Postavljamo informaciju o tome da li je komentar vlasnistvo trenutno ulogovanog korisnika
                    //Potrebno je za modifikovanje i brisanje komentara

                    if (session['logged_in'] == true) {
                        $scope.commentsData.forEach(function (comment) {
                            if (comment['fullname'] == session['username']) {
                                comment['created_by_current_user'] = true;
                            }
                            else
                                comment['created_by_current_user'] = false;
                        });
                    }

                });

            //--------------------------------------------------


            if (session.logged_in == true){
                $scope.showRating = true;

                $scope.readOnlyOption = false;
                $scope.ratingOption = 0;

                var translationID = $location.url().substring($location.url().lastIndexOf("/")+1);
                VoteService.getVote(translationID).then(function(response){
                    var data = parseInt(response.data);

                    if (data != -1){
                        $scope.readWriteOption = true;
                        $scope.ratingOption = data;
                    }
                });
            }
            else
                $scope.showRating = false;



            $scope.addVote = function(rating){

                var translationID = $location.url().substring($location.url().lastIndexOf("/")+1);

                var data = {Translation : translationID, Vote : rating};
                var jsonData = angular.toJson(data);
                VoteService.addVote(jsonData);
                window.location.reload();
            }

            $scope.labels = ['1', '2', '3', '4', '5'];
            $scope.series = ['Glasovi'];
            VoteService.getVotes($scope.translation.id).then(function (response){

                var data = response.data;
                $scope.voteData = [[data['one'], data['two'], data['three'], data['four'], data['five']]];

            });





        }])
    .directive('translation', function () {
        return {
            restrict: 'E',
            scope: {
                mode: "@",
                data: '=',
                controls: "="
            },
            templateUrl: 'translation/translationTemplate.html',
            link: function ($scope) {
                $scope.languages = $scope.$parent.languages; //hack
            }

        }
        }
    )
//Direktiva koja upravlja prikazom komentara
    //Zasniva se na jquery-comments
    .directive('comments', function() {
        return {
            restrict: 'A',
            scope : false,
            controller: "TranslationCtrl",
            link: function($scope) {
                $('#comments-container').comments({
                    profilePictureURL: 'https://app.viima.com/static/media/user_profiles/user-icon.png',
                    enableUpvoting: false,
                    enableEditing: true,
                    getComments: function (success, error) {

                        $scope.$watch('commentsData', function(commentsData) {
                            if(commentsData != null){
                               success(commentsData);}
                        });

                    },

                    postComment: function(commentJSON, success, error) {
                        var data = {translation_id : $scope.translation.id, content : commentJSON['content'], parent : commentJSON['parent']};
                       // alert(angular.toJson(data));
                            $scope.commentControls.saveComment(data);

                    },

                    putComment: function(commentJSON, success, error) {

                        var data = {id : commentJSON['id'], content : commentJSON['content']};
                        $scope.commentControls.updateComment(angular.toJson(data));

                    },

                    deleteComment: function(commentJSON, success, error) {
                        $scope.commentControls.removeComment(angular.toJson(commentJSON['id']));
                    }


                });

            }
        };
    });

