/**
 * Created by Nemanja on 26.6.2016.
 */
'use strict';

angular.module('myApp.activityLog', [])

    .controller('ActivityLogCtrl', ['$scope', 'SessionService', 'ActivityLogService','userSession',function($scope, SessionService,ActivityLogService,userSession) {

        $scope.error = null;
        $scope.paginator = {
            page:1,
            pagesize:10,
            pages : 1,
            pagesizes:[10,20,50],
            back : function(count)
            {
                $scope.paginator.page = Math.max(this.page -count,1);
                $scope.applyFilters();
            },
            fwd: function(count)
            {
                $scope.paginator.page = Math.min(this.pages, this.page+count);
                $scope.applyFilters();
            },
            first: function()
            {
                $scope.paginator.page = 1;
                $scope.applyFilters();
            },
            last: function()
            {
                $scope.paginator.page = this.pages;
                $scope.applyFilters();
            },
            updateRecordCount: function(count) {
                $scope.paginator.pages = Math.ceil(count / this.pagesize) || 1;
                var npage = Math.min(this.page,this.pages);
                if(npage != this.page) {
                    this.page = npage;
                    $scope.applyFilters();
                }
            }

        };

        $scope.applyFilters = function()
        {
            $scope.activities = [];
            ActivityLogService.getActivities(
                $scope.paginator.pagesize,
                $scope.paginator.page,
                $scope.typeFilters.filter(activeOnly).map(function(item){return item.resourceType}),
                $scope.actionFilters.filter(activeOnly).map(function(item){return item.actionType}))
                .then(updateActivities, updateErrors).catch(updateErrors);
        };

        //init default filters;
        $scope.typeFilters = [
            {resourceType:'CATEGORY',id:'cat',name:'Kategorije',active:true},
            {resourceType:'TERM',id:'term',name:'Termini',active:true},
            {resourceType:'EXAMPLE',id:'ex',name:'Primeri',active:true},
            {resourceType:'TRANSLATION',id:'trs',name:'Prevodi',active:true},
            {resourceType:'COMMENT',id:'com',name:'Komentari',active:true},
            {resourceType:'VOTE',id:'vot',name:'Glasovi',active:false}
        ];

        $scope.actionFilters = [
            {actionType:0,name:"Dodavanja",active:true},
            {actionType:1,name:"Izmene",active:true},
            {actionType:2,name:"Brisanja",active:true}
        ];


        //init default paginator
        if((userSession.user || {userRole:-1}).userRole != 1)
            $scope.error = "You need to be an administrator to access this area";
        //access to log listing is barred server-side, go ahead and set your user level in the client :)
            /*FIXME SessionService je lenjo đubre koje će veselo da vrati praznu sesiju ako se poziv za refresh ne resolve-uje pre prvog getSession poziva */
        else {
            $scope.activities = [];
            ActivityLogService.getActivities(
                $scope.paginator.pagesize,
                $scope.paginator.page,
                $scope.typeFilters.filter(activeOnly).map(function(item){return item.resourceType}),
                $scope.actionFilters.filter(activeOnly).map(function(item){return item.actionType}))
                .then(updateActivities, updateErrors).catch(updateErrors);
        }

        function activeOnly(item) {
            return item.active;
        }

        var getActiveTypes = function()
        { return };



        $scope.activeTypes = function(item)
        {
            for(var i = 0; i < $scope.typeFilters.length; i++) {
                if ($scope.typeFilters[i].active && item.resourceType == $scope.typeFilters[i].resourceType)
                    return item;
            }
        };

        $scope.activeActions = function(item)
        {
            for(var i = 0; i < $scope.actionFilters.length; i++) {
                if ($scope.actionFilters[i].active && item.actiontype == $scope.actionFilters[i].actionType)
                    return item;
            }
        };


        function updateActivities(response) {
            $scope.activities = response.data.rows;
            $scope.activities.forEach(function (item) {
                item.argument = angular.fromJson(item.argument);
            }); // FIXME this is lazy
            $scope.paginator.updateRecordCount(response.data.total[0])
        }

        function updateErrors(response) {
            $scope.error = response.data.error.message; //TODO fix error handling
        }



    }]).filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int  
        return input.slice(start);
    }})

    .directive('jsonDisplay', function() {
        function link(scope, element, attrs) {
            var $ = window.jQuery;
            var transforms = {
                'object':{'tag':'div','class':'package ${show} ${type}','children':[
                    {'tag':'div','class':'header','children':[
                        {'tag':'div','class':function(obj){
                            var classes = ["arrow"];
                            if( getValue(obj.value) !== undefined ) classes.push("hide");
                            return(classes.join(' '));
                        }},
                        {'tag':'span','class':'name','html':'${name}'},
                        {'tag':'span','class':'value','html':function(obj) {
                            var value = getValue(obj.value);
                            if( value !== undefined ) return(" : " + value);
                            else return('');
                        }},
                        {'tag':'span','class':'type','html':'${type}'}
                    ]},
                    {'tag':'div','class':'children','children':function(obj){return(children(obj.value));}}
                ]}
            };

            function getValue(obj) {
                var type = $.type(obj);
                //Determine if this object has children
                switch(type) {
                    case 'array':
                    case 'object':
                        return undefined;
                    case 'function':
                        return 'function';
                    case 'string':
                        return "'" + obj + "'";
                    default:
                        return obj;
                }
            }
            //Transform the children
            function children(obj){
                var type = $.type(obj);
                //Determine if this object has children
                switch(type) {
                    case 'array':
                    case 'object':
                        return json2html.transform(obj, transforms.object);
                    default:
                        //This must be a literal
                        break;
                }
            }
            function convert(name, obj, show) {
                var type = $.type(obj);
                if(show === undefined) show = 'closed';
                var children = [];
                //Determine the type of this object
                switch(type) {
                    case 'array':
                        //Transform array
                        //Iterate through the array and add it to the elements array
                        var len=obj.length;
                        for(var j=0;j<len;++j){
                            //Concat the return elements from this objects tranformation
                            children[j] = convert(j,obj[j]);
                        }
                        break;
                    case 'object':
                        //Transform Object
                        var j = 0;
                        for(var prop in obj) {
                            children[j] = convert(prop,obj[prop]);
                            j++;
                        }
                        break;
                    default:
                        //This must be a literal (or function)
                        children = obj;
                        break;
                }
                return {'name':name, 'value':children, 'type':type, 'show':show};
            }
            function regEvents() {
                $(element).find('.header').click(function(){
                    var parent = $(this).parent();
                    if(parent.hasClass('closed')) {
                        parent.removeClass('closed');
                        parent.addClass('open');
                    } else {
                        parent.removeClass('open');
                        parent.addClass('closed');
                    }
                });
            }

            $(element).addClass("jsonPreview");
            try {
                var realData = angular.fromJson(scope.data);
                if (realData && realData.hasOwnProperty('_fields'))
                    delete realData['_fields'];
                if (realData && realData.hasOwnProperty('_modified'))
                    delete realData['_modified'];
                for (var key in realData) {
                    if (realData[key]) {
                        delete realData[key]['_fields'];
                        delete realData[key]['_modified'];
                    }
                }
                if (realData != null) {
                    $(element).json2html(
                        convert(scope.title ? scope.title : 'data', realData, scope.open ? 'open' : 'closed'),
                        transforms.object);
                    regEvents();
                }
                else
                    $(element).html("NULL");
            }
            catch (e) {
                console.warn(e);
                if (typeof scope.data == "string")
                    $(element).html(angular.toJson(scope.data));
                else
                    $(element).html(scope.data);
            }
        }

        return {
            scope: {
                data: '=jsonDisplay',
                open: '=jsonOpen',
                title: '=jsonTitle'
            },
            link: link
        };
    });
