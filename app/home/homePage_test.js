'use strict';

describe('myApp.homePage module', function() {

  beforeEach(module('myApp.homePage'));

  describe('homepage controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var homeCtrl = $controller('HomeCtrl');
      expect(homeCtrl).toBeDefined();
    }));

  });
});