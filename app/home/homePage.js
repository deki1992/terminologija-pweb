'use strict';

angular.module('myApp.homePage', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl: 'home/home.html',
		controller: 'HomeCtrl'
	});
}])

.controller('HomeCtrl', ['$scope','SessionService',function($scope,SessionService) {
	$scope.userSession = SessionService.getUserSession();
}]);