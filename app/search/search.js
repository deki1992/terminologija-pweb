/**
 * Created by Nemanja on 12.6.2016.
 */
'use strict';

angular.module('myApp.search',['ngRoute'])
.controller('SearchCtrl', ['$scope','TermService','CategoryService','UtilityService','$routeParams','$location',
    function ($scope,TermService,CategoryService,UtilityService,$routeParams,$location) {
        $scope.errors = [];
        $scope.searchResults = [];
        $scope.params = new SearchParams($routeParams.term,$routeParams.language,$routeParams.category);
        $scope.categories = [{id:'*',title:'(Bilo koja)'}];
        $scope.languages = [{code:'*',title:'(svi jezici)'}];
        CategoryService.getCategories().then(function(response) {
            $scope.categories = [{id:"*",title:'(Bilo koja)'}].concat(response.data);
        });

        UtilityService.getLanguages().then(function(response){
           $scope.languages =  [{code:'*',language:'(svi jezici)'}].concat(response);
        });

        $scope.d = {search : function(params)
        {
            TermService.search(params.term,params.language,params.category)
                .then(UpdateSearchResults).catch(SetErrors);
        }};

        if($scope.params.term)
            $scope.d.search($scope.params);

       function UpdateSearchResults(response)
       {
            $scope.searchResults = response.data;
       }
        
        function SetErrors(response)
        {
            $scope.errors.push(response.data.message);
        }

        $scope.showItem = function(termId,categoryId)
        {
            $location.path('term/'+termId+'/'+categoryId);
        }


    }])

.controller('QuickSearchCtrl',['$scope','$location', function($scope,$location)
{
    $scope.params = {term:null,language:'EN',category:'*'};
    $scope.d = {search : function(params)
    {
        $location.path("/search/"+params.term+'/EN/*');
    }}
}

])
    .directive('search',function()
    {
        return {
            restrict: 'E',
            scope: { mode:"@",
                params:'=',
                searchfn:'&',
                categories:'=',
                languages:'='
            },
                templateUrl:'search/searchTemplate.html'
        }
    });


