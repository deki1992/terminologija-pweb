/**
 * Created by Nemanja on 5.6.2016.
 */
'use strict';

angular.module('myApp.term', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/addTerm',
                {
                    templateUrl: 'term/addTerm.html',
                    controller: 'AddTermCtrl'
                })
            .when('/term/:categoryId/:termId',
                {
                    templateUrl: 'term/term.html',
                    controller: 'TermCtrl',
                    resolve: { languages: ['UtilityService',
                        function(us)
                        {
                            return us.getLanguages();
                        }]}
                });
    }])

    .controller('TermCtrl', ['$scope', 'SessionService', 'CategoryService', 'TermService', 'ExampleService','TranslationService','UtilityService','$location', '$routeParams','languages', function ($scope, SessionService, CategoryService, TermService, ExampleService, TranslationService, UtilityService, $location, $routeParams,languages) {
        $scope.error = null;
        $scope.termId = $routeParams.termId;
        $scope.categoryId = $routeParams.categoryId;

        var userSession = SessionService.getUserSession();
        $scope.canEdit = false;
        $scope.canDelete = false;
        $scope.languages = languages;

        $scope.categories = [];
        $scope.details = null;
        $scope.editDetails = null;
        $scope.mode = 'view';
        $scope.newTranslation = null;

        $scope.editTerm = function()
        {
                $scope.editDetails = angular.copy($scope.details);
        };

        $scope.deleteTerm = function()
        {
            if(confirm('Obrisati termin?'))
            {
                TermService.deleteTerm(termId).then(returnToCategory).catch(DisplayError);
            }
        };

        function DisplayError(response)
        {
            $scope.error = response.data.errorMsg;
        }


        function returnToCategory(response)
        {
            alert('Termin je obrisan');
            $location.path('category/'+$scope.categoryId);
        }
        $scope.cancelEdit = function()
        {
            if(!confirm('Odbaciti izmene?')) return;
            $scope.editDetails = null;
        };

        $scope.showAddExample = false;
        $scope.examplesOptions =
            [
                {
                    name:'Dodaj primer',
                    action: function () {
                        if ($scope.showAddExample == false)
                            $scope.showAddExample = true;
                        else
                            $scope.showAddExample = false;
                    }
                }
            ];

        $scope.addExample = function(){
            var data = {Example : $scope.newExampleText, Category: $scope.categoryId, Term : $scope.termId};
            var JsonData = angular.toJson(data);

                 ExampleService.addExample(JsonData).then(function(response){
                     alert("Example added!");
                     $scope.details.examples.push(response.data);
                     $scope.showAddExample = false;
                     $scope.newExampleText = "";
                 });


        };

        $scope.deleteExample = function (id){

            if(confirm('Obrisati primer čiji je ID = ' + id +'?')){
             ExampleService.deleteExample(id).then(function(response){
               alert("Example deleted!");
                 $scope.details.examples = $scope.details.examples.filter(function(item){return item.id!=id;});

             });}
        };


        $scope.updateTerm = function(termDetails)
        {
            TermService.updateTerm($scope.termId,termDetails).then(function(response) {
                if (response.status == '200') {
                    $scope.details = angular.copy($scope.editDetails)
                    $scope.editDetails = null;
                    alert('Details updated!')
                }
            });
        };
        
        CategoryService.getCategories().then(function(response){$scope.categories = response.data},updateErrors).catch(updateErrors);
        TermService.getCategoriesForTerm($scope.termId).then(function(response){
            $scope.otherCategories = response.data;
            $scope.otherCategories = $scope.otherCategories.filter(function(item){return item.ID!=$scope.categoryId;});
        });

        $scope.showCategories = false;
        $scope.showCategoriesList = function(){
            if ($scope.showCategories == false)
                $scope.showCategories = true;
            else
                $scope.showCategories = false;
        };

        $scope.changeCategory = function (cid){
            $location.path("term/"+cid+"/"+$scope.termId);
        };

        $scope.translationOptions =
        {
            addTranslation: function () {
                $scope.newTranslation = {term_id:$scope.termId,category_id:$scope.categoryId};
            },
            cancel: function (shouldConfirm) {
                if(!shouldConfirm || confirm('Otkazati dodavanje?'))
                    $scope.newTranslation =  null;
            },
            save: function (translation) {
                if (!translation.id)
                    TranslationService.addTranslation(translation).then(UpdateTranslations).catch(updateErrors);
                else
                    TranslationService.updateTranslation(translation).then(ShowConfirmation).catch(updateErrors);
            },
            delete: function(translationId)
            {
                if(confirm('Jeste li sigurni da želite da obrišete ovaj predlog prevoda?'))
                {
                    TranslationService.deleteTranslation(translationId).then(function(response)
                    {
                        $scope.details.translations = $scope.details.translations.filter(function(item){return item.id!=translationId;});
                    });
                }
                else return false;
            }
        };
        function UpdateTranslations(response)
        {
            $scope.newTranslation.id = response.data.id;
            $scope.details.translations.push($scope.newTranslation);
            $scope.newTranslation = null;
        }
        function ShowConfirmation(response)
        {
            alert('Translation saved');
        }

        function updateErrors(response) {
            $scope.errors.push(response ? response.data ? response.data.errorMessage ? response.data.errorMessage : response.data : response : 'unspecified error');
        }

        TermService.getTermDetails($routeParams.termId, $routeParams.categoryId)
            .then(function (response) {

                $scope.details = response.data;
            }, updateErrors).catch(updateErrors);

        $scope.command = function (action) {
            action();
        };

        $scope.properties = [];

    }])
    .controller('AddTermCtrl', ['$scope', 'SessionService', 'CategoryService', 'TermService', '$location', '$routeParams', function ($scope, SessionService, CategoryService, TermService, $location, $routeParams) {
        $scope.errors = [];
        CategoryService.getCategories().then(updateCategories, updateErrors).catch(updateErrors);
        function updateCategories(response) {
            $scope.categories = response.data;
        }

        function updateErrors(response) {
            $scope.errors = [];
            $scope.errors.push(response ? response.data ? response.data.message ? response.data.message : response.data : response : 'unspecified error');
        }

        $scope.newExample = [];
        $scope.newTranslation = [];

        $scope.addTerm = function (data, form) {
            var userSession = SessionService.getUserSession();
            TermService.addTerm(data).then(function (response) {
                $location.path('/term/' + response.data.id);
            }, updateErrors)
                .catch(function (response) {
                    $scope.errors.push("error adding new term:" + response.statusText);
                });
        };

    }])
    .directive('categoryterm', ['CategoryService', function (CategoryService) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                mode: "@",
                data: "=",
                controls: "=",
                categoriesData: "="
            },
            controller: ['$scope', function($scope){

                $scope.showCategoryList = false;

                $scope.showCL = function(){
                    if ( $scope.showCategoryList == false)
                        $scope.showCategoryList = true;
                    else
                        $scope.showCategoryList = false;
                };


            }],
            templateUrl: 'term/termTemplate.html',

            link:function($scope)
            {
                $scope.languages = $scope.$parent.languages;
                $scope.categories = $scope.$parent.categories;
            }

        }
    }]);



