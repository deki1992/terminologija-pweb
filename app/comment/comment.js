/**
 * Created by Nemanja on 15.6.2016.
 */

//Comment controller is only used to in-place edit a comment and initiate add from another controller
//Comment collection operations are up to the parent (Translation)
angular.module('myApp.comment',['ngRoute'])
    .controller('CommentController',['$scope','CommentService','UserService','$routeParams','$location',
    function ($scope,CommentService,UserService,$routeParams,$location)
    {
        $scope.translation = {id:$routeParams.tId}

        $scope.commentOptions =
        {
            cancel : function()
            {
                if(confirm('Discard current data?')) {
                    $scope.newComments.clear();
                    $scope.mode = 'view';
                }
            },

            editComment: function ()
            {
                if($scope.mode='new' && !confirm('Discard changes ?')) return;
                //$scope.newComments.push(new TranslationComment()); let the directive handle it's editing
                $scope.mode = 'edit';
            },

            saveComment: function(commentData)
            {
                if(commentData.id!=null)
                    CommentService.updateComment(commentData).then(UpdateComment,UpdateErrors).catch(UpdateErrors);
                else
                    CommentService.addComment(commentData).then(UpdateComment,UpdateErrors).catch(UpdateErrors);
            }
        };
        
        
    }])

.directive('comment',function()
{
    return {
        restrict:'E',
        scope:{
            mode:"@",
            comment:"=",
            controls:"="
        },
        templateUrl:'comment/commentTemplate.html',
        controller:function($scope, SessionService, CommentService)
        {
            var oldComment = null;
            $scope.errors = [];
            $scope.session - SessionService.getUserSession();
            //$scope.owner = (comment.creator == session.user);
            $scope.edit = function()
            {
                oldComment = angular.copy(comment); //save a copy of data
                $scope.mode = 'edit';
            }

            $scope.cancel = function (changed)
            {
                if(!changed || confirm('Cancel edit?')) {
                    comment.content = oldComment.content;
                    oldComment = null;
                    $scope.mode = 'view';
                    return false;
                }
            }
            $scope.saveComment = function(comment)
            {
                if(confirm('Save comment data?')) {
                    oldComment = null;
                    CommentService.updateComment(comment).then(function (response) {
                        alert('Comment changed')
                    }).catch(UpdateErrors);
                }
            }
            function UpdateErrors(response)
            {
                $scope.errors.length = 0;
                $scope.errors.push(response.data.errorMessage || 'Unspecified error');
            }
        }
    }
});