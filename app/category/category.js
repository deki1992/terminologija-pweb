'use strict';

//TODO možda da sve routes smestimo u root aplikacije ?

angular.module('myApp.category', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/categories', {
            templateUrl: 'category/categories.html',
            controller: 'CategoriesCtrl'
        });
        $routeProvider.when('/category/:categoryId', {
            templateUrl: 'category/category.html',
            controller: 'CategoryCtrl',
            resolve:{languages:function(UtilityService){return UtilityService.getLanguages();}}
        });
    }])

    .controller('CategoriesCtrl', ['$scope', 'CategoryService', '$location', function ($scope, CategoryService, $location) {
        $scope.error = null;
        $scope.categories;
        $scope.newCategory = null;

        $scope.AddCategory = function($data)
        {
            CategoryService.addCategory($data).then(AddToCategories).catch(DisplayError);
        }
        function AddToCategories(response)
        {
            if(response.status == '201') {
                $scope.newCategory = null;
                $scope.categories.push(response.data);
            }
        }

        $scope.errOk = function()
        {
            $scope.error = null;
        }

        $scope.AddNewCategory = function()
        {
            $scope.newCategory = {};
        }

        $scope.CancelAdd = function()
        {
            $scope.newCategory = null;
        }
        $scope.displayStyle = 'CategoryList';
        CategoryService.getCategories().then(UpdateCategories).catch(DisplayError);

        function UpdateCategories(response) {
            $scope.categories = response.data;
        }

        $scope.viewCategory = function (id) {
            $location.path('/category/' + id);
        };

        $scope.alert = function (text) {
            alert(text);
        };

        function DisplayError(response) {
            $scope.error = response.statusText;
        }

        $scope.setDisplayStyle = function (style) {
            this.displayStyle = style;
        }
    }])

    .controller('CategoryCtrl', ['$scope', 'CategoryService', 'TermService', '$location', '$routeParams','languages',function ($scope, CategoryService, TermService, $location, $routeParams,languages) {
        $scope.error = null;
        $scope.categoryDetails = {};
        $scope.categoryTerms = [];
        $scope.newTerm = null;
        $scope.languages = languages;
        $scope.categories = [];
        $scope.currentCategory = +$routeParams.categoryId;

        CategoryService.getCategories().then(function(response){$scope.categories = response.data;});

        $scope.editDetails = null;

        $scope.EditDetails = function()
        {
            $scope.editDetails = angular.copy($scope.categoryDetails);
        }

        $scope.CancelEdit = function()
        {
            $scope.editDetails = null;
        }
        
        $scope.UpdateDetails = function()
        {
            CategoryService.updateCategory($scope.editDetails.id, $scope.editDetails).then(RefreshDetails).catch(DisplayError);
        }

        function RefreshDetails(response)
        {
            if(response.status=='200')
            {
                $scope.categoryDetails = angular.copy($scope.editDetails);
                $scope.editDetails = null;
            }
            else DisplayError(response);
        }



        
        $scope.mode = 'view';

        CategoryService.getCategoryDetails($routeParams.categoryId).then(UpdateCategoryDetails).catch(DisplayError);
        CategoryService.getCategoryTerms($routeParams.categoryId).then(UpdateCategoryTerms).catch(DisplayError);

        $scope.categoryOptions = [];
        var emptyTerm = function()
        {
            //add new term with default language and current category
            return {ID:-1, description:null, term:null,category:$routeParams.categoryId,language:'EN'};
        };
        $scope.addNewTerm = function()
        {
            $scope.newTerm = {categories:[$scope.currentCategory]};
        };

        $scope.tcontrols = {
            deleteTerm:function(tId)
            {
                if(confirm("Confirm term delete"))
                TermService.deleteTerm(tId, $scope.categoryDetails.id).then(function(response)
                {
                    $scope.categoryTerms = $scope.categoryTerms.filter(function (term) {
                        return term.ID!=tId;
                    });
                    //$scope.$apply();
                })
            },
            cancelAdd: function () {
                if(confirm('Otkazati dodavanje novog termina?'))
                $scope.newTerm = null;
            },
            save: function (termData) {
                if(!termData) throw "" //smisleno nesto ?
                else {
                    if(!confirm("Confirm add new term")) return;
                    var apiCall = {
                        Categories:termData.categories,
                        Term:termData.term,
                        Description:termData.description,
                        Language:termData.language
                    };


                    TermService.addTerm(apiCall).then(function (response) {
                        //push terms into current for category only if it has been added
                            if(termData.categories.indexOf($scope.currentCategory)>-1) {
                                var term = {
                                    term: termData.term,
                                    language: termData.language,
                                    description: termData.description
                                };
                                term.category_id = $routeParams.categoryId;
                                term.ID = response.data.ID; //copy the rest
                                $scope.categoryTerms.push(term);
                            }
                        $scope.newTerm = null;
                        $scope.error = null;
                        //$timeout($scope.$apply());
                    },
                    function(response)
                    {
                      $scope.error = response.errorMessage; //TODO ovo treba da se poveze na globalni handler ili da ne dolazi do kontrolera
                    });
                }
            },
            addToCategory: function(termID, categoryID){
                TermService.addTermToCategory(termID, categoryID).then(function(response){
                    if (response.status == 201)
                        alert("Uspešno dodat termin u drugu kategoriju!")
                    else
                        alert("Neuspešno dodavanje! Status code: " + response.status);
                });
            }

        };

        $scope.command = function (action) {
            action();
        };

        function addNewTerm() {
            $location.path('/term/add');
        }

        function UpdateCategoryDetails(response) {
            $scope.categoryDetails = response.data;
        }

        function UpdateCategoryTerms(response) {
            $scope.categoryTerms = response.data;
        }

        function DisplayError(response) {
            $scope.error = response?response.data?response.data.errorMessage?response.data.errorMessage:response.data:response:'Unspecified error';
        }
    }]);
