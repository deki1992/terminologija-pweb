'use strict';

angular.module('myApp.user', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/user/login', {
            templateUrl: 'user/login.html',
            controller: 'UserLoginCtrl'
        }).when('/user/register', {
            templateUrl: 'user/register.html',
            controller: 'UserRegisterCtrl'
        })
            .when('/user/profile',{
                templateUrl:'user/profile.html',
                controller: 'UserProfileCtrl'
            });
    }])

    .controller('UserLoginCtrl', ['$scope', 'SessionService', '$location', function ($scope, SessionService, $location) {
        $scope.error = null;
        $scope.loginUser = //SessionService.loginUser;
            function (data, form) {
                if (!form.$valid)
                    return;

                //$scope.error =
                SessionService.loginUser(data.username, data.password)
                    .then(function (response) {
                        if (response.success) {
                            $scope.user_session = SessionService.getUserSession();
                            $location.path('/home');
                        }
                    })
                    .catch(function (response) {
                        $scope.error = //"Greška u prijavi: " +
                            response.error.message;
                    });
            };
    }])

    .controller('UserRegisterCtrl', ['$scope', 'SessionService', '$location', function ($scope, SessionService, $location) {
        $scope.error = null;
        $scope.registerData = {};
        $scope.passwordMatch = function() {
            return $scope.registerData.password!=$scope.registerData.password_confirm;
        };

        $scope.registerUser =
            function (data, form) {
                if (!form.$valid)
                    return;
                SessionService.registerUser(data)
                    .then(function (response) {
                        //$scope.user_session = SessionService.getUserSession();
                        $location.path("/user/login"); // /home
                    })
                    .catch(function (response) {
                        $scope.error = response.error.message;
                    })
            };
    }])
    .controller('UserProfileCtrl',['$scope','SessionService','$location',function($scope,SessionService,$location)
    {
        var user_session = SessionService.getUserSession();
        $scope.$watch(user_session);

        if(!user_session || !user_session.logged_in)
            $location.path('/user/login');
        else
            $scope.user = user_session.user;

        $scope.editData = null;
        $scope.edit = function()
        {
            $scope.editData = angular.copy($scope.user);
            $scope.editData.oldpassword = "";
            $scope.editData.password = "";
            $scope.editData.password2 = "";
        };

        $scope.cancelEdit = function()
        {
            $scope.editData = null;
        };


        $scope.updateProfile = function(data,form)
        {
            if(!form.$valid)
                return;
            if ((typeof data['password'] !== 'undefined') && (data['password'] != data['password2']))
                return; // Passwords (if provided) do not match!
            var data2 = angular.copy(data);
            if (form.email.$pristine) // Don't send new e-mail to server if it has not been changed; server would ask for password
                delete data2['email'];
            //delete data2['password2'];
            SessionService.updateUser(data2).then(notifySuccess).catch(setErrors);
        };
        function notifySuccess(response)
        {
            $scope.user = angular.copy($scope.editData);
            $scope.editData = null;
        }

        function setErrors(response)
        {
            $scope.error = response.data.errorMsg;
        }

    }])
    .directive("tmUser",function()
    {
        return {
            restrict: 'E',
            templateUrl: "userTemplate.html",
            replace: true,
            scope: { user: '=', updateProfile: '&', mode: '@', save:function()
            {
                updateProfile(user);
            }}
        }
    });
