'use strict';

describe('myApp.user module', function() {

  beforeEach(module('myApp.user'));

  describe('user login controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var userLoginCtrl = $controller('UserLoginCtrl');
      expect(userLoginCtrl).toBeDefined();
    }));

  });

  describe('user register controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var userRegisterCtrl = $controller('UserRegisterCtrl');
      expect(userRegisterCtrl).toBeDefined();
    }));

  });
});