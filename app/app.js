'use strict';

// Declare app level module which depends on views, and components
(function() {
	var myApp = angular.module('myApp', [
		'ngRoute',
		'jkAngularRatingStars',
		'chart.js',
		'myApp.menuView',
		'myApp.homePage',
		'myApp.user',
		'myApp.category',
		'myApp.term',
		'myApp.search',
		'myApp.translation',
		'myApp.comment',
		'myApp.activityLog',


		'myApp.sessionService',
		'myApp.apiService',
		'myApp.utilityService',
		'myApp.categoryService',
		'myApp.termService',
		'myApp.commentService',
		'myApp.translationService',
		'myApp.exampleService',
		'myApp.voteService',
		'myApp.activityLogService',

		'myApp.version'
	]).config(['$routeProvider', function ($routeProvider) {

		$routeProvider.when('/search/:term/:language?/:category?',
			{
				templateUrl: 'search/searchResult.html',
				controller: 'SearchCtrl'
			})
			.when('/translation/:id',
				{
					templateUrl: 'translation/translation.html',
					controller: 'TranslationCtrl'
				})
			.when('/dashboard',
				{
					templateUrl: 'activityLog/dashboard.html',
					controller: 'ActivityLogCtrl',
					resolve: {
						userSession: function (SessionService) {
							return SessionService.getUserSession();
						}
					}
				})
			.otherwise({redirectTo: '/home'});
	}])
	.filter("asDate", function () {
		return function (input) {
			return new Date(input);
		}
	});

	myApp.value('apiRoot', '../api/'); // http://localhost/terminologija/api/

	var initWithSession = function(response)
	{
		myApp.constant("oldSession", response.data);
		init();
	};

	var initWithoutSession = function(response)
	{
		localStorage.removeItem("access_token");
		localStorage.removeItem("access_token_expires");
		myApp.constant("oldSession", {logged_in:false});
		init();
	};

	function init(){
		angular.element(document).ready(function() {
			angular.bootstrap(document, ["myApp"]);
		});
	}

	if(localStorage.getItem("access_token"))
	{
		var initInjector = angular.injector(["ng"]);
		var $http = initInjector.get("$http");
		$http.get('../api/sessions/user', {
			headers: {'Authorization': 'Bearer ' + localStorage.getItem("access_token")}
		}).then(initWithSession, initWithoutSession);
	}
	else initWithoutSession();
}());