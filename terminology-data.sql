-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 12, 2016 at 03:57 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `terminology`
--

--
-- Truncate table before insert `categories`
--

TRUNCATE TABLE `categories`;
--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `Title`, `Creation_date`, `Creator`, `Description`, `Conclusion`) VALUES
(1, 'Nazivi programa i programskih jezika', '2016-07-05 17:50:01', 1, 'Da li pišemo u originalu ili transkribovano? Da li u latiničkom tekstu originale pišemo italic? A u ćiriličkom? Da li prvo slovo treba da bude veliko?', NULL),
(2, 'Transkripcija ličnih imena', '2016-07-05 17:50:22', 1, 'Da li se transkribuje kako se izgovara (zašto onda nije Šikago i Vošington)? Gde se mogu pronaći opšta pravila transkripcije za razne jezike?', NULL),
(3, 'Anglicizmi', '2016-07-05 17:50:57', 1, 'Kada upotrebiti anglicizam? Kada nemamo prirodan prevod na srpski jezik? Šta je prirodan prevod? Kada reč ne postoji u pravopisu? Da li se umesto preuzimanja anglicizma treba potruditi i pronaći adekvatan prevod, pa tek ako ne ide preuzeti anglicizam? Vrlo teško je naći opšte kriterijume.', NULL),
(4, 'Prevodi', '2016-07-05 17:52:53', 1, 'Razni termini iz programiranja i računarstva', NULL),
(5, 'Strukture podataka', '2016-07-05 17:53:25', 1, 'Strukture podataka', NULL),
(6, 'Nazivi profesija', '2016-07-05 17:53:35', 1, 'Nazivi profesija', NULL),
(7, 'Kontrole interfejsa', '2016-07-05 17:53:48', 1, '...', 'Kakav pa opis staviti? A obavezno je polje...'),
(8, 'Oblik i pisanje reči', '2016-07-05 17:54:04', 1, 'ovo nije posebna kategorija termina? postavlja se pitanje kako se neki naši računarski izrazi (odnosno prevodi) pišu...', 'Tehnički, mogao bi se napisati originalni termin za većinu slučajeva, pa navesti različiti oblici pisanja kao prevodi. Pa bi u opis / zaključak termina išla diskusija kako se piše...\nE sad ponekad to nisu prosti termini nego duži delovi tipa da li se zadaje "sa standardnog ulaza" ili "na standardnom ulazu"?');

--
-- Truncate table before insert `category_term`
--

TRUNCATE TABLE `category_term`;
--
-- Dumping data for table `category_term`
--

INSERT INTO `category_term` (`Category_ID`, `Term_ID`) VALUES
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(2, 14),
(2, 15),
(2, 16),
(3, 1),
(3, 2),
(3, 3),
(4, 29),
(4, 30),
(4, 31),
(4, 32),
(4, 33),
(5, 17),
(5, 18),
(5, 19),
(5, 20),
(5, 21),
(5, 22),
(5, 23),
(5, 24),
(5, 25),
(5, 26),
(5, 27),
(5, 28),
(6, 34),
(6, 35),
(6, 36),
(6, 37),
(6, 38),
(6, 39),
(6, 40),
(7, 41),
(7, 42),
(7, 43),
(7, 44),
(7, 45),
(7, 46),
(7, 47),
(7, 48),
(7, 49),
(7, 50),
(7, 51);

--
-- Truncate table before insert `comments`
--

TRUNCATE TABLE `comments`;
--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`ID`, `Creator`, `Translated_term_ID`, `Creation_Date`, `Text`, `Reply`, `Last_modified`) VALUES
(1, 2, 2, '2016-07-06 19:48:51', 'Dobar prevod.\n', NULL, NULL),
(4, 2, 9, '2016-07-07 11:15:16', 'Možda bi to bilo i ispravno, ali to više liči na prirodan prevod (iako je jezik nazvan kao zmija) i niko taj jezik ne izgorava tako.\n', NULL, NULL);

--
-- Truncate table before insert `examples`
--

TRUNCATE TABLE `examples`;
--
-- Dumping data for table `examples`
--

INSERT INTO `examples` (`ID`, `Example`, `Category_ID`, `Term_ID`) VALUES
(2, 'Programiranje u C-u', 1, 5),
(3, 'Statički Web sajtovi se sastoje od skupa HTML stranica', 1, 13),
(4, 'Stranice na Internetu se pišu u HTML-u', 1, 13),
(5, 'A form, in desktop programming, represents one window of the resulting application.', 7, 41),
(6, 'On the other hand, in web programming a form is part of the page that requests the user to input the desired data.', 7, 41),
(7, '"Please fill in this form to register for our service." - This is an example of the latter.', 7, 41);

--
-- Truncate table before insert `languages`
--

TRUNCATE TABLE `languages`;
--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`Code`, `Language`) VALUES
('en', 'engleski'),
('sr', 'srpski');

--
-- Truncate table before insert `log`
--

TRUNCATE TABLE `log`;
--
-- Dumping data for table `log`
--

INSERT INTO `log` (`ID`, `User_ID`, `Date`, `ActionType`, `ResourceType`, `ResourceId`, `Arguments`) VALUES
(1, 1, '2016-07-05 17:50:01', 0, 'CATEGORY', '1', '{"id":1,"title":"Nazivi programa i programskih jezika","creation_date":"2016-07-05 17:50:01","creator":"test","description":"Da li pi\\u0161emo u originalu ili transkribovano? Da li u latini\\u010dkom tekstu originale pi\\u0161emo italic? A u \\u0107irili\\u010dkom? Da li prvo slovo treba da bude veliko?","conclusion":null,"_fields":[],"_modified":[]}'),
(2, 1, '2016-07-05 17:50:22', 0, 'CATEGORY', '2', '{"id":2,"title":"Transkripcija li\\u010dnih imena","creation_date":"2016-07-05 17:50:22","creator":"test","description":"Da li se transkribuje kako se izgovara (za\\u0161to onda nije \\u0160ikago i Vo\\u0161ington)? Gde se mogu prona\\u0107i op\\u0161ta pravila transkripcije za razne jezike?","conclusion":null,"_fields":[],"_modified":[]}'),
(3, 1, '2016-07-05 17:50:57', 0, 'CATEGORY', '3', '{"id":3,"title":"Anglicizmi","creation_date":"2016-07-05 17:50:57","creator":"test","description":"Kada upotrebiti anglicizam? Kada nemamo prirodan prevod na srpski jezik? \\u0160ta je prirodan prevod? Kada re\\u010d ne postoji u pravopisu? Da li se umesto preuzimanja anglicizma treba potruditi i prona\\u0107i adekvatan prevod, pa tek ako ne ide preuzeti anglicizam? Vrlo te\\u0161ko je na\\u0107i op\\u0161te kriterijume.","conclusion":null,"_fields":[],"_modified":[]}'),
(4, 1, '2016-07-05 17:52:53', 0, 'CATEGORY', '4', '{"id":4,"title":"Prevodi","creation_date":"2016-07-05 17:52:53","creator":"test","description":"Razni termini iz programiranja i ra\\u010dunarstva","conclusion":null,"_fields":[],"_modified":[]}'),
(5, 1, '2016-07-05 17:53:25', 0, 'CATEGORY', '5', '{"id":5,"title":"Strukture podataka","creation_date":"2016-07-05 17:53:25","creator":"test","description":"Strukture podataka","conclusion":null,"_fields":[],"_modified":[]}'),
(6, 1, '2016-07-05 17:53:35', 0, 'CATEGORY', '6', '{"id":6,"title":"Nazivi profesija","creation_date":"2016-07-05 17:53:35","creator":"test","description":"Nazivi profesija","conclusion":null,"_fields":[],"_modified":[]}'),
(7, 1, '2016-07-05 17:53:48', 0, 'CATEGORY', '7', '{"id":7,"title":"Kontrole interfejsa","creation_date":"2016-07-05 17:53:48","creator":"test","description":"...","conclusion":null,"_fields":[],"_modified":[]}'),
(8, 1, '2016-07-05 17:54:04', 0, 'CATEGORY', '8', '{"id":8,"title":"Oblik i pisanje re\\u010di","creation_date":"2016-07-05 17:54:04","creator":"test","description":"...","conclusion":null,"_fields":[],"_modified":[]}'),
(9, 2, '2016-07-06 18:43:35', 1, 'CATEGORY', '7', '{"id":7,"title":"Kontrole interfejsa","creation_date":"2016-07-05 17:53:48","creator":"test","description":"...","conclusion":"Kakav pa opis staviti? A obavezno je polje...","_fields":[],"_modified":[]}'),
(10, 2, '2016-07-06 19:25:20', 0, 'TERM', '1', '{"term":{"ID":1,"term":"computer","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-06 19:25:20","description":"kompjuter","conclusion":null,"category_id":3,"category_title":"Anglicizmi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Anglicizmi"]}'),
(11, 2, '2016-07-06 19:27:58', 0, 'TERM', '2', '{"term":{"ID":2,"term":"software","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-06 19:27:58","description":"softver","conclusion":null,"category_id":3,"category_title":"Anglicizmi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Anglicizmi"]}'),
(12, 2, '2016-07-06 19:28:27', 0, 'TERM', '3', '{"term":{"ID":3,"term":"hardware","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-06 19:28:27","description":"hardver","conclusion":null,"category_id":3,"category_title":"Anglicizmi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Anglicizmi"]}'),
(13, 2, '2016-07-06 19:31:06', 0, 'TRANSLATION', '1', '{"id":"1","translation":"kompjuter","language":"srpski","org_term":1,"category":"Anglicizmi","user_id":2,"username":"admin","creation_date":"2016-07-06 19:31:06","argument":"Anglicizam - tako se izgovara.","rating":null,"_fields":[],"_modified":[]}'),
(14, 2, '2016-07-06 19:36:00', 0, 'TRANSLATION', '2', '{"id":"2","translation":"ra\\u010dunar","language":"srpski","org_term":1,"category":"Anglicizmi","user_id":2,"username":"admin","creation_date":"2016-07-06 19:36:00","argument":"Srpska re\\u010d - prirodan prevod","rating":null,"_fields":[],"_modified":[]}'),
(15, 2, '2016-07-06 19:45:11', 0, 'VOTE', '2', '{"user_id":2,"vote":5,"translation":"2"}'),
(16, 2, '2016-07-06 19:48:51', 0, 'COMMENT', '1', '{"id":1,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:48:51","content":"Dobar prevod.\\n","parent":null,"modified":null,"_fields":[],"_modified":[]}'),
(17, 2, '2016-07-06 19:50:47', 0, 'COMMENT', '2', '{"id":2,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:50:47","content":"Test komentar.\\n","parent":null,"modified":null,"_fields":[],"_modified":[]}'),
(18, 2, '2016-07-06 19:51:17', 1, 'COMMENT', '2', '{"id":2,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:50:47","content":"Test komentar.\\n","parent":null,"modified":null,"_fields":[],"_modified":[]}'),
(19, 2, '2016-07-06 19:51:44', 0, 'COMMENT', '3', '{"id":3,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:51:44","content":"O Bo\\u017ee za\\u0161to\\n","parent":2,"modified":null,"_fields":[],"_modified":[]}'),
(20, 2, '2016-07-06 19:52:31', 2, 'COMMENT', '2', '{"id":3,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:51:44","content":"O Bo\\u017ee za\\u0161to\\n","parent":2,"modified":null,"_fields":[],"_modified":[]}'),
(21, 2, '2016-07-06 19:52:52', 2, 'COMMENT', '2', '{"id":2,"user_id":2,"fullname":"admin","translation_id":2,"created":"2016-07-06 19:50:47","content":"Test komentar. Punjenje log-a.\\n","parent":null,"modified":"2016-07-06 19:51:17","_fields":[],"_modified":[]}'),
(22, 2, '2016-07-07 10:39:11', 0, 'TERM', '4', '{"term":{"ID":4,"term":"Java","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:39:11","description":"programski jezik","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(23, 2, '2016-07-07 10:39:21', 0, 'TERM', '5', '{"term":{"ID":5,"term":"C","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:39:21","description":"programski jezik","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(24, 2, '2016-07-07 10:39:32', 0, 'TERM', '6', '{"term":{"ID":6,"term":"Python","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:39:32","description":"programski jezik","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(25, 2, '2016-07-07 10:39:43', 0, 'TERM', '7', '{"term":{"ID":7,"term":"Pascal","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:39:43","description":"programski jezik","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(26, 2, '2016-07-07 10:39:59', 0, 'TERM', '8', '{"term":{"ID":8,"term":"Windows","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:39:59","description":"operativni sistem","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(27, 2, '2016-07-07 10:40:07', 0, 'TERM', '9', '{"term":{"ID":9,"term":"Linux","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:40:07","description":"operativni sistem","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(28, 2, '2016-07-07 10:42:37', 0, 'TERM', '10', '{"term":{"ID":10,"term":"Microsoft","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:42:37","description":"softverska firma","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(29, 2, '2016-07-07 10:43:51', 0, 'TERM', '11', '{"term":{"ID":11,"term":"OpenGL","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:43:51","description":"3D tehnologija","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(30, 2, '2016-07-07 10:44:26', 0, 'TERM', '12', '{"term":{"ID":12,"term":"Bluetooth","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:44:26","description":"na\\u010din prenosa podataka","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(31, 2, '2016-07-07 10:46:00', 0, 'TERM', '13', '{"term":{"ID":13,"term":"HTML","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 10:46:00","description":"jezik na Webu (pogledati \\"Web\\")","conclusion":null,"category_id":1,"category_title":"Nazivi programa i programskih jezika","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi programa i programskih jezika"]}'),
(32, 2, '2016-07-07 10:47:42', 0, 'TRANSLATION', '3', '{"id":"3","translation":"Java","language":"srpski","org_term":4,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 10:47:42","argument":"Pisanje u originalu. \\u010cesto se kod nas ovako i izgovara (\\u0161to je verovatno pogre\\u0161no).","rating":null,"_fields":[],"_modified":[]}'),
(33, 2, '2016-07-07 10:48:06', 0, 'TRANSLATION', '4', '{"id":"4","translation":"D\\u017eava","language":"srpski","org_term":4,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 10:48:06","argument":"Ovako se pravilno izgovara.","rating":null,"_fields":[],"_modified":[]}'),
(34, 2, '2016-07-07 10:57:52', 0, 'TRANSLATION', '5', '{"id":"5","translation":"C","language":"srpski","org_term":5,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 10:57:52","argument":"original","rating":null,"_fields":[],"_modified":[]}'),
(35, 2, '2016-07-07 10:58:09', 0, 'TRANSLATION', '6', '{"id":"6","translation":"Ce","language":"srpski","org_term":5,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 10:58:09","argument":"kako mi izgovaramo","rating":null,"_fields":[],"_modified":[]}'),
(36, 2, '2016-07-07 10:58:30', 0, 'TRANSLATION', '7', '{"id":"7","translation":"Si","language":"srpski","org_term":5,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 10:58:30","argument":"Kako se u originalu (na engleskom) izgovara","rating":null,"_fields":[],"_modified":[]}'),
(37, 2, '2016-07-07 10:59:38', 1, 'TERM', '5', 'null'),
(38, 2, '2016-07-07 11:01:00', 1, 'TERM', '5', 'null'),
(39, 2, '2016-07-07 11:01:35', 0, 'EXAMPLE', '2', '{"id":2,"example":"Programiranje u C-u","category_ID":1,"term_ID":5,"_fields":[],"_modified":[]}'),
(40, 2, '2016-07-07 11:09:35', 0, 'TRANSLATION', '8', '{"id":"8","translation":"Python","language":"srpski","org_term":6,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:09:34","argument":"original","rating":null,"_fields":[],"_modified":[]}'),
(41, 2, '2016-07-07 11:10:07', 0, 'TRANSLATION', '9', '{"id":"9","translation":"Piton","language":"srpski","org_term":6,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:10:07","argument":"Kao \\u0161to i zmiju zovemo.","rating":null,"_fields":[],"_modified":[]}'),
(42, 2, '2016-07-07 11:10:51', 0, 'TRANSLATION', '10', '{"id":"10","translation":"Pajton","language":"srpski","org_term":6,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:10:51","argument":"Kako se izgovara (niko ne izgovara \\"piton\\")","rating":null,"_fields":[],"_modified":[]}'),
(43, 2, '2016-07-07 11:11:00', 0, 'VOTE', '10', '{"user_id":2,"vote":4,"translation":"10"}'),
(44, 2, '2016-07-07 11:15:16', 0, 'COMMENT', '4', '{"id":4,"user_id":2,"fullname":"admin","translation_id":9,"created":"2016-07-07 11:15:16","content":"Mo\\u017eda bi to bilo i ispravno, ali to vi\\u0161e li\\u010di na prirodan prevod (iako je jezik nazvan kao zmija) i niko taj jezik ne izgorava tako.\\n","parent":null,"modified":null,"_fields":[],"_modified":[]}'),
(45, 2, '2016-07-07 11:15:36', 0, 'VOTE', '9', '{"user_id":2,"vote":2,"translation":"9"}'),
(46, 2, '2016-07-07 11:17:56', 0, 'TRANSLATION', '11', '{"id":"11","translation":"Paskal","language":"srpski","org_term":7,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:17:56","argument":"Po Vuku.","rating":null,"_fields":[],"_modified":[]}'),
(47, 2, '2016-07-07 11:19:37', 1, 'TERM', '7', 'null'),
(48, 2, '2016-07-07 11:21:57', 0, 'TRANSLATION', '12', '{"id":"12","translation":"Vindovs","language":"srpski","org_term":8,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:21:57","argument":"ve\\u0107 u\\u0161lo u naviku kod nas da se ovako izgovara","rating":null,"_fields":[],"_modified":[]}'),
(49, 2, '2016-07-07 11:22:45', 0, 'TRANSLATION', '13', '{"id":"13","translation":"Vindouz","language":"srpski","org_term":8,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:22:45","argument":"otprilike ovako zvu\\u010di engleski izgovor. mo\\u017eda i ne.","rating":null,"_fields":[],"_modified":[]}'),
(50, 2, '2016-07-07 11:23:23', 0, 'TRANSLATION', '14', '{"id":"14","translation":"Linuks","language":"srpski","org_term":9,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:23:23","argument":"jer x","rating":null,"_fields":[],"_modified":[]}'),
(51, 2, '2016-07-07 11:30:35', 0, 'TRANSLATION', '15', '{"id":"15","translation":"Majkrosoft","language":"srpski","org_term":10,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:30:35","argument":"Ako ve\\u0107 imena po pravilu pi\\u0161emo kako se izgovaraju... za\\u0161to ne i imena firmi","rating":null,"_fields":[],"_modified":[]}'),
(52, 2, '2016-07-07 11:31:59', 0, 'TRANSLATION', '16', '{"id":"16","translation":"OpenGL","language":"srpski","org_term":11,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:31:59","argument":"OpenGL, OpenD\\u017eiEl, Open D\\u017ei El?","rating":null,"_fields":[],"_modified":[]}'),
(53, 2, '2016-07-07 11:33:12', 0, 'TRANSLATION', '17', '{"id":"17","translation":"Blutut","language":"srpski","org_term":12,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:33:12","argument":"Da li bi trebalo da pi\\u0161emo Bluetooth ili BluTut?","rating":null,"_fields":[],"_modified":[]}'),
(54, 2, '2016-07-07 11:34:22', 0, 'EXAMPLE', '3', '{"id":3,"example":"Stati\\u010dki Web sajtovi se sastoje od skupa HTML stranica","category_ID":1,"term_ID":13,"_fields":[],"_modified":[]}'),
(55, 2, '2016-07-07 11:34:43', 0, 'EXAMPLE', '4', '{"id":4,"example":"Stranice na Internetu se pi\\u0161u u HTML-u","category_ID":1,"term_ID":13,"_fields":[],"_modified":[]}'),
(56, 2, '2016-07-07 11:35:47', 0, 'TRANSLATION', '18', '{"id":"18","translation":"HTML","language":"srpski","org_term":13,"category":"Nazivi programa i programskih jezika","user_id":2,"username":"admin","creation_date":"2016-07-07 11:35:47","argument":"HTML, HaTeEmEl ili Ha-Te-Em-El? u HTML-u, u HaTeEmEl-u? A \\u0107irilica?","rating":null,"_fields":[],"_modified":[]}'),
(57, 2, '2016-07-07 11:38:43', 0, 'TERM', '14', '{"term":{"ID":14,"term":"Deijkstra","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:38:42","description":"poznat po algoritmima","conclusion":null,"category_id":2,"category_title":"Transkripcija li\\u010dnih imena","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Transkripcija li\\u010dnih imena"]}'),
(58, 2, '2016-07-07 11:39:41', 1, 'TERM', '14', 'null'),
(59, 2, '2016-07-07 11:40:34', 0, 'TRANSLATION', '19', '{"id":"19","translation":"Dajkstra","language":"srpski","org_term":14,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:40:34","argument":"jedna mogu\\u0107nost","rating":null,"_fields":[],"_modified":[]}'),
(60, 2, '2016-07-07 11:40:52', 0, 'TRANSLATION', '20', '{"id":"20","translation":"Dejkstra","language":"srpski","org_term":14,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:40:52","argument":"jedna mogu\\u0107nost","rating":null,"_fields":[],"_modified":[]}'),
(61, 2, '2016-07-07 11:41:28', 0, 'TRANSLATION', '21', '{"id":"21","translation":"Dijkstra","language":"srpski","org_term":14,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:41:28","argument":"jedna mogu\\u0107nost. I to naj\\u010de\\u0161\\u0107a.","rating":null,"_fields":[],"_modified":[]}'),
(62, 2, '2016-07-07 11:42:12', 0, 'TERM', '15', '{"term":{"ID":15,"term":"Tseytin","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:42:12","description":"informati\\u010dar","conclusion":null,"category_id":2,"category_title":"Transkripcija li\\u010dnih imena","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Transkripcija li\\u010dnih imena"]}'),
(63, 2, '2016-07-07 11:42:37', 0, 'TRANSLATION', '22', '{"id":"22","translation":"Tsajtin","language":"srpski","org_term":15,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:42:37","argument":"?","rating":null,"_fields":[],"_modified":[]}'),
(64, 2, '2016-07-07 11:42:50', 0, 'TRANSLATION', '23', '{"id":"23","translation":"Cajtin","language":"srpski","org_term":15,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:42:50","argument":"valjda se tako izgovara!","rating":null,"_fields":[],"_modified":[]}'),
(65, 2, '2016-07-07 11:43:20', 0, 'TERM', '16', '{"term":{"ID":16,"term":"De Bruijn","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:43:20","description":"informati\\u010dar","conclusion":null,"category_id":2,"category_title":"Transkripcija li\\u010dnih imena","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Transkripcija li\\u010dnih imena"]}'),
(66, 2, '2016-07-07 11:43:54', 0, 'TRANSLATION', '24', '{"id":"24","translation":"De Brujn","language":"srpski","org_term":16,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:43:54","argument":"mo\\u017eda","rating":null,"_fields":[],"_modified":[]}'),
(67, 2, '2016-07-07 11:44:09', 0, 'TRANSLATION', '25', '{"id":"25","translation":"De Brulj","language":"srpski","org_term":16,"category":"Transkripcija li\\u010dnih imena","user_id":2,"username":"admin","creation_date":"2016-07-07 11:44:09","argument":"mo\\u017eda","rating":null,"_fields":[],"_modified":[]}'),
(68, 2, '2016-07-07 11:47:35', 0, 'TRANSLATION', '26', '{"id":"26","translation":"softver","language":"srpski","org_term":2,"category":"Anglicizmi","user_id":2,"username":"admin","creation_date":"2016-07-07 11:47:35","argument":"Zato \\u0161to softver.","rating":null,"_fields":[],"_modified":[]}'),
(69, 2, '2016-07-07 11:47:58', 0, 'TRANSLATION', '27', '{"id":"27","translation":"hardver","language":"srpski","org_term":3,"category":"Anglicizmi","user_id":2,"username":"admin","creation_date":"2016-07-07 11:47:58","argument":"Ako mo\\u017ee softver onda mo\\u017ee i hardver","rating":null,"_fields":[],"_modified":[]}'),
(70, 2, '2016-07-07 11:49:06', 1, 'TERM', '3', 'null'),
(71, 2, '2016-07-07 11:50:59', 1, 'TERM', '2', 'null'),
(72, 2, '2016-07-07 11:55:15', 0, 'TERM', '17', '{"term":{"ID":17,"term":"hash","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:55:15","description":"Vrednost dobijena jednosmernom transformacijom od originala. Slu\\u017ei npr za proveru ispravnosti.","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(73, 2, '2016-07-07 11:56:25', 0, 'TERM', '18', '{"term":{"ID":18,"term":"stack","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:56:25","description":"LIFO struktura (elementi se skidaju u suprotnom redosledu od umetanja)","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(74, 2, '2016-07-07 11:57:16', 0, 'TERM', '19', '{"term":{"ID":19,"term":"queue","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:57:16","description":"FIFO struktura - prvi dodat element se prvi i uklanja","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(75, 2, '2016-07-07 11:58:32', 0, 'TERM', '20', '{"term":{"ID":20,"term":"priority queue","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 11:58:32","description":"Elementi se skidaju po prioritetima, kojim god redom da su dodati","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(76, 2, '2016-07-07 12:12:14', 0, 'TERM', '21', '{"term":{"ID":21,"term":"list","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:12:13","description":"elementi se obilaze jedan po jedan","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(77, 2, '2016-07-07 12:13:17', 0, 'TERM', '22', '{"term":{"ID":22,"term":"tree","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:13:17","description":"struktura poput drveta - po\\u010dinje od korena pa ima grane","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(78, 2, '2016-07-07 12:13:39', 0, 'TERM', '23', '{"term":{"ID":23,"term":"struct","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:13:39","description":"op\\u0161ti termin za strukturu","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(79, 2, '2016-07-07 12:14:01', 0, 'TERM', '24', '{"term":{"ID":24,"term":"record","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:14:01","description":"jedan red zapisa","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(80, 2, '2016-07-07 12:14:34', 0, 'TERM', '25', '{"term":{"ID":25,"term":"array","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:14:34","description":"niz elemenata, pristupa im se po poziciji","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(81, 2, '2016-07-07 12:15:20', 0, 'TERM', '26', '{"term":{"ID":26,"term":"associative array","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:15:20","description":"skup elemenata kojima se pristupa po \\"nazivu\\" (klju\\u010du)","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(82, 2, '2016-07-07 12:16:11', 0, 'TERM', '27', '{"term":{"ID":27,"term":"binary search tree","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:16:11","description":"(BST) svaka grana se deli na ta\\u010dno dve, u jednoj svi elementi manji, u drugoj svi ve\\u0107i","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(83, 2, '2016-07-07 12:16:34', 0, 'TERM', '28', '{"term":{"ID":28,"term":"edit distance","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-07 12:16:34","description":"rastojanje","conclusion":null,"category_id":5,"category_title":"Strukture podataka","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Strukture podataka"]}'),
(84, 2, '2016-07-12 09:15:51', 0, 'TERM', '29', '{"term":{"ID":29,"term":"statement","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 09:15:51","description":"ovo izvr\\u0161ava procesor ili interpretator","conclusion":null,"category_id":4,"category_title":"Prevodi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Prevodi"]}'),
(85, 2, '2016-07-12 09:16:08', 0, 'TERM', '30', '{"term":{"ID":30,"term":"assignment","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 09:16:08","description":"=","conclusion":null,"category_id":4,"category_title":"Prevodi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Prevodi"]}'),
(86, 2, '2016-07-12 09:16:53', 0, 'TERM', '31', '{"term":{"ID":31,"term":"expression","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 09:16:53","description":"matemati\\u010dki, funkcionalni ili ve\\u0107 ima raznih...","conclusion":null,"category_id":4,"category_title":"Prevodi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Prevodi"]}'),
(87, 2, '2016-07-12 09:17:11', 0, 'TERM', '32', '{"term":{"ID":32,"term":"variable","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 09:17:11","description":"x,y,z...","conclusion":null,"category_id":4,"category_title":"Prevodi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Prevodi"]}'),
(88, 2, '2016-07-12 09:32:26', 0, 'TERM', '33', '{"term":{"ID":33,"term":"loop","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 09:32:26","description":"loop loop loop loop loop loop loop loop....","conclusion":null,"category_id":4,"category_title":"Prevodi","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Prevodi"]}'),
(89, 2, '2016-07-12 09:41:51', 0, 'TRANSLATION', '28', '{"id":"28","translation":"naredba","language":"srpski","org_term":29,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:41:51","argument":"najlogi\\u010dniji prevod.\\nka\\u017ee ra\\u010dunaru \\u0161ta da radi.","rating":null,"category_id":4,"lang":"sr","term":"statement","_fields":[],"_modified":[]}'),
(90, 2, '2016-07-12 09:43:14', 0, 'TRANSLATION', '29', '{"id":"29","translation":"iskaz","language":"srpski","org_term":29,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:43:14","argument":"Najprirodniji prevod, va\\u017ei za op\\u0161ti slu\\u010daj ne samo za kompjuterske programe","rating":null,"category_id":4,"lang":"sr","term":"statement","_fields":[],"_modified":[]}'),
(91, 2, '2016-07-12 09:43:40', 0, 'TRANSLATION', '30', '{"id":"30","translation":"komanda","language":"srpski","org_term":29,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:43:40","argument":"na neki na\\u010din sinonim. govori ra\\u010dunaru \\u0161ta da radi.","rating":null,"category_id":4,"lang":"sr","term":"statement","_fields":[],"_modified":[]}'),
(92, 2, '2016-07-12 09:44:04', 0, 'TRANSLATION', '31', '{"id":"31","translation":"instrukcija","language":"srpski","org_term":29,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:44:04","argument":"Pogledati \\"komanda\\". Dakle do\\u0111e na isto. Sinonim.","rating":null,"category_id":4,"lang":"sr","term":"statement","_fields":[],"_modified":[]}'),
(93, 2, '2016-07-12 09:49:44', 0, 'TRANSLATION', '32', '{"id":"32","translation":"dodela","language":"srpski","org_term":30,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:49:44","argument":"hmmm...","rating":null,"category_id":4,"lang":"sr","term":"assignment","_fields":[],"_modified":[]}'),
(94, 2, '2016-07-12 09:49:59', 0, 'TRANSLATION', '33', '{"id":"33","translation":"pridru\\u017eivanje","language":"srpski","org_term":30,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:49:59","argument":"hmmm...","rating":null,"category_id":4,"lang":"sr","term":"assignment","_fields":[],"_modified":[]}'),
(95, 2, '2016-07-12 09:50:39', 0, 'TRANSLATION', '34', '{"id":"34","translation":"izraz","language":"srpski","org_term":31,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 09:50:39","argument":"bukvalno nema kako druga\\u010dije da se prevede.","rating":null,"category_id":4,"lang":"sr","term":"expression","_fields":[],"_modified":[]}'),
(96, 2, '2016-07-12 10:02:01', 0, 'TRANSLATION', '35', '{"id":"35","translation":"promenljiva","language":"srpski","org_term":32,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 10:02:01","argument":"menja se kao pridev? dakle promen?","rating":null,"category_id":4,"lang":"sr","term":"variable","_fields":[],"_modified":[]}'),
(97, 2, '2016-07-12 10:02:36', 0, 'TRANSLATION', '36', '{"id":"36","translation":"varijabla","language":"srpski","org_term":32,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 10:02:36","argument":"?","rating":null,"category_id":4,"lang":"sr","term":"variable","_fields":[],"_modified":[]}'),
(98, 2, '2016-07-12 10:03:40', 1, 'TERM', '33', '{"ID":33,"Term":"loop","Language":"en","Creator":2,"Creation_date":"2016-07-12 09:32:26","Description":"while (true) echo ''loop''; loop loop loop loop loop loop loop loop....","Conclusion":null}'),
(99, 2, '2016-07-12 10:05:06', 0, 'TRANSLATION', '37', '{"id":"37","translation":"petlja","language":"srpski","org_term":33,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 10:05:06","argument":"op\\u0161teprihva\\u0107en prevod kod nas, mada nemam pojma kako je do\\u0161lo do njega. petlja = ne\\u0161to \\u0161to se ponavlja?","rating":null,"category_id":4,"lang":"sr","term":"loop","_fields":[],"_modified":[]}'),
(100, 2, '2016-07-12 10:05:39', 0, 'TRANSLATION', '38', '{"id":"38","translation":"ciklus","language":"srpski","org_term":33,"category":"Prevodi","user_id":2,"username":"admin","creation_date":"2016-07-12 10:05:39","argument":"ovo ve\\u0107 ima smisla. mada vi\\u0161e ozna\\u010dava krug. i manje se koristi.","rating":null,"category_id":4,"lang":"sr","term":"loop","_fields":[],"_modified":[]}'),
(101, 2, '2016-07-12 12:02:34', 0, 'TERM', '34', '{"term":{"ID":34,"term":"computer scientist","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:02:34","description":"zna sa ra\\u010dunarima","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(102, 2, '2016-07-12 12:02:59', 0, 'TERM', '35', '{"term":{"ID":35,"term":"informatician","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:02:59","description":"koliko je ovo zvani\\u010dan termin?","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(103, 2, '2016-07-12 12:03:48', 0, 'TERM', '36', '{"term":{"ID":36,"term":"software developer","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:03:48","description":"neki programer","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(104, 2, '2016-07-12 12:04:11', 0, 'TERM', '37', '{"term":{"ID":37,"term":"data scientist","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:04:11","description":"neki nau\\u010dnik","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(105, 2, '2016-07-12 12:04:49', 0, 'TERM', '38', '{"term":{"ID":38,"term":"data miner","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:04:49","description":"opet neko ra\\u010dunarsko zanimanje","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(106, 2, '2016-07-12 12:05:23', 0, 'TERM', '39', '{"term":{"ID":39,"term":"software engineer","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:05:22","description":"u su\\u0161tini programer ali malo vi\\u0161e","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(107, 2, '2016-07-12 12:06:04', 0, 'TERM', '40', '{"term":{"ID":40,"term":"developer","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:06:04","description":"DEVELOPERS! DEVELOPERS! DEVELOPERS!","conclusion":null,"category_id":6,"category_title":"Nazivi profesija","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Nazivi profesija"]}'),
(108, 2, '2016-07-12 12:07:06', 0, 'TRANSLATION', '39', '{"id":"39","translation":"ra\\u010dunarac","language":"srpski","org_term":34,"category":"Nazivi profesija","user_id":2,"username":"admin","creation_date":"2016-07-12 12:07:06","argument":"verovatno najta\\u010dniji prevod. nau\\u010dnik za ra\\u010dunare tj. bavi se ra\\u010dunarstvom?","rating":null,"category_id":6,"lang":"sr","term":"computer scientist","_fields":[],"_modified":[]}'),
(109, 2, '2016-07-12 12:10:21', 0, 'TRANSLATION', '40', '{"id":"40","translation":"ra\\u010dunarski nau\\u010dnik","language":"srpski","org_term":34,"category":"Nazivi profesija","user_id":2,"username":"admin","creation_date":"2016-07-12 12:10:21","argument":"bukvalni prevod. hmmm...","rating":null,"category_id":6,"lang":"sr","term":"computer scientist","_fields":[],"_modified":[]}'),
(110, 2, '2016-07-12 12:11:16', 0, 'TRANSLATION', '41', '{"id":"41","translation":"informati\\u010dar","language":"srpski","org_term":34,"category":"Nazivi profesija","user_id":2,"username":"admin","creation_date":"2016-07-12 12:11:16","argument":"?\\nDa li za informati\\u010dara postoji drugi (originalni) stru\\u010dni termin? Ra\\u010dunarstvo != informatika ?","rating":null,"category_id":6,"lang":"sr","term":"computer scientist","_fields":[],"_modified":[]}'),
(111, 2, '2016-07-12 12:35:26', 0, 'TRANSLATION', '42', '{"id":"42","translation":"informati\\u010dar","language":"srpski","org_term":35,"category":"Nazivi profesija","user_id":2,"username":"admin","creation_date":"2016-07-12 12:35:26","argument":"konkretan originalni engleski termin je ba\\u0161 za to, ostala zvanja po na\\u0161em su vi\\u0161e od \\"computer scientist\\"","rating":null,"category_id":6,"lang":"sr","term":"informatician","_fields":[],"_modified":[]}'),
(112, 2, '2016-07-12 12:53:21', 1, 'CATEGORY', '8', '{"id":8,"title":"Oblik i pisanje re\\u010di","creation_date":"2016-07-05 17:54:04","creator":"test","description":"ovo nije posebna kategorija termina? postavlja se pitanje kako se neki na\\u0161i ra\\u010dunarski izrazi (odnosno prevodi) pi\\u0161u...","conclusion":"Tehni\\u010dki, mogao bi se napisati originalni termin za ve\\u0107inu slu\\u010dajeva, pa navesti razli\\u010diti oblici pisanja kao prevodi. Pa bi u opis \\/ zaklju\\u010dak termina i\\u0161la diskusija kako se pi\\u0161e...\\nE sad ponekad to nisu prosti termini nego du\\u017ei delovi tipa da li se zadaje \\"sa standardnog ulaza\\" ili \\"na standardnom ulazu\\"?","_fields":[],"_modified":[]}'),
(113, 2, '2016-07-12 12:57:25', 0, 'TERM', '41', '{"term":{"ID":41,"term":"form","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 12:57:25","description":"Sigurno se ne misli na \\"oblik\\" ne\\u010dega...","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(114, 2, '2016-07-12 13:01:33', 0, 'TERM', '42', '{"term":{"ID":42,"term":"text box","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:01:33","description":"text text text text text text","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(115, 2, '2016-07-12 13:01:55', 0, 'TERM', '43', '{"term":{"ID":43,"term":"label","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:01:55","description":"...","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(116, 2, '2016-07-12 13:02:08', 0, 'TERM', '44', '{"term":{"ID":44,"term":"button","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:02:08","description":"click!","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(117, 2, '2016-07-12 13:02:39', 0, 'TERM', '45', '{"term":{"ID":45,"term":"icon","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:02:39","description":"...","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(118, 2, '2016-07-12 13:03:03', 0, 'TERM', '46', '{"term":{"ID":46,"term":"combo box","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:03:03","description":"...","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(119, 2, '2016-07-12 13:03:33', 0, 'TERM', '47', '{"term":{"ID":47,"term":"checkbox","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:03:33","description":"[x]","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(120, 2, '2016-07-12 13:04:25', 0, 'TERM', '48', '{"term":{"ID":48,"term":"status bar","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:04:25","description":"[_____|____________|__|__]","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(121, 2, '2016-07-12 13:05:25', 1, 'TERM', '46', '{"ID":46,"Term":"combo box","Language":"en","Creator":2,"Creation_date":"2016-07-12 13:03:03","Description":"[___________[v]","Conclusion":null}'),
(122, 2, '2016-07-12 13:06:44', 0, 'TERM', '49', '{"term":{"ID":49,"term":"toolbar","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:06:44","description":"[_][_][_][_][_]__________________]","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(123, 2, '2016-07-12 13:08:10', 0, 'TERM', '50', '{"term":{"ID":50,"term":"desktop","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:08:10","description":"vrh stola","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(124, 2, '2016-07-12 13:08:32', 0, 'TERM', '51', '{"term":{"ID":51,"term":"wallpaper","language":"engleski","language_code":"en","creator":"admin","creation_date":"2016-07-12 13:08:32","description":"papir za zid","conclusion":null,"category_id":7,"category_title":"Kontrole interfejsa","translations":null,"examples":null,"_fields":[],"_modified":[]},"categories":["Kontrole interfejsa"]}'),
(125, 2, '2016-07-12 16:21:11', 0, 'TRANSLATION', '43', '{"id":"43","translation":"forma","language":"srpski","org_term":41,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:21:11","argument":"bukvalno","rating":null,"category_id":7,"lang":"sr","term":"form","_fields":[],"_modified":[]}'),
(126, 2, '2016-07-12 16:23:07', 0, 'TRANSLATION', '44', '{"id":"44","translation":"formular","language":"srpski","org_term":41,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:23:07","argument":"odnosi se na slu\\u010daj kada se tra\\u017ei unos podataka. Ne samo informati\\u010dki pojam","rating":null,"category_id":7,"lang":"sr","term":"form","_fields":[],"_modified":[]}'),
(127, 2, '2016-07-12 16:24:19', 0, 'TRANSLATION', '45', '{"id":"45","translation":"prozor","language":"srpski","org_term":41,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:24:19","argument":"Obi\\u010dno se termin \\"form\\" koristi u programiranju da ozna\\u010di jedan prozor. To je jedno zna\\u010denje tog termina, a ovo mu je prevod.","rating":null,"category_id":7,"lang":"sr","term":"form","_fields":[],"_modified":[]}'),
(128, 2, '2016-07-12 16:24:46', 0, 'TRANSLATION', '46', '{"id":"46","translation":"obrazac","language":"srpski","org_term":41,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:24:46","argument":"Sli\\u010dno kao \\"formular\\" - u pitanju je unos podataka","rating":null,"category_id":7,"lang":"sr","term":"form","_fields":[],"_modified":[]}'),
(129, 2, '2016-07-12 16:27:33', 0, 'EXAMPLE', '5', '{"id":5,"example":"A form, in desktop programming, represents one window of the resulting application.","category_ID":7,"term_ID":41,"_fields":[],"_modified":[]}'),
(130, 2, '2016-07-12 16:28:33', 0, 'EXAMPLE', '6', '{"id":6,"example":"On the other hand, in web programming a form is part of the page that requests the user to input the desired data.","category_ID":7,"term_ID":41,"_fields":[],"_modified":[]}'),
(131, 2, '2016-07-12 16:29:55', 0, 'EXAMPLE', '7', '{"id":7,"example":"\\"Please fill in this form to register for our service.\\" - This is an example of the latter.","category_ID":7,"term_ID":41,"_fields":[],"_modified":[]}'),
(132, 2, '2016-07-12 16:32:23', 0, 'TRANSLATION', '47', '{"id":"47","translation":"polje za unos teksta","language":"srpski","org_term":42,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:32:23","argument":"jer bukvalno to i jeste. ne znam neki poseban na\\u0161 naziv za to?","rating":null,"category_id":7,"lang":"sr","term":"text box","_fields":[],"_modified":[]}'),
(133, 2, '2016-07-12 16:32:41', 0, 'TRANSLATION', '48', '{"id":"48","translation":"natpis","language":"srpski","org_term":43,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:32:41","argument":"...","rating":null,"category_id":7,"lang":"sr","term":"label","_fields":[],"_modified":[]}'),
(134, 2, '2016-07-12 16:33:09', 0, 'TRANSLATION', '49', '{"id":"49","translation":"labela","language":"srpski","org_term":43,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:33:09","argument":"bukvalni prevod, i \\u010desto se ba\\u0161 takav naziv i koristi kod nas","rating":null,"category_id":7,"lang":"sr","term":"label","_fields":[],"_modified":[]}'),
(135, 2, '2016-07-12 16:33:27', 0, 'TRANSLATION', '50', '{"id":"50","translation":"dugme","language":"srpski","org_term":44,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:33:27","argument":"tebra dugme!","rating":null,"category_id":7,"lang":"sr","term":"button","_fields":[],"_modified":[]}'),
(136, 2, '2016-07-12 16:34:43', 0, 'TRANSLATION', '51', '{"id":"51","translation":"ikona","language":"srpski","org_term":45,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:34:43","argument":"sigurno ne ona u crkvi. nego sli\\u010dica koja bi trebalo da asocira korisnika na odgovaraju\\u0107u akciju ili ve\\u0107 \\u0161ta.","rating":null,"category_id":7,"lang":"sr","term":"icon","_fields":[],"_modified":[]}'),
(137, 2, '2016-07-12 16:44:39', 0, 'TRANSLATION', '52', '{"id":"52","translation":"kombinovano polje","language":"srpski","org_term":46,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:44:39","argument":".","rating":null,"category_id":7,"lang":"sr","term":"combo box","_fields":[],"_modified":[]}'),
(138, 2, '2016-07-12 16:45:17', 0, 'TRANSLATION', '53', '{"id":"53","translation":"kombinovana lista","language":"srpski","org_term":46,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:45:17","argument":".","rating":null,"category_id":7,"lang":"sr","term":"combo box","_fields":[],"_modified":[]}'),
(139, 2, '2016-07-12 16:45:53', 0, 'TRANSLATION', '54', '{"id":"54","translation":"padaju\\u0107a lista","language":"srpski","org_term":46,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:45:53","argument":"\\u0161to je nekako najprirodnije \\u0161to se ti\\u010de samog na\\u010dina funkcionisanja.","rating":null,"category_id":7,"lang":"sr","term":"combo box","_fields":[],"_modified":[]}'),
(140, 2, '2016-07-12 16:47:18', 0, 'TRANSLATION', '55', '{"id":"55","translation":"polje za \\u0161tikliranje","language":"srpski","org_term":47,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:47:18","argument":"jer mo\\u017ee biti \\u0160TIKLIRANO (\\"on\\") ili ne (\\"off\\")","rating":null,"category_id":7,"lang":"sr","term":"checkbox","_fields":[],"_modified":[]}'),
(141, 2, '2016-07-12 16:52:17', 0, 'TRANSLATION', '56', '{"id":"56","translation":"polje za \\u010dekiranje","language":"srpski","org_term":47,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:52:17","argument":"jer \\u010cEK box","rating":null,"category_id":7,"lang":"sr","term":"checkbox","_fields":[],"_modified":[]}'),
(142, 2, '2016-07-12 16:53:44', 0, 'TRANSLATION', '57', '{"id":"57","translation":"polje za potvrdu","language":"srpski","org_term":47,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:53:44","argument":"jer ozna\\u010davamo potvrdu ne\\u010dega, ili njen nedostatak, pomo\\u0107u njega","rating":null,"category_id":7,"lang":"sr","term":"checkbox","_fields":[],"_modified":[]}'),
(143, 2, '2016-07-12 16:54:08', 0, 'TRANSLATION', '58', '{"id":"58","translation":"statusna linija","language":"srpski","org_term":48,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:54:08","argument":".","rating":null,"category_id":7,"lang":"sr","term":"status bar","_fields":[],"_modified":[]}'),
(144, 2, '2016-07-12 16:54:33', 0, 'TRANSLATION', '59', '{"id":"59","translation":"linija alatki","language":"srpski","org_term":49,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 16:54:33","argument":"po principu tool = alat","rating":null,"category_id":7,"lang":"sr","term":"toolbar","_fields":[],"_modified":[]}');
INSERT INTO `log` (`ID`, `User_ID`, `Date`, `ActionType`, `ResourceType`, `ResourceId`, `Arguments`) VALUES
(145, 2, '2016-07-12 17:34:57', 0, 'TRANSLATION', '60', '{"id":"60","translation":"traka alatki","language":"srpski","org_term":49,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 17:34:57","argument":".","rating":null,"category_id":7,"lang":"sr","term":"toolbar","_fields":[],"_modified":[]}'),
(146, 2, '2016-07-12 17:35:15', 1, 'TERM', '49', '{"ID":49,"Term":"toolbar","Language":"en","Creator":2,"Creation_date":"2016-07-12 13:06:44","Description":"[_][_][_][_][_]__________________]","Conclusion":"Treba voditi ra\\u010duna da se svaki \\"bar\\" prevodi na isti na\\u010din!"}'),
(147, 2, '2016-07-12 17:37:27', 0, 'TRANSLATION', '61', '{"id":"61","translation":"desktop","language":"srpski","org_term":50,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 17:37:27","argument":"realno, svi smo usvojili tu englesku re\\u010d kao na\\u0161u i uvek ka\\u017eemo \\"desktop\\".","rating":null,"category_id":7,"lang":"sr","term":"desktop","_fields":[],"_modified":[]}'),
(148, 2, '2016-07-12 17:40:52', 0, 'TRANSLATION', '62', '{"id":"62","translation":"radna povr\\u0161ina","language":"srpski","org_term":50,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 17:40:52","argument":"\\"Desktop\\" se ne odnosi samo na ra\\u010dunare zapravo, ali je ra\\u010dunarski termin uveden kao analogija. Onda je i prevod isti u oba slu\\u010daja. Stru\\u010dni prevod koji koriste svi ozbiljni programi.","rating":null,"category_id":7,"lang":"sr","term":"desktop","_fields":[],"_modified":[]}'),
(149, 2, '2016-07-12 17:42:14', 0, 'TRANSLATION', '63', '{"id":"63","translation":"tapeta","language":"srpski","org_term":51,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 17:42:14","argument":"pa bukvalno. ako je ekran kao zid, tapeta mi slu\\u017ei da ulep\\u0161a prizor \\"gledanja u prazan zid\\". Ovako se \\"wallpaper\\" prevodi i u stvarnom, ne-ra\\u010dunarskom svetu.","rating":null,"category_id":7,"lang":"sr","term":"wallpaper","_fields":[],"_modified":[]}'),
(150, 2, '2016-07-12 17:43:52', 0, 'TRANSLATION', '64', '{"id":"64","translation":"pozadina","language":"srpski","org_term":51,"category":"Kontrole interfejsa","user_id":2,"username":"admin","creation_date":"2016-07-12 17:43:52","argument":"jer je u pitanju slika koja ide u... pozadinu ekrana tj. iza svih prozora, ikona, teksta, SVEGA. Prakti\\u010dno uvek se koristi taj izraz kod nas. \\"Desktop pozadina\\" i sl.","rating":null,"category_id":7,"lang":"sr","term":"wallpaper","_fields":[],"_modified":[]}');

--
-- Truncate table before insert `originalterm`
--

TRUNCATE TABLE `originalterm`;
--
-- Dumping data for table `originalterm`
--

INSERT INTO `originalterm` (`ID`, `Term`, `Language`, `Creator`, `Creation_date`, `Description`, `Conclusion`) VALUES
(1, 'computer', 'en', 2, '2016-07-06 19:25:20', 'kompjuter', NULL),
(2, 'software', 'en', 2, '2016-07-06 19:27:58', 'program ili kolekcija programa; mašinski kod; izvršava ga hardver. Bez softvera hardver nema upotrebu; softver određuje kako se upotrebljava hardver.', NULL),
(3, 'hardware', 'en', 2, '2016-07-06 19:28:27', 'elektronski uređaj; radi konkretan posao. Ima interfejs za komunikaciju sa softverom.', NULL),
(4, 'Java', 'en', 2, '2016-07-07 10:39:11', 'programski jezik', NULL),
(5, 'C', 'en', 2, '2016-07-07 10:39:21', 'programski jezik', 'Kako se ovo menja kroz padeže? u C-­u, u Ceu, u jeziku C, u jeziku Ce, … u Ce++u, Si'),
(6, 'Python', 'en', 2, '2016-07-07 10:39:32', 'programski jezik', NULL),
(7, 'Pascal', 'en', 2, '2016-07-07 10:39:43', 'programski jezik', 'Pascal, Paskal, PASCAL?'),
(8, 'Windows', 'en', 2, '2016-07-07 10:39:59', 'operativni sistem', NULL),
(9, 'Linux', 'en', 2, '2016-07-07 10:40:07', 'operativni sistem', NULL),
(10, 'Microsoft', 'en', 2, '2016-07-07 10:42:37', 'softverska firma', NULL),
(11, 'OpenGL', 'en', 2, '2016-07-07 10:43:51', '3D tehnologija', NULL),
(12, 'Bluetooth', 'en', 2, '2016-07-07 10:44:26', 'način prenosa podataka', NULL),
(13, 'HTML', 'en', 2, '2016-07-07 10:46:00', 'jezik na Webu (pogledati "Web")', NULL),
(14, 'Deijkstra', 'en', 2, '2016-07-07 11:38:42', 'poznat po algoritmima', 'Dodati ili "ostalo / nepoznat" ili jezike za sve ove naučnike, po tome čije je to ime (tj koji se jezik priča tamo)!'),
(15, 'Tseytin', 'en', 2, '2016-07-07 11:42:12', 'informatičar', NULL),
(16, 'De Bruijn', 'en', 2, '2016-07-07 11:43:20', 'informatičar', NULL),
(17, 'hash', 'en', 2, '2016-07-07 11:55:15', 'Vrednost dobijena jednosmernom transformacijom od originala. Služi npr za proveru ispravnosti.', NULL),
(18, 'stack', 'en', 2, '2016-07-07 11:56:25', 'LIFO struktura (elementi se skidaju u suprotnom redosledu od umetanja)', NULL),
(19, 'queue', 'en', 2, '2016-07-07 11:57:16', 'FIFO struktura - prvi dodat element se prvi i uklanja', NULL),
(20, 'priority queue', 'en', 2, '2016-07-07 11:58:32', 'Elementi se skidaju po prioritetima, kojim god redom da su dodati', NULL),
(21, 'list', 'en', 2, '2016-07-07 12:12:13', 'elementi se obilaze jedan po jedan', NULL),
(22, 'tree', 'en', 2, '2016-07-07 12:13:17', 'struktura poput drveta - počinje od korena pa ima grane', NULL),
(23, 'struct', 'en', 2, '2016-07-07 12:13:39', 'opšti termin za strukturu', NULL),
(24, 'record', 'en', 2, '2016-07-07 12:14:01', 'jedan red zapisa', NULL),
(25, 'array', 'en', 2, '2016-07-07 12:14:34', 'niz elemenata, pristupa im se po poziciji', NULL),
(26, 'associative array', 'en', 2, '2016-07-07 12:15:20', 'skup elemenata kojima se pristupa po "nazivu" (ključu)', NULL),
(27, 'binary search tree', 'en', 2, '2016-07-07 12:16:11', '(BST) svaka grana se deli na tačno dve, u jednoj svi elementi manji, u drugoj svi veći', NULL),
(28, 'edit distance', 'en', 2, '2016-07-07 12:16:34', 'rastojanje', NULL),
(29, 'statement', 'en', 2, '2016-07-12 09:15:51', 'ovo izvršava procesor ili interpretator', NULL),
(30, 'assignment', 'en', 2, '2016-07-12 09:16:08', '=', NULL),
(31, 'expression', 'en', 2, '2016-07-12 09:16:53', 'matematički, funkcionalni ili već ima raznih...', NULL),
(32, 'variable', 'en', 2, '2016-07-12 09:17:11', 'x,y,z...', NULL),
(33, 'loop', 'en', 2, '2016-07-12 09:32:26', 'while (true) echo ''loop''; loop loop loop loop loop loop loop loop....', NULL),
(34, 'computer scientist', 'en', 2, '2016-07-12 12:02:34', 'zna sa računarima', NULL),
(35, 'informatician', 'en', 2, '2016-07-12 12:02:59', 'koliko je ovo zvaničan termin?', NULL),
(36, 'software developer', 'en', 2, '2016-07-12 12:03:48', 'neki programer', NULL),
(37, 'data scientist', 'en', 2, '2016-07-12 12:04:11', 'neki naučnik', NULL),
(38, 'data miner', 'en', 2, '2016-07-12 12:04:49', 'opet neko računarsko zanimanje', NULL),
(39, 'software engineer', 'en', 2, '2016-07-12 12:05:22', 'u suštini programer ali malo više', NULL),
(40, 'developer', 'en', 2, '2016-07-12 12:06:04', 'DEVELOPERS! DEVELOPERS! DEVELOPERS!', NULL),
(41, 'form', 'en', 2, '2016-07-12 12:57:25', 'Sigurno se ne misli na "oblik" nečega...', NULL),
(42, 'text box', 'en', 2, '2016-07-12 13:01:33', 'text text text text text text', NULL),
(43, 'label', 'en', 2, '2016-07-12 13:01:55', '...', NULL),
(44, 'button', 'en', 2, '2016-07-12 13:02:08', 'click!', NULL),
(45, 'icon', 'en', 2, '2016-07-12 13:02:39', '...', NULL),
(46, 'combo box', 'en', 2, '2016-07-12 13:03:03', '[___________[v]', NULL),
(47, 'checkbox', 'en', 2, '2016-07-12 13:03:33', '[x]', NULL),
(48, 'status bar', 'en', 2, '2016-07-12 13:04:25', '[_____|____________|__|__]', NULL),
(49, 'toolbar', 'en', 2, '2016-07-12 13:06:44', '[_][_][_][_][_]__________________]', 'Treba voditi računa da se svaki "bar" prevodi na isti način!'),
(50, 'desktop', 'en', 2, '2016-07-12 13:08:10', 'vrh stola', NULL),
(51, 'wallpaper', 'en', 2, '2016-07-12 13:08:32', 'papir za zid', NULL);

--
-- Truncate table before insert `password`
--

TRUNCATE TABLE `password`;
--
-- Dumping data for table `password`
--

INSERT INTO `password` (`UserID`, `Password`) VALUES
(1, '$2y$10$342cca1f6dcfa445ca1fauQ9H1UgPjlBsJ.Cfa5ghg4hzrcYh4QL2'),
(2, '$2y$10$dd5412a1b2e718a9c4651O4Bj5viLFWP.9QVn7zgh/100gsXZ7AN2');

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;

--
-- Truncate table before insert `translation`
--

TRUNCATE TABLE `translation`;
--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`ID`, `Translation`, `Language`, `Org_term`, `Category_ID`, `Creator`, `Creation_date`, `Argument`) VALUES
(1, 'kompjuter', 'sr', 1, 3, 2, '2016-07-06 19:31:06', 'Anglicizam - tako se izgovara.'),
(2, 'računar', 'sr', 1, 3, 2, '2016-07-06 19:36:00', 'Srpska reč - prirodan prevod'),
(3, 'Java', 'sr', 4, 1, 2, '2016-07-07 10:47:42', 'Pisanje u originalu. Često se kod nas ovako i izgovara (što je verovatno pogrešno).'),
(4, 'Džava', 'sr', 4, 1, 2, '2016-07-07 10:48:06', 'Ovako se pravilno izgovara.'),
(5, 'C', 'sr', 5, 1, 2, '2016-07-07 10:57:52', 'original'),
(6, 'Ce', 'sr', 5, 1, 2, '2016-07-07 10:58:09', 'kako mi izgovaramo'),
(7, 'Si', 'sr', 5, 1, 2, '2016-07-07 10:58:30', 'Kako se u originalu (na engleskom) izgovara'),
(8, 'Python', 'sr', 6, 1, 2, '2016-07-07 11:09:34', 'original'),
(9, 'Piton', 'sr', 6, 1, 2, '2016-07-07 11:10:07', 'Kao što i zmiju zovemo.'),
(10, 'Pajton', 'sr', 6, 1, 2, '2016-07-07 11:10:51', 'Kako se izgovara (niko ne izgovara "piton")'),
(11, 'Paskal', 'sr', 7, 1, 2, '2016-07-07 11:17:56', 'Po Vuku.'),
(12, 'Vindovs', 'sr', 8, 1, 2, '2016-07-07 11:21:57', 'već ušlo u naviku kod nas da se ovako izgovara'),
(13, 'Vindouz', 'sr', 8, 1, 2, '2016-07-07 11:22:45', 'otprilike ovako zvuči engleski izgovor. možda i ne.'),
(14, 'Linuks', 'sr', 9, 1, 2, '2016-07-07 11:23:23', 'jer x'),
(15, 'Majkrosoft', 'sr', 10, 1, 2, '2016-07-07 11:30:35', 'Ako već imena po pravilu pišemo kako se izgovaraju... zašto ne i imena firmi'),
(16, 'OpenGL', 'sr', 11, 1, 2, '2016-07-07 11:31:59', 'OpenGL, OpenDžiEl, Open Dži El?'),
(17, 'Blutut', 'sr', 12, 1, 2, '2016-07-07 11:33:12', 'Da li bi trebalo da pišemo Bluetooth ili BluTut?'),
(18, 'HTML', 'sr', 13, 1, 2, '2016-07-07 11:35:47', 'HTML, HaTeEmEl ili Ha-Te-Em-El? u HTML-u, u HaTeEmEl-u? A ćirilica?'),
(19, 'Dajkstra', 'sr', 14, 2, 2, '2016-07-07 11:40:34', 'jedna mogućnost'),
(20, 'Dejkstra', 'sr', 14, 2, 2, '2016-07-07 11:40:52', 'jedna mogućnost'),
(21, 'Dijkstra', 'sr', 14, 2, 2, '2016-07-07 11:41:28', 'jedna mogućnost. I to najčešća.'),
(22, 'Tsajtin', 'sr', 15, 2, 2, '2016-07-07 11:42:37', '?'),
(23, 'Cajtin', 'sr', 15, 2, 2, '2016-07-07 11:42:50', 'valjda se tako izgovara!'),
(24, 'De Brujn', 'sr', 16, 2, 2, '2016-07-07 11:43:54', 'možda'),
(25, 'De Brulj', 'sr', 16, 2, 2, '2016-07-07 11:44:09', 'možda'),
(26, 'softver', 'sr', 2, 3, 2, '2016-07-07 11:47:35', 'Zato što softver.'),
(27, 'hardver', 'sr', 3, 3, 2, '2016-07-07 11:47:58', 'Ako može softver onda može i hardver'),
(28, 'naredba', 'sr', 29, 4, 2, '2016-07-12 09:41:51', 'najlogičniji prevod.\nkaže računaru šta da radi.'),
(29, 'iskaz', 'sr', 29, 4, 2, '2016-07-12 09:43:14', 'Najprirodniji prevod, važi za opšti slučaj ne samo za kompjuterske programe'),
(30, 'komanda', 'sr', 29, 4, 2, '2016-07-12 09:43:40', 'na neki način sinonim. govori računaru šta da radi.'),
(31, 'instrukcija', 'sr', 29, 4, 2, '2016-07-12 09:44:04', 'Pogledati "komanda". Dakle dođe na isto. Sinonim.'),
(32, 'dodela', 'sr', 30, 4, 2, '2016-07-12 09:49:44', 'hmmm...'),
(33, 'pridruživanje', 'sr', 30, 4, 2, '2016-07-12 09:49:59', 'hmmm...'),
(34, 'izraz', 'sr', 31, 4, 2, '2016-07-12 09:50:39', 'bukvalno nema kako drugačije da se prevede.'),
(35, 'promenljiva', 'sr', 32, 4, 2, '2016-07-12 10:02:01', 'menja se kao pridev? dakle promen?'),
(36, 'varijabla', 'sr', 32, 4, 2, '2016-07-12 10:02:36', '?'),
(37, 'petlja', 'sr', 33, 4, 2, '2016-07-12 10:05:06', 'opšteprihvaćen prevod kod nas, mada nemam pojma kako je došlo do njega. petlja = nešto što se ponavlja?'),
(38, 'ciklus', 'sr', 33, 4, 2, '2016-07-12 10:05:39', 'ovo već ima smisla. mada više označava krug. i manje se koristi.'),
(39, 'računarac', 'sr', 34, 6, 2, '2016-07-12 12:07:06', 'verovatno najtačniji prevod. naučnik za računare tj. bavi se računarstvom?'),
(40, 'računarski naučnik', 'sr', 34, 6, 2, '2016-07-12 12:10:21', 'bukvalni prevod. hmmm...'),
(41, 'informatičar', 'sr', 34, 6, 2, '2016-07-12 12:11:16', '?\nDa li za informatičara postoji drugi (originalni) stručni termin? Računarstvo != informatika ?'),
(42, 'informatičar', 'sr', 35, 6, 2, '2016-07-12 12:35:26', 'konkretan originalni engleski termin je baš za to, ostala zvanja po našem su više od "computer scientist"'),
(43, 'forma', 'sr', 41, 7, 2, '2016-07-12 16:21:11', 'bukvalno'),
(44, 'formular', 'sr', 41, 7, 2, '2016-07-12 16:23:07', 'odnosi se na slučaj kada se traži unos podataka. Ne samo informatički pojam'),
(45, 'prozor', 'sr', 41, 7, 2, '2016-07-12 16:24:19', 'Obično se termin "form" koristi u programiranju da označi jedan prozor. To je jedno značenje tog termina, a ovo mu je prevod.'),
(46, 'obrazac', 'sr', 41, 7, 2, '2016-07-12 16:24:46', 'Slično kao "formular" - u pitanju je unos podataka'),
(47, 'polje za unos teksta', 'sr', 42, 7, 2, '2016-07-12 16:32:23', 'jer bukvalno to i jeste. ne znam neki poseban naš naziv za to?'),
(48, 'natpis', 'sr', 43, 7, 2, '2016-07-12 16:32:41', '...'),
(49, 'labela', 'sr', 43, 7, 2, '2016-07-12 16:33:09', 'bukvalni prevod, i često se baš takav naziv i koristi kod nas'),
(50, 'dugme', 'sr', 44, 7, 2, '2016-07-12 16:33:27', 'tebra dugme!'),
(51, 'ikona', 'sr', 45, 7, 2, '2016-07-12 16:34:43', 'sigurno ne ona u crkvi. nego sličica koja bi trebalo da asocira korisnika na odgovarajuću akciju ili već šta.'),
(52, 'kombinovano polje', 'sr', 46, 7, 2, '2016-07-12 16:44:39', '.'),
(53, 'kombinovana lista', 'sr', 46, 7, 2, '2016-07-12 16:45:17', '.'),
(54, 'padajuća lista', 'sr', 46, 7, 2, '2016-07-12 16:45:53', 'što je nekako najprirodnije što se tiče samog načina funkcionisanja.'),
(55, 'polje za štikliranje', 'sr', 47, 7, 2, '2016-07-12 16:47:18', 'jer može biti ŠTIKLIRANO ("on") ili ne ("off")'),
(56, 'polje za čekiranje', 'sr', 47, 7, 2, '2016-07-12 16:52:17', 'jer ČEK box'),
(57, 'polje za potvrdu', 'sr', 47, 7, 2, '2016-07-12 16:53:44', 'jer označavamo potvrdu nečega, ili njen nedostatak, pomoću njega'),
(58, 'statusna linija', 'sr', 48, 7, 2, '2016-07-12 16:54:08', '.'),
(59, 'linija alatki', 'sr', 49, 7, 2, '2016-07-12 16:54:33', 'po principu tool = alat'),
(60, 'traka alatki', 'sr', 49, 7, 2, '2016-07-12 17:34:57', '.'),
(61, 'desktop', 'sr', 50, 7, 2, '2016-07-12 17:37:27', 'realno, svi smo usvojili tu englesku reč kao našu i uvek kažemo "desktop".'),
(62, 'radna površina', 'sr', 50, 7, 2, '2016-07-12 17:40:52', '"Desktop" se ne odnosi samo na računare zapravo, ali je računarski termin uveden kao analogija. Onda je i prevod isti u oba slučaja. Stručni prevod koji koriste svi ozbiljni programi.'),
(63, 'tapeta', 'sr', 51, 7, 2, '2016-07-12 17:42:14', 'pa bukvalno. ako je ekran kao zid, tapeta mi služi da ulepša prizor "gledanja u prazan zid". Ovako se "wallpaper" prevodi i u stvarnom, ne-računarskom svetu.'),
(64, 'pozadina', 'sr', 51, 7, 2, '2016-07-12 17:43:52', 'jer je u pitanju slika koja ide u... pozadinu ekrana tj. iza svih prozora, ikona, teksta, SVEGA. Praktično uvek se koristi taj izraz kod nas. "Desktop pozadina" i sl.');

--
-- Truncate table before insert `user`
--

TRUNCATE TABLE `user`;
--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `FirstName`, `LastName`, `email`, `Company`, `Salt`, `Notes`, `Role`, `Active`, `Registration_date`) VALUES
(1, 'test', 'Test', 'Korisnik', 'test@webp.app', 'MATF', '342cca1f6dcfa445ca1fa3d93b212154', NULL, 0, 1, '2016-05-26 12:36:04'),
(2, 'admin', 'Administrator', 'Sajta', 'admin@matf.bg.ac.rs', 'Matematički fakultet', 'dd5412a1b2e718a9c4651b3b3bb68ffb', NULL, 1, 1, '2016-05-26 12:37:40');

--
-- Truncate table before insert `votes`
--

TRUNCATE TABLE `votes`;
--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`User_ID`, `Translated_term_ID`, `Vote`, `Creation_date`) VALUES
(2, 2, 5, '2016-07-06 19:45:11'),
(2, 9, 2, '2016-07-07 11:15:36'),
(2, 10, 4, '2016-07-07 11:11:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
